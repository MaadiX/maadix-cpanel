document.addEventListener("DOMContentLoaded", function()
{
    [...document.querySelectorAll('[name=select-all]')].forEach(
        function(trigger){
            trigger.addEventListener('click', function(){
            name = trigger.value;
	    checkboxes = document.querySelectorAll('.' + name );
	    for(var i=0, n=checkboxes.length;i<n;i++) {
		checkboxes[i].checked = trigger.checked;
	    } 
            });
        }
    );
});
