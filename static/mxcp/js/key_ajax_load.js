var firstPath= get_installation_path();
var theUrl = firstPath + '/api/check-borgkey-status'
//var theUrl = firstPath + '/api/get-mount-status'
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function httpGetAsync(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4){

            if (xmlHttp.responseText == 'available' && xmlHttp.status == 200){
              try {
                document.getElementById('key-ready').className='visible'
                document.getElementById('waiting-key').className='invisible'
              } catch(e){
               // Dom haws changed. Everything is ok  
              }
            } else {
              setTimeout(function() {httpGetAsync(theUrl)}, 3000);
            }
            console.log( xmlHttp.responseText );
        }
    }
    xmlHttp.open("POST", theUrl, true); // true for asynchronous 

    var csrftoken = getCookie('csrfcpaneltoken');
    xmlHttp.setRequestHeader("X-CSRFToken", csrftoken);
    xmlHttp.send();

}

httpGetAsync (theUrl);

