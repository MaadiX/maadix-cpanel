var firstPath= get_installation_path();
var theUrl = firstPath + '/api/check-borg'
//var theUrl = firstPath + '/api/get-mount-status'
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function httpGetAsync(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4  && this.status == 200){
            const res = JSON.parse(xmlHttp.responseText);
            //console.log(res["backups"]);
            files=res["backups"];
            files.forEach((repo) => {
                var repo_status=repo["status"]
                if(repo_status=="None"){

                    var mounted=repo["is_mounted"]
                    var name = repo["name"]
                    // Get all involved div
                    var mountPathDiv = document.getElementById(name+'-mountpoint').getElementsByClassName("mountpath")[0]
                    var noMountPathDiv = document.getElementById(name+'-mountpoint').getElementsByClassName("nomountpath")[0]
                    var inputFormDiv = document.getElementById(name+'-status').getElementsByClassName("withform")[0]
                    var noImputFormDiv = document.getElementById(name+'-status').getElementsByClassName("noform")[0]
                    var actionTextMount = document.getElementById(name+'-status').getElementsByClassName("withform")[0].getElementsByClassName("umount")[0]
                    var actionTextUmount = document.getElementById(name+'-status').getElementsByClassName("withform")[0].getElementsByClassName("mounted")[0]
                    inputFormDiv.className ="field__value withform "
                    noImputFormDiv.className = "field__value noform invisible";
                    // Instead of toogle, we impose the classes we need for each situation
                    if(mounted=="true"){
                        // Checj the mountpoint div in dom
                        mountPathDiv.className="mountpath";
                        noMountPathDiv.className="nomountpath invisible";
                        actionTextMount.className = "umount invisible";
                        actionTextUmount.className = "mounted";
                    } else {
                        mountPathDiv.className="mountpath invisible";
                        noMountPathDiv.className="nomountpath";
                        actionTextMount.className = "umount";
                        actionTextUmount.className = "mounted invisible";
                  } 
                }
            });
          // container
          var mfiles=res["mountedpoints"];
          console.log(mfiles)
          var myElements = document.getElementsByClassName('borgfs');
          for(var counter = 0; counter < myElements.length; counter++){ 
             var name = myElements[counter].value;
              if( !mfiles.some(item => item.name === name)){
                      var mountPathDiv = document.getElementById(name+'-container')
                      mountPathDiv.className="trash--table invisible";
              } 
          }


              setTimeout(function() {httpGetAsync(theUrl)}, 5000)
            }
        }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    var csrftoken = getCookie('csrfcpaneltoken');
    xmlHttp.setRequestHeader("X-CSRFToken", csrftoken);
    xmlHttp.send();

}

httpGetAsync (theUrl);
