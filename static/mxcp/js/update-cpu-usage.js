/**
 *  Returns the value of a cookie
 *  @see {@link https://docs.djangoproject.com/en/2.0/ref/csrf/}
 *  @param {str} name - Name of the cookie
 *  @returns {str}
 */
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

/**
 *  Makes AJAX POST request
 */
function post(url, callback)
{
    var xmlhttp   = new XMLHttpRequest();
    var csrftoken = getCookie('csrfcpaneltoken');
    xmlhttp.open("POST", url);
    xmlhttp.setRequestHeader("X-CSRFToken", csrftoken);
    xmlhttp.onload = function() {
        callback(null, xmlhttp.response);
    };
    xmlhttp.onerror = function() {
        callback(xmlhttp.response);
    };
    xmlhttp.send();
}

// This value have to be 'synced' with those in
// the function in views_system.py
var graph_total_height = 120;
var cpu_bars = 20;

/**
 *  get_height
 *  calculates the height of a system graph from a given percent
 */
function get_height(percent){
    return Math.round(graph_total_height * percent/100)
}

// Equivalent to jquery's "$(document).ready()"
document.addEventListener("DOMContentLoaded", function()
{
    var percent, height;
    var cpu_empty_bar = document.querySelector('.bar-group--1 .empty');
    var cpu_used_bar  = document.querySelector('.bar-group--1 .used');
    var cpu_value     = document.querySelector('.cpu-stats__item--used .value');
    var firstPath     = get_installation_path();
    // Gets the CPU usage from an api call each second
    var timer = setInterval(function(){
        post(firstPath + '/api/get-cpu-usage', function(error, request){
         if(isNaN(request)){ 
            cpu_value.textContent = 'Session expired'
            clearInterval(timer);

          } else {            
            var empty_1, empty_0, used_1, used_0;
            // Shift heights from second to last item
            for(var i = cpu_bars; i > 1; i--){
                empty_1 = document.querySelector('.bar-group--' + i + ' .empty');
                used_1  = document.querySelector('.bar-group--' + i + ' .used');
                empty_0 = document.querySelector('.bar-group--' + (i-1) + ' .empty');
                used_0  = document.querySelector('.bar-group--' + (i-1) + ' .used');
                empty_1.setAttribute("height", empty_0.getAttribute("height"));
                used_1.setAttribute("y", used_0.getAttribute("y"));
                used_1.setAttribute("height", used_0.getAttribute("height"));
            }
            // Gets the value and sets the height of the first item in the CPU graph
            percent = parseFloat(request);
            height = get_height(percent);
            cpu_empty_bar.setAttribute("height", 120 - height);
            cpu_used_bar.setAttribute("y", 120 - height);
            cpu_used_bar.setAttribute("height", height);
            // Sets the value in the legend text
            cpu_value.textContent = request;
          } 
        });
    }, 1000);
});
