var loginform = document.getElementById("form-ready");
var updatingform =  document.getElementById("form-updating");
var firstPath= get_installation_path();
var firstload = true;
var theUrl = '/status-cpanel'

function httpGetAsync(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4){
            if (xmlHttp.responseText == 'ready' && xmlHttp.status == 200){
              loginform.classList.toggle("hide");
              updatingform.className += " hide";
            }
            else if (xmlHttp.responseText != 'ready' && xmlHttp.status == 200){
              // If status not ready, go on with request
              if (firstload == true) {
                updatingform.classList.toggle("hide"); 
                firstload = false;
              }
              setTimeout(function() {httpGetAsync(theUrl)}, 3000);
            } else {
              // If apache is restarted we need to go on retreiving status
              setTimeout(function() {httpGetAsync(theUrl)}, 5000)
            }
            console.log( xmlHttp.responseText );
        }
    }
    xmlHttp.open("POST", theUrl, true); // true for asynchronous 
    var csrftoken = getCookie('csrfcpaneltoken');
    xmlHttp.setRequestHeader("X-CSRFToken", csrftoken);
    xmlHttp.send();

}

httpGetAsync (theUrl);

