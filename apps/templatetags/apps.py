from django.apps import AppConfig


class TemplatetagsConfig(AppConfig):
    name = 'apps.templatetags'
