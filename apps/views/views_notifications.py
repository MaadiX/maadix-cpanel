# django
from django.utils.translation import gettext_lazy as _
from django import views
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.urls import reverse_lazy, reverse
from django.contrib import messages
from django.utils.timezone import now
# project
from . import utils
from ldap3 import MODIFY_REPLACE
from .forms import NotificationForm, NotificationPgpForm
from django.conf import settings
import gnupg, os, base64, datetime, random, string 

class Notifications(FormView):
    """
    Email accounts list view and form to add a new email account
    """

    template_name = 'pages/notifications.html'
    form_class    = NotificationForm
    domains       = {}

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context               = super(Notifications, self).get_context_data(**kwargs)
        context['sendermail'] = self.sendermail
        return context

    def get_form(self):
        """Return an instance of the form to be used in this view."""
        emails = utils.get_existing_emails(self.request.ldap)
        self.emails = [ email.mail for email in emails ]
        self.emails.insert(0, 'www-data@%s' % utils.get_server_host() )
        self.sendermail = utils.get_notifications_address(self.request.ldap)
        # Get current configuration for Logs messages
        dn = "ou=logmail_custom,ou=conf,%s" %  settings.LDAP_TREE_CPANEL
        try:
            self.request.ldap.search(
                dn,
                '(ou=logmail_custom)',
                attributes=['status','postalAddress']
                )
            self.log_mail_status = self.request.ldap.entries[0].status.value
            mail_address = self.request.ldap.entries[0].postalAddress.value
            self.log_mail_address = mail_address if mail_address else utils.get_admin(self.request.ldap).email 
        except Exception as e:
            print("There was a problem getting custo logs value the new email")
            print(e)
        kwargs    = self.get_form_kwargs()
        kwargs['initial'] = {
            'log_server' : self.log_mail_status,
            'email'      : self.sendermail,
            'emailAddress' : self.log_mail_address
            }
        # Sen puppet status to form. Avoid changing logs receiver if puppet status is not ready
        puppet_status = utils.get_puppet_status(self.request)
        maintenance = puppet_status['puppetstatus']
        return NotificationForm(emails=self.emails, maintenance=maintenance,request=self.request, **kwargs)

    def form_valid(self, form):
        """If the form is valid, save the associated model."""

        try:
            dn = settings.LDAP_TREE_SENDERMAIL
            self.request.ldap.modify(dn, {
                'cn' : [(MODIFY_REPLACE, form['email'].value())]
            })
            # Update the value from log_server. It must be checed if the value has changed
            # If so, lock_cpanel is needed...else not
            # NOTICE: previously sthatus value was alway lowercase
            # TODO: Use has_changed
            
            if self.log_mail_status is not form['log_server'].value() or self.log_mail_address is not form['emailAddress'].value():
                
                dn = "ou=logmail_custom,ou=conf,%s" %  settings.LDAP_TREE_CPANEL
                self.request.ldap.modify(dn, {
                    'status' : [(MODIFY_REPLACE, form['log_server'].value())],
                    'postalAddress' : [(MODIFY_REPLACE, form['emailAddress'].value())]
                })
                try:
                    utils.lock_cpanel(self.request)
                except Exception as e:
                    print("There was a problem locking cpanle")
                    print(e)
                return HttpResponseRedirect( reverse('logout') )

        except Exception as e:
            print("There was a problem setting the new email")
            print(e)

        return super(Notifications, self).form_valid(form)

class NotificationsPgp(FormView):
    template_name = 'pages/pgpkeys.html'
    form_class    = NotificationPgpForm

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context = super(NotificationsPgp, self).get_context_data(**kwargs)
        
        try:
            self.request.ldap.search(
                settings.LDAP_TREE_GPG ,
                '(objectClass=pgpKeyInfo)',
                attributes=['*']
                )
            public_keys = self.request.ldap.entries
            context['public_keys'] = public_keys
            context['ldap_tree'] = settings.LDAP_TREE_GPG
        except Exception as e:
            print(e)
        return context

    def get_form(self):
        return self.form_class(request=self.request, **self.get_form_kwargs())

    def form_valid(self, form):
        #email_name = form['email'].value().strip()
        pkpKey     = form['pgpkey'].value().strip()
        try:
            gpg = gnupg.GPG(gnupghome="/usr/share/mxcp/.gnupg")
            letters = string.ascii_lowercase
            file_name =''.join(random.choice(letters) for i in range(10))
            filepath="/tmp/%s.asc" % file_name
            f = open( filepath, 'w+')
            f.write(pkpKey)
            f.close()
            #public_keys = gpg.list_keys()
            if os.path.isfile(filepath): 
                key_content = gpg.scan_keys(filepath)
                key =key_content[0]
            else:
                messages.error(self.request, _('Se ha producido un error añadiendo la clave' ))
            key_id = key["keyid"]
            dn="pgpCertID=%s,%s" % (key_id,settings.LDAP_TREE_GPG)
            data_bytes = pkpKey.encode("utf-8")
            key_64=base64.urlsafe_b64encode(data_bytes)
            #key_64 = str(key_64, "utf-8")
            #pgpKeyType: RSA
            """ Field valididty codes are explaines here
            https://github.com/gpg/gnupg/blob/master/doc/DETAILS#field-2---validity
            """
            data = {
                    'pgpKeyID'      : key["fingerprint"][-8:], 
                    'pgpUserID'     : key["uids"][0], 
                    'pgpSignerID'   : key_id,
                    'pgpCertID'     : key_id,
                    'pgpKey'        : pkpKey,
                    'pgpKeyExpireTime': 0,
                    }
            classes = ['pgpKeyInfo']
            os.remove(filepath)
            self.request.ldap.add(dn,classes,data)
            utils.lock_cpanel_local(self.request.ldap, 'zeyple');
            messages.success(self.request, _('Clave añadida con éxito'))
        except Exception as e:
            print(e)
            if e.result == 68:
                messages.error(self.request, _('La clave para %s ya existe. Eliminala para poder continuar' % key["uids"][0] ))
            else:
                messages.error(self.request, _('Se ha producido un error añadiendo la clave' ))

        return super().form_valid(form)

class ViewPgpKey(views.View):

    def get(self, request,*args, **kwargs):
        key_id=self.kwargs['keyid']
        if(key_id):
            dn="pgpCertID=%s,%s" % (key_id, settings.LDAP_TREE_GPG)
            try:
                self.request.ldap.search(
                    dn,
                    '(objectClass=pgpKeyInfo)',
                    attributes=['*']
                    )
                key = self.request.ldap.entries[0]
            except Exception as e:
                print(e)
                messages.error(self.request, _('No se ha encontrado la clave' ))
        return render(request, 'pages/view-pgpkey.html', locals())
