# django
from django import views
from django.utils.translation import gettext_lazy as _
from django.http import HttpResponseRedirect
from django.contrib import messages
# project
from . import utils
from django.conf import settings

class DeleteEntry(views.View):
    """ View to delete an entry from LDAP """

    def post(self, request):
        url = request.POST.get('url')
        local_module=module_status=''
        dns = []
        totrash = False
        try:
            dn  = request.POST.get('dn')
            print("DRDRDRDRDRDR " , dn)
            # Lock cpanel domains if a domains is deleted
            if (str(dn).startswith("vd=") and settings.LDAP_TREE_HOSTING in dn):
                print("FIRST STSTRTRRT ")
                local_module='domains'
                totrash = True
                #Build array of mails dn
                self.request.ldap.search(dn,
		    '(objectClass=*)',
		)
                search =self.request.ldap.entries
                for dnitem in search:
                    dns.append(dnitem.entry_dn)
            # Lock cpanel zyple if a key is deleted
            elif (str(dn).startswith("mail=") and settings.LDAP_TREE_HOSTING in dn):
                print("SECOND  STSTRTRRT")
                local_module='domains'
                totrash = True
                dns.append(dn)

            elif (str(dn).startswith("pgpCertID") and settings.LDAP_TREE_GPG in dn):
                print("iTHIRD  STSTRTRRT ")
                local_module='zeyple'
            
            # If Cpanel local module is locked do not allow performing deletion. Tell user to waiut a minute an try again
            if(local_module):
                module_status = utils.get_cpanel_local_status(self.request.ldap, local_module)     
            if (local_module and  module_status!= 'ready'):
                    messages.warning(self.request, _('Hay otro proceso en curso. '
                   'Vuelve a eliminar el elemento pasado un minuto, cuando el proceso anterior haya terminado'))
            else: 
                if ('ou=sshd,ou=People,' in dn ):
                    totrash = True
                    dns.append(dn)
                # Move item to trash
                if totrash:
                    #move_to_trash already locks local puppet module
                    utils.move_to_trash(self.request.ldap, dns) 
                    print("DES totrash", dns)
                if local_module:
                    #utils.lock_cpanel_local(self.request.ldap,local_module);
                    print("LOVA MODL ", local_module)
                # Delete item recursively
                print("DEeTIN DB ", dn)
                utils.recursive_delete(self.request.ldap, dn)
                messages.success(self.request, _('Elemento eliminado con éxito'))
                return HttpResponseRedirect(url)
        except Exception as e:
            if settings.DEBUG:
                print(e)
            messages.error(self.request, _('Hubo algún problema eliminando el elemento. '
                                       'Contacta con los administrador-s si el problema persiste'))
        return HttpResponseRedirect(url)
