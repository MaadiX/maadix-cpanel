# django
from django import views
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
# project
from .utils import add_dkim, lock_dkim
from django.conf import settings


class CreateDkim(views.View):
    """ View to  create dki m"""
    # There is no model for mailman domains. They are not stroed in ldap

    def post(self, request):
        try:
            dn  = request.POST.get('dn')
            url = request.POST.get('url')
            # Create  item 
            add_dkim(self.request.ldap, dn)
            # lock dkim cpanel
            lock_dkim(self.request.ldap)
            messages.success(self.request, _('Creando certificado dkim para el dominio %s' % dn))
            return HttpResponseRedirect(url)
        except Exception as e:
            if settings.DEBUG:
                print(e)
            messages.error(self.request, _('Hubo algún problema creando el certificado Dkim '
                                           'Contacta con los administrador-s si el problema persiste'))
            return HttpResponseRedirect(url)

