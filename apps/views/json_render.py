#pythoncheck-borg
import json
# django
from django import views
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.http import  HttpResponse, JsonResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_protect
# project
from . import utils

@csrf_protect
def get_json_data(request):
    backupsBorg=[]
    backupFiles=[]
    mountedFiles=[]
    try:
        with open(settings.BORG_JSON) as f:
            data = json.load(f)
            for item in data['archives']:
                backupsBorg.append(item['name'])
    except Exception as e:
        print("views_backups.py - There was an erro getting json data")
        print(e)
    try:
        # Get superuser to build mountpoint 
        request.ldap.search(
            search_base   = settings.LDAP_TREE_BASE,
            search_filter = settings.LDAP_FILTERS_SUPERUSER,
            attributes = ['uid']
        )

        superuser = request.ldap.entries[0]
        hostname = utils.get_server_hostname()
        mountpoint = "/home/" + superuser.uid.value + "/" + hostname + "-backups/"
        # Need to check if files are to be mounted or umounted, to shoew ifo in view
        i=0
        for item in backupsBorg:
            is_mounted= "true" if utils.check_mountpoint(mountpoint + item) else "false"
            try:
                    dn = "cn=%s,%s" % (item, settings.LDAP_TREE_BORG)
                    request.ldap.search(
                    dn,
                    "(objectClass=metaInfo)",
                    attributes=['status']
                    )
                    # if clean tree already exist in ldap update it
                    if request.ldap.entries:
                        mount_status = request.ldap.entries[0].status.value
            except Exception as e:
                mount_status = "None"
            data={"name":item, "status":mount_status, "is_mounted":is_mounted}
            #data = json.dumps(data)
            backupFiles.append(data)
            i=i+1
        # Get mounted points that are not in borg json anymore.
        # We want them on a separate list to show them as diffenet kind in template
        mounted_points = utils.get_mounted_borgfs()
        for namefile in mounted_points:
            if namefile in backupFiles:
                mounted_points.remove(namefile)

        for item in mounted_points:
            is_mounted= "true" 
            try:
                    dn = "cn=%s,%s" % (item, settings.LDAP_TREE_BORG)
                    request.ldap.search(
                    dn, 
                    "(objectClass=metaInfo)",
                    attributes=['status']
                    )   
                    # if clean tree already exist in ldap update it
                    if request.ldap.entries:
                        mount_status = request.ldap.entries[0].status.value
            except Exception as e:
                mount_status = "None"
            data={"name":item, "status":mount_status, "is_mounted":is_mounted}
            #data = json.dumps(data)
            mountedFiles.append(data)

    except Exception as e:
        print(e)

    return JsonResponse({"backups": backupFiles,"mountedpoints": mountedFiles}, content_type = "application/json")

@csrf_protect
def get_keyborg_status(request):
    #cn=borgbackup,ou=credentials,dc=example,dc=tld
    #When attribute type is available, key has been exported to ldap
    dn = "cn=borgbackup,ou=credentials,%s" % settings.LDAP_TREE_BASE
    filter_search  = '(&(objectClass=applicationProcess)(objectClass=metaInfo))'
    try:
        # Get superuser to build mountpoint
        request.ldap.search(
            search_base   = dn,
            search_filter = filter_search,
            attributes = ['type']
        )
        status = request.ldap.entries[0]['type']
    except Exception as e:
        print(e)
        status = None
    return HttpResponse(status)
