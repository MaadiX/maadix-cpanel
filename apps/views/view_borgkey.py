# python
import json
import os

# django
from django.utils.translation import gettext_lazy as _, get_language
from django import views
from django.shortcuts import render
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.generic.edit import FormView
from django.core.serializers.json import DjangoJSONEncoder
# contrib
from ldap3 import MODIFY_REPLACE,SUBTREE
# project
from . import utils
from django.conf import settings
from . import debug
from .forms import ShowBorgKeyForm
from apps.views import json_render as jsonr
class BorgPrivateKey(FormView):
    template_name = 'pages/borg-key.html'
    form_class    = ShowBorgKeyForm 
    ldap          = {}
    success_url   = reverse_lazy('system-details')


    def get_context_data(self, **kwargs):
        context = super(BorgPrivateKey, self).get_context_data(**kwargs)
        #get superusername to build path for mounpotin
        context['show_copy_key'] = 'form' in kwargs
        context['key_value'] = 'form' in kwargs
        #context['waiting_key'] = True if self.borgkey_status=='locked' or self.key_available=="available"  else False
        context['waiting_key'] = 'form' in kwargs
        #context['borg_key_copied'] = 'form' in kwargs
        key_copied = utils.get_cpanel_local_status(self.request.ldap, 'keycopy')
        #context['borg_key_copied'] = True if key_copied=="ready" else False
        context['borg_key_status'] = key_copied
        return context

    def get(self, request):
        try:
            self.borgkey_status = utils.get_cpanel_local_status(self.request.ldap, 'borgkey')
            key_available = jsonr.get_keyborg_status(self.request)
            self.key_available = key_available.content.decode('utf-8')
            # If borg_key_status = locked, means that the key has already been required
        except Exception as e:
            print(e)
            messages.error(self.request, _('Se ha producido un error'))
        return super(BorgPrivateKey, self).get(request)

    def get_form(self):
        return self.form_class(**self.get_form_kwargs())


    def post(self, request, *args, **kwargs):
        data = self.get(request)
        form = self.get_form()
        context = self.get_context_data(**kwargs)
        # Get all checked iputs
        errors=False
        lock = False
        if request.method=='POST' and 'dontbother' in request.POST:
            # Need to give user the chance to not be botherd when they have already copied the key
            objectClasses = ['organizationalUnit', 'metaInfo','extensibleObject'] 
            dn = "ou=keycopy,%s" % settings.LDAP_TREE_CPANEL
            bother_error = _("Se ha producido un error")
            ready="ready"
            utils.generic_update_or_create_ldap_entry(
                    self.request.ldap,
                    dn,
                    objectClasses,
                    {'status' : [ (MODIFY_REPLACE, ready) ]}, 

                    bother_error, 

                    {
                        'info'    : ready,
                        'status' : ready,
                    },
                    bother_error)

            context['show_copy_key'] = False
            return HttpResponseRedirect(self.success_url)
        if request.method=='POST' and 'lockmodule' in request.POST:
            # Just lock borgkeymodule
            utils.lock_cpanel_local(self.request.ldap, 'borgkey')
            #define cotext to show the ExportKey form
            context['show_copy_key'] = True
            context['waiting_key'] = True

        if form.is_valid() and request.method=='POST':
            # Binf using inserter password to show key value
            username = self.request.user.username
            dn     = "cn=%s,%s" % (username, settings.LDAP_TREE_BASE)
            old_pswd = form['current_password'].value()
            fields = {}
            try:
                ldap = utils.connect_ldap(dn, old_pswd)
                if ldap['connection']:
                    bind = ldap['connection']

                    privatekey = bind.search(
                        'cn=borgbackup,ou=credentials,dc=example,dc=tld',
                        settings.LDAP_FILTERS_METAINFO,
                        attributes=['status']
                    )

                    if privatekey:
                        #sHOW key in page
                        context['key_value'] = bind.entries[0]['status']
                    else:
                        messages.success(self.request, _('La clave ya no está disponible. Vuelve a exportarla'))
                else:
                    messages.error(self.request, _('La contraseña no es correcta' ))
            except Exception as e:
                messages.error(self.request, _('La contraseña no es correcta ' ))
                utils.p("✕ view_borgkey.py", "There's a problem in  LDAP: ", e)

            # Update User Model to have same data as in LDAP

        #return HttpResponseRedirect(self.request.get_full_path())
        return self.render_to_response(context)

