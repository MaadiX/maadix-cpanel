import psutil

# django
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST
from django.http import HttpResponse

@csrf_protect
@require_POST
def get_cpu_usage(request):
    """
    View to get the CPU usage of the host machine.
    """
    cpu = psutil.cpu_percent()
    return HttpResponse(cpu, content_type="application/json")

