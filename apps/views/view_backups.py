# python
import json
import os

# django
from django.utils.translation import gettext_lazy as _, get_language
from django import views
from django.shortcuts import render
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.generic.edit import FormView
from django.core.serializers.json import DjangoJSONEncoder
# contrib
from ldap3 import MODIFY_REPLACE,SUBTREE
# project
from . import utils
from django.conf import settings
from . import debug
from .forms import BorgForm
from apps.views import json_render as jsonr
class BorgBackups(FormView):
    template_name = 'pages/borg-backups.html'
    form_class    = BorgForm
    ldap          = {}

    def get_context_data(self, **kwargs):
        context = super(BorgBackups, self).get_context_data(**kwargs)
        #get superusername to build path for mounpotin
        context['mountpoint'] = self.mountpoint
        context['backpsExists'] = True if len(self.backupfiles)>0 else False
            # Need to check if files are to be mounted or umounted, to shoew ifo in view
        context['show_modal'] = 'form' in kwargs
        context['modal_text_body'] =_("Estás a punto de montar/desmontar las copias de seguridad seleccionadas. El proceso tardará unos minutos hasta que el sistema termine la operación.")
        context['modal_text_confirm'] =_("¿Confirmas que quieres continuar?")

        return context

    def get(self, request):
        try:
            # Get superuser to build mountpoint 
            self.request.ldap.search(
                search_base   = settings.LDAP_TREE_BASE,
                search_filter = settings.LDAP_FILTERS_SUPERUSER,
                attributes = ['uid']
            )

            superuser = self.request.ldap.entries[0]
            hostname = utils.get_server_hostname()
            self.mountpoint = "/home/" + superuser.uid.value + "/" + hostname + "-backups/"
            backupfiles=jsonr.get_json_data(request)
            datastr = backupfiles.content.decode('utf-8')
            self.backupfiles=json.loads(datastr)

        except Exception as e:
            print(e)
            messages.error(self.request, _('Se ha producido un error'))
        return super(BorgBackups, self).get(request)

    def get_form(self):
        return self.form_class(request=self.request,backupfiles=self.backupfiles,**self.get_form_kwargs())

    def post(self, request, *args, **kwargs):
        data = self.get(request) 
        form = self.get_form()
        context = self.get_context_data(**kwargs)
        # Get all checked iputs
        errors=False
        lock = False
        if form.is_valid() and request.method=='POST':
            context['show_modal'] = True
            objectClasses = ['organizationalUnit', 'metaInfo','extensibleObject']
            error_msg=_("Se ha producido un error")
            dn="ou=borgbackup,%s" % settings.LDAP_TREE_CPANEL 
            try:
                borgtree = request.ldap.search(
                    settings.LDAP_TREE_BORG,
                    settings.LDAP_FILTERS_METAINFO,
                    attributes=['ou']
            )
            except Exception as e:
                print("✕ borg tree does not exists. Create it " , e)
                request.ldap.add(settings.LDAP_TREE_BORG,objectClasses, {'status' : 'ready', 'ou' : 'borgbackup'})

        if request.method=='POST' and 'writeldap' in request.POST:
            objectClasses = ['applicationProcess', 'metaInfo','top']
            errors=""
            action=""
            for field in form:
                if field.value():
                    if utils.check_mountpoint(self.mountpoint + field.name):
                        action="umount"
                    else:
                        action="mount"
                    dn="cn=%s,%s" %(field.name,settings.LDAP_TREE_BORG)
                    try:
                        utils.generic_update_or_create_ldap_entry(request.ldap, dn,objectClasses,
                            {'status' : [ (MODIFY_REPLACE, action) ]}, error_msg,
                            {
                                'cn'    : field.name,
                                'status' : action,
                            },error_msg)
                    except Exception as e:
                        errors=True
                        messages.error(self.request, error_msg)
            if not errors:
                utils.lock_cpanel_local(self.request.ldap, 'borgbackup')
                context['show_modal'] = False
                messages.success(self.request, _('Cambios aplicados con éxito'))
            return HttpResponseRedirect(self.request.get_full_path())

        return self.render_to_response(context)

