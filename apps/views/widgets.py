# django
from django.forms.widgets import CheckboxInput, TextInput, Widget, MultiWidget
from django.utils.translation import gettext_lazy as _
from django.template.loader import render_to_string
from django import forms
class LabelledCheckbox(CheckboxInput):
    """A custom checkbox that displays a label besides the checkbox """

    def __init__(self, attrs=None, label=_('Activado')):
        self.label = label
        super(LabelledCheckbox, self).__init__(attrs)

    def render(self, name, value, attrs=None,renderer=None):
        """Render widget"""
        parent_widget = super(LabelledCheckbox, self).render(name, value, attrs )
        widget = render_to_string("blocks/labelled-checkbox.html", {
            'parent_widget' : parent_widget,
            'label'         : self.label,
        })
        return widget


class ListTextWidget(TextInput):
    """ A custom widget that lets the user to select a existing value or add a new value. """
    """ @see https://stackoverflow.com/questions/24783275/django-form-with-choices-but-also-with-freetext-option """

    def __init__(self, data_list, name, *args, **kwargs):
        super(ListTextWidget, self).__init__(*args, **kwargs)
        self._name = name
        self._list = data_list
        self.attrs.update({ 'list' : 'list__%s' % self._name })

    def render(self, name, value, attrs=None, renderer=None):
        text_html = super(ListTextWidget, self).render(name, value, attrs=attrs)
        data_list = '<datalist id="list__%s">' % self._name
        for item in self._list:
            data_list += '<option value="%s">%s' % (item,item)
        data_list += '</datalist>'

        return (text_html + data_list)

class HeaderWidget(Widget):
    def __init__(self, attrs=None, label=None, tag='h1'):
        self.label = label
        self.tag = tag
        super(HeaderWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None,renderer=None):
        
        widget = render_to_string("blocks/header-form-field.html", {
            'tag'   : self.tag,
            'label' : self.label,
            })
        return widget

class MultipleCheckBoxWidget(MultiWidget):

    def __init__(self, attrs={}):
        _widgets = (
            CheckboxInput(attrs=attrs),
            CheckboxInput(attrs=attrs)
        )  

        super(MultipleCheckBoxWidget, self).__init__(_widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.enable, value.delete ]
        return [None, None]

class MultipleCheckBoxField(forms.MultiValueField):
    """
    Custom field to display multiple input boxes for a related object
    """

    def __init__(self, label, fieldnames):
        # sub_max_length, is the max_length of each subfield
        self.widget = CheckboxWidget(fieldnames, label)
        fields=()
        for name in fieldnames:
            field = forms.BooleanField(label="%s-%s" %(label, name))
            print("FILED IS ", field)
            fields+= (forms.BooleanField(label="%s-%s" %(label, name),
                            ),)
        super(MultipleCheckBoxField, self).__init__(fields, required=False)

    def compress(self, value_list):
        if value_list:
            print("VAUE LIST ", value_list)
            return value_list
        return [[] for i in self.total]

class CheckboxWidget(forms.MultiWidget):
    """
    Custom widget
    """
    def __init__(self, fieldnames , label):
        self.fieldnames=fieldnames 
        self.label = label
        widgets = [forms.CheckboxInput(attrs={'name':name, 'value':name}) for name in fieldnames]
        super(CheckboxWidget, self).__init__(widgets)

    def decompress(self, value):
        if value:
            return value
        return [None for i in self.fieldnames]


