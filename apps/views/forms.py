# python
import re, base64, pwd, os, time, psutil,json,requests
# django
from django import forms
from django.utils.translation import gettext_lazy as _, get_language
from django.urls import reverse, reverse_lazy
from django.contrib.auth.password_validation import validate_password
from django.core.validators import validate_email
from django.utils.safestring import mark_safe
from django.utils.text import slugify
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django_auth_ldap.backend import LDAPBackend
from django.contrib.auth import  get_user_model
from django.core import serializers
# contrib
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA
from ldap3.utils.hashed import hashed
# project
from django.conf import settings
from django.contrib.auth.forms import (
    AuthenticationForm, PasswordChangeForm, PasswordResetForm as passresetform, SetPasswordForm as setpassword,
    )
from django.contrib.postgres.forms import SimpleArrayField as SimpleArrayFieldForm
from . import widgets, utils, customfileds

UserModel = get_user_model()

class GenericForm(forms.Form):
    """
    Generic form to be inherited by regular forms on the site.
    """

    required_css_class = 'required'
    error_css_class    = 'error'

    #  Clean method that check if passwords coincide
    #  In case the form doesn't have both password fields
    #  it will work because both fields will be None
    def clean(self):
        cleaned_data = super(GenericForm, self).clean()
        password           = cleaned_data.get("password")
        confirmed_password = cleaned_data.get("password_2")
        if password and not confirmed_password:
            raise forms.ValidationError(
                _("Por favor confirma la contraseña que has introducido.")
            )
            context['display_form'] = True
        if password != confirmed_password:
            raise forms.ValidationError(
                _("Las contraseñas no coinciden.")
            )
            context['display_form'] = True
        if password:
            validate_password(password)


    #  Render fields as divs
    #  and adds a css class to these divs, as we've removed %(html_class_attr)s
    #  this rendering method won't include any class defined by field in the form
    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True
        )


class CustomLoginForm(forms.Form):
    """ Custom login form """

    username = forms.CharField(label=_('Nombre de la Cuenta'), required=True)
    password = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(), required=True)

    def as_p(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(field)s %(label)s %(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True)
    def clean(self):
        cleaned_data = super().clean()
        password = self.cleaned_data.get("password")
        username = self.cleaned_data.get("username")
        ldap = utils.connect_ldap(username, password)
        message=None
         # Logi = utils.connect_ldap(username, password)
        if 'error' in ldap and ldap['error']== 'LDAPSocketOpenError':
            # If socket error, ldap is shut down for some reasons
            # warn the user accordingly
            message= _('Hay problemas de conectividad con la base de datos.'
                      'Intenta entrar dentro de unos minutos. Disculpa las '
                      'molestias. Si el problema persiste contacta con nosotras')
        if 'error' in ldap and ldap['error'] == 'LDAPInvalidCredentialsResult':
            #log remote IP when login with invalid credentials
            #doc: https://stackoverflow.com/questions/4581789/how-do-i-get-user-ip-address-in-django
            message= _('Las credenciales son incorrectas. ¿Las has escrito correctamente?')
        if message:
            raise forms.ValidationError(message)
""" Start new recover password methods """

class PassResetForm(passresetform):

    def get_users(self, email):
        """Given an email, return matching user(s) who should receive a reset.

        This allows subclasses to more easily customize the default policies
        that prevent inactive users and users with unusable passwords from
        resetting their password.
        """
        ldap  = utils.anonymous_connect_ldap()
        admin = utils.get_admin(ldap)
        user = LDAPBackend().populate_user(admin.cn.value)
        if user is None:
                raise Exception("No user found")

        email_field_name = UserModel.get_email_field_name()
        active_users = UserModel._default_manager.filter(**{
            '%s__iexact' % email_field_name: email,
            'is_active': True,
        })
        return (
            u for u in active_users
            if u.has_usable_password() and
            _unicode_ci_compare(email, getattr(u, email_field_name))
        )
    """ Check that provided email matches admin email.
    Admin is the only user that can recover password"""
    def clean(self):
        cleaned_data = super().clean()
        email = self.cleaned_data.get('email')
        ldap  = utils.anonymous_connect_ldap()
        admin = utils.get_admin(ldap)
        if not email == admin.email.value:
            raise forms.ValidationError(
                _("No se puede enviar el correo" )
            )

    def save(self, domain_override=None,
             subject_template_name='emails/password_reset_subject.txt',
             email_template_name='emails/password_reset_email.html',
             use_https=True, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name=None,
             extra_email_context=None):
        """
        Generate a one-use only link for resetting password and send it to the
        user.
        """
        email = self.cleaned_data["email"]
        ldap  = utils.anonymous_connect_ldap()
        admin = utils.get_admin(ldap)
        user = LDAPBackend().populate_user(admin.cn.value)
        # Only admin user can recover password
        user.is_active = True
        user.is_staff = True 
        user.is_superuser = True

        user_email= admin.email.value
        current_site = get_current_site(request)
        site_name = current_site.name
        domain = current_site.domain
        context = {
            'email': user_email,
            'domain': domain,
            'site_name': site_name,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'token': token_generator.make_token(user),
            'protocol': 'https', 
            **(extra_email_context or {}),
        }
        self.send_mail(
            subject_template_name, email_template_name, context, from_email,
            user_email, html_email_template_name=html_email_template_name,
        )
#class SetNewPasswordForm(setpassword):
""" End new recover password method"""


class ActivateForm(GenericForm):
    """ Form to edit user's profile. """

    username         = forms.CharField(label=_('Nombre de usuarix'), required=False)
    current_password = forms.CharField(label=_('Contraseña actual'), widget=forms.PasswordInput(), required=True)
    password         = forms.CharField(label=_('Nueva contraseña'), widget=forms.PasswordInput(), required=True)
    password_2       = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=True)
    email            = forms.EmailField(label=_('Correo electrónico'), required=True,
                                        help_text=_('Asegúrate de que el correo electrónico asociado a tu cuenta sea válido y '
                                                    'que tengas acceso a él: si pierdes la contraseña únicamente podrás '
                                                    'resetearla a través de este correo electrónico.'))
    sudousername         = forms.CharField(label=_('Nombre de cuenta sudo (root)'), required=False)
    sudopassword         = forms.CharField(label=_('Nueva contraseña'), widget=forms.PasswordInput(), required=True)
    sudopassword_2       = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=True)
    mysqlPass         = forms.CharField(label=_('Contraseña de acceso a MySQL'), widget=forms.PasswordInput(), required=True,
                                        help_text=_('Inserta la contraseña para acceder a las bases de datos '))
    mysqlPass_2       = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=True)
    def __init__(self, *args, **kwargs):
        #self.old_pwd  = kwargs.pop('pwd')
        #self.sudouser = kwargs.pop('sudouser')
        super(ActivateForm,self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs = {
            'readonly' : True,
            'class'    : 'disabled'
        }
        self.fields['sudousername'].widget.attrs = {
            'readonly' : True,
            'class'    : 'disabled'
        }

    def clean(self):
        cleaned_data = super().clean()
        username = self.cleaned_data.get('username')
        sudousername = self.cleaned_data.get('sudousername')
        current_pwd  = self.cleaned_data.get('current_password')
        new_password = self.cleaned_data.get('password')
        new_password_2 = self.cleaned_data.get('password_2')
        sudo_password = self.cleaned_data.get('sudopassword')
        sudo_password_2 = self.cleaned_data.get('sudopassword_2')
        mysqlPass =self.cleaned_data.get('mysqlPass');
        mysqlPass_2 =self.cleaned_data.get('mysqlPass_2');
        # Current password  match doesn't need to be checked as it is used for bind
        # Just check if password confirmation matches
        ldap = utils.connect_ldap(username, current_pwd)
        if ldap['connection']: 
            if current_pwd == new_password:
                raise forms.ValidationError(
                    _("La nueva contraseña es igual a la anterior. Por razones de seguridad debes cambiarla" )
                ) 
            if current_pwd and not new_password:
                raise forms.ValidationError(
                    _("Introduce la nueva contraseña para la cuenta %s" % username)
                )
            if current_pwd and not sudo_password:
                raise forms.ValidationError(
                    _("Introduce la nueva contraseña para la cuenta %s" % sudousername)
                )
            if sudo_password and not sudo_password_2:
                raise forms.ValidationError(
                    _("Confirma la contraseña para la cuenta %s" % sudousername)
                )
            if sudo_password != sudo_password_2:
                raise forms.ValidationError(
                    _("Las contraseñas de la cuenta %s no coinciden" % sudousername)
                )
            if mysqlPass != mysqlPass_2:
                raise forms.ValidationError(
                    _("Las contraseñas MysQL no coinciden")
                )
            if new_password and sudo_password:
                validate_password(new_password)
                validate_password(sudo_password)
                #validate_password(mysqlPass)
 
        elif ldap['error'] == 'LDAPSocketOpenError':
            # If socket error, ldap is shut down for some reasons
            # warn the user accordingly
            raise forms.ValidationError(
                _("Error de conexión a la base de datos")
            )

        elif ldap['error'] == 'LDAPInvalidCredentialsResult':
            raise forms.ValidationError(
                _("La contraseña actual que has insertado no es correcta")
            )


class ProfileForm(GenericForm):
    """ Form to edit user's profile. """

    username         = forms.CharField(label=_('Nombre de usuarix'), required=False)
    current_password = forms.CharField(label=_('Contraseña actual'), widget=forms.PasswordInput(), required=True,help_text=_('Introduce la contraseña de acceso al panel de control'))
    password         = forms.CharField(label=_('Nueva contraseña'), widget=forms.PasswordInput(), required=False)
    password_2       = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=False)
    email            = forms.EmailField(label=_('Correo electrónico'), required=True,
                                        help_text=_('Asegúrate de que el correo electrónico asociado a tu cuenta sea válido y '
                                                    'que tengas acceso a él: si pierdes la contraseña únicamente podrás '
                                                    'resetearla a través de este correo electrónico.'))

    def __init__(self, *args, **kwargs):
        #self.old_pwd  = kwargs.pop('pwd')
        role          = kwargs.pop('role')
        super(ProfileForm,self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs = {
            'readonly' : True,
            'class'    : 'disabled'
        }
        if role != 'admin':
            del self.fields['email']

    def clean(self):
        cleaned_data = super().clean()
        """
        If noting has changed do not perform save post
        Admin password is required, so just check if changed_data is ==1
        """
        if len(self.changed_data) == 1:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
                )

        current_pwd  = self.cleaned_data.get('current_password')
        new_password = self.cleaned_data.get('password')
        """
        if current_pwd != self.old_pwd:
            raise forms.ValidationError(
                _("La contraseña actual no coincide con la introducida.")
            )
        """

class DomainForm(GenericForm):
    """ To preserver the order of the fields, we include all of them inthis parenyt calss and then we remove
    The ones we do not nedd in the add or edit form
    """

    name_help_text = _("Inserta un nombre de dominio válido (o un subdominio). ")
    mail_help_text = _("Activa la siguiente casilla si quieres que el correo electrónico "
                      "para este dominio sea gestionado por este servidor. Si el correo está "
                      "gestionado por otro servidor "
                      "deja esta casilla desactivada. Podrás cambiar esta opción en cualquier momento "
                      "desde la página de edición del dominio.")
    webmaster_help_text = _("Concede permisos de edición y escritura a una cuenta que tengas creda "
                            "para que pueda editar o subir archivos a la carpeta del dominio "
                           "que estará ubicada en /var/www/html/.<br> "
                           "Si en el desplegable no aparece ninguna opción, crea antes una cuenta "
                           "SSH (para acceso a la terminal) o SFTP (sin acceso a la terminal y enjaulada en su directorio personal)")
    dkim_help_text=_("Advertencia: si activas la siguiente casilla tendrás que incluir el registro "
                         "DKIM en los DNS de tu dominio. La falta del registro DKiM "
                         "puede generar problemas de entrega de tu correo electrónico.")


    webserver_help_text = _("Este dominio no tiene creada la configuración necesaria para ser accesible con los navegadores. Activando esta casilla, si la configuración DNS es correcta, se activará el acceso web con https y se creará la carpeta para alojar tu aplicación en /var/www/html/ ")
    headerWeb             = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Web"),tag="h3"))
    name        = forms.CharField(label=_('Nombre de dominio'),
                                 max_length=100, help_text=name_help_text, required=True)
    webServer    = forms.BooleanField(label=_('Servidor Web'),
                                     help_text=_('Marca esta casilla si quieres usar este dominio para publicar tu aplicación web.'),
                                     widget=widgets.LabelledCheckbox(label=_('Activar Servidor Web')), required=False)
    documentRoot     = forms.CharField(label=_('Carpeta raíz'), required=False,  help_text=_("Si dejas este campo en blanco la carpeta raíz será /vaw/www/html/nombre.de.dominio/ . Puedes añadir subcarpetas a  este directorio, por ejemplo insertando public_html/web. En este caso la carpeta raíz será /vaw/www/html/nombre.de.dominio/public_html/web/.  Este valor no se puede editar. Si quieres cambiarlo puedes elimiar el dominio y volverlo a crear. En caso de dudas, deja el espacio en blanco."))
    deleteWebroot = forms.BooleanField(label=_('Eliminar Carpeta del dominio'),
                              widget=widgets.LabelledCheckbox(label=_('Eliminar')), required=False)
    wpinstalled = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label= mark_safe("<i class='fa fa-wordpress medium-icon'></i> Wordpress"),tag="h5"))
    wordpressInstall    = forms.BooleanField(label= mark_safe("<i class=\"fa fa-wordpress medium-icon\"></i> %s" % _('Instalar Wordpress')),
                                     help_text=_('Si marcas esta casilla se instalará wordpress de forma automatizada. Si ya existe una carpeta no vacía para este dominio en /var/www/html/, la instalación no se llevará a cabo para no borrar de forma accidental ningún contenido.. Tendrias entonces que eliminar o mover manualmente los archivos que contiene para completar la instalación'),
                                     widget=widgets.LabelledCheckbox(label=_('Instalar Wordpress')), required=False)
    wpPass1 = forms.CharField(label=_('Contraseña para la cuenta de administración del Wordpress'), widget=forms.PasswordInput(), required=False,  help_text=_("El nombre de la cuenta será el mismo que utilizas para acceder al panel de control"))
    wpPass2 = forms.CharField(label=_('Repite la contraseña'), widget=forms.PasswordInput(), required=False )

    phpPool =  forms.ChoiceField( required = False, widget = forms.Select())

    headerPerm             = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Permisos"),tag="h3"))
    webmaster   = forms.ChoiceField(label=_('Webmaster (Cuenta con acceso de lectura/escrtura a los archivos)'),
                                    help_text=webmaster_help_text,required=False)
    acl = forms.BooleanField(label=  _('Activar ACL (Access Control List)'),
                                     help_text=_('Si marcas esta casilla se añadirán reglas de acceso a la carpeta del dominio. De esta forma tanto la cuenta webmaster como el servidor web tendrán permisos de escritura/lectura y podrás así modifcar archivos creados por la aplicación (por ejemplo, plugins instalados desde la interfaz gráfica de Wordpress).'),
                                     widget=widgets.LabelledCheckbox(label=_('Activar ACL')), required=False)

    headerMail             = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Correo"),tag="h3"))
    mail_server = forms.BooleanField(label=_('Activar servidor de correo para este dominio'),
                                     help_text=mail_help_text, required=False,
                                     widget=widgets.LabelledCheckbox(label=_('Activar servidor de correo')))
    dkim        = forms.BooleanField(label=_('Activar DKIM para este dominio'), help_text = dkim_help_text,
                                     required=False,
                                     widget=widgets.LabelledCheckbox(label=_('Activar DKIM')))
    def clean(self):
        cleaned_data = super().clean()
        name  = self.cleaned_data.get('name')
        wordpressInstall = self.cleaned_data.get("wordpressInstall")
        mail_server = self.cleaned_data.get("mail_server")
        # Only allow Wordpress installation if webservers is checked
        if self.cleaned_data.get('wordpressInstall') and not self.cleaned_data.get('webServer'):
            raise forms.ValidationError(_("Para instalr Wordpress tienes que activar el servidor web"))

        # If web server has been activated, need to check DNS for let's encrypt
        if self.cleaned_data.get('webServer'):
            dns_records = utils.get_dns_records(name)
            record_message = utils.check_dns_A_record(dns_records)
            if record_message['error']:
                if record_message['msg'] == 'NOT_OK':
                    link = reverse_lazy('domains-instructions')
                    dns_message = mark_safe(_("La configuración de los DNS no es correcta para activar el servidor web. Consulta instrucciones para corregirla <a href=\"%s\">aquí</a>." % link))
                else:
                    dns_message = record_message['msg']
                raise forms.ValidationError(dns_message)

        # If domain puppet module is locked, nootify user that another process is in progress.
        domain_status = utils.get_cpanel_local_status(self.request.ldap, 'domains')
        if ( domain_status!='ready'):
            raise forms.ValidationError(
            _("Hay otro proceso de configuración de dominio en curso. Para evitar perdida de datos o errores no se puede añadir otra tarea hasta que acabe el proceso en curso. Vuelve a hacer clic en el botón 'Guardar' pasado un minuto.")
            )
        if wordpressInstall and not self.cleaned_data.get('webServer'):
            raise forms.ValidationError(_("Para instalr Wordpress tienes que activar el servidor web"))

        #Worpdress Checking - If wordpress installation is checked, require a password
        if wordpressInstall:
            wpPass1 = self.cleaned_data.get("wpPass1")
            wpPass2 = self.cleaned_data.get("wpPass2")
            if not wpPass1 or not wpPass2:
                raise forms.ValidationError(_("Inserta dos veces la contraseña para la cuenta de administración del Wordpress"))
            if wpPass1 != wpPass2:
                raise forms.ValidationError(_("Las contraseñas para la cuenta de administración del Wordpress no coiniciden"))
            else:
                 validate_password(wpPass1)

    def __init__(self, *args, **kwargs):
        users = kwargs.pop('users')
        self.request = kwargs.pop('request')
        super(DomainForm,self).__init__(*args, **kwargs)
        #self.fields['webmaster'].choices = ((user, user ) for user in users)
        self.fields['webmaster'].choices = ((user, user if user != "nobody" else _("Ninguno")) for user in users)
        self.fields['webmaster'].choices.insert(0,(None, _('Asignar Webmaster')))
        #self.fields['webmaster'].choices.insert(1,('nobody', _('Ninguno')))
        self.fields['wpPass1'].widget.attrs = {
            'class'    : 'targetToggle'
        }
        self.fields['wpPass2'].widget.attrs = {
            'class'    : 'targetToggle'
        }

        self.fields['wordpressInstall'].widget.attrs = { 'data-multilink-display' : 'targetToggle' }
        pools = utils.get_available_pools(self.request.ldap)
        if pools:
            choices=[(None,_("Ninguno"))]
            for pool in pools:
                if pool["type"] == 'dynamic':
                    domains = len(pool["domains"])
                    choices.append((pool["ou"],pool["ou"]+' (%d)' % domains))
            self.fields[ 'phpPool' ].choices =choices
            # Crazy string creation to preserve transaltions
            # TOD: find a better way to preserevr __proxy__ format
            fpm_str = _('Si instalas un Wordpress o una aplicación PhP en este dominio '
                         'asígnale un pool propio para que se ejecute como un proceso separado reforzando así la seguridad. '
                         'El número entre parentesis indica cuantos dominios están ya asignados al pool. '
                         'Puedes añadir nuevos pools en esta pagina: ')
            link = '<a href="%s">+ Pools</a>' % reverse_lazy('php-fpm')
            self.fields[ 'phpPool' ].help_text= str(fpm_str) + link


class AddDomainForm(DomainForm):
    """ Form to add a new domain """


    def clean(self):
        cleaned_data = super().clean()
        documentRoot = self.cleaned_data.get('documentRoot')
        name  = self.cleaned_data.get('name')
        if (documentRoot and utils.not_valid_path(documentRoot)):
            raise forms.ValidationError(
            _("Los caracteres especiales no están permitidos para la carpeta raíz. Solo pueses utilizar los caracteres / - _ ")
            )
        if not re.match("^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,20}$", name):
            raise forms.ValidationError(
                _("%s no en un nombre de dominio válido" % name)
            )
        if name in self.domains:
            raise forms.ValidationError(
                _("El dominio %s ya está activado" % name)
            )
        if name == utils.get_server_host():
            raise forms.ValidationError(
                _("%s es el dominio actual del sistema" % name)
          )
        if name in self.domains_in_use:
            raise forms.ValidationError(
                _("El dominio %s está en uso por otra aplicación" % name)
          )

        mailman_domains = utils.get_mailman_domain_names()
        if name in mailman_domains and mail_server:
            raise forms.ValidationError(
                _("No puedes activar el servidor de correo para el dominio %s. Ya  está en uso por la aplicación Mailman." % name)
              )


    def __init__(self, *args, **kwargs):
        self.domains = kwargs.pop('domain_list')
        self.domains_in_use = kwargs.pop('domains_used_list')
        super(AddDomainForm,self).__init__(*args, **kwargs)
        del self.fields['deleteWebroot']
        del self.fields['wpinstalled']
        self.fields['name'].widget.attrs['pattern'] = "^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,20}$"
     

class EditDomainForm(DomainForm):
    old_dkim    = forms.BooleanField(widget=forms.HiddenInput(), required=False)

    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(field)s %(help_text)s </div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=False
        )

    def __init__(self, *args, **kwargs):
        self.domain_name = kwargs.pop('domain')
        self.webserver  = kwargs.pop('webserver')
        path = utils.get_document_root(self.domain_name)	
        super(EditDomainForm, self).__init__(*args, **kwargs)
        self.fields['name' ]= forms.CharField(
                    widget   = forms.HiddenInput(),
                    required = False,
                    initial = self.domain_name 
        )

        self.fields["documentRoot"] = customfileds.HeaderField(
                label="",
                required=False,
                label_suffix='',
                widget=widgets.HeaderWidget(label = mark_safe("<i class='fa fa-folder medium-icon'></i> %s" % path),
                tag="pre"))
        self.fields['documentRoot'].widget.attrs['readonly'] = True

        has_wordpress = False
        wp = utils.has_cms(self.domain_name)
        # Worpress marked as installed if status is enabled or install
        if(wp):
            has_wordpress = True if wp.type == 'wordpress' and (wp.status == 'enabled' or wp.status == 'install') else False  
        if has_wordpress:
            del self.fields['wordpressInstall']
            del self.fields['wpPass1']
            del self.fields['wpPass2']
            # Need to concatenate strings to apply translation
            dom_str =  _("La carpeta del Wordpress está en /var/www/html/")
            self.fields['wpinstalled'].help_text = dom_str + self.domain_name 
        else:
            del self.fields['wpinstalled']
            self.fields['wordpressInstall'].help_text=_('Si la carpeta del dominio ya contiene archivos la instalación no se llevará a cabo para no borrar de forma accidental ningún contenido. Tendrías entonces que eliminar o mover manualmente los archivos que contiene. Recibirás un correo electrónico una vez completada la instalación.')

        self.fields['webServer'].widget.attrs = { 'data-link-hide' : 'id_deleteWebroot'}
        self.fields['documentRoot'].widget.attrs['readonly'] = True
        self.din_url = reverse_lazy('dns')+'?domain='+self.domain_name
        help_msg = _('Activa esta casilla si quieres que el correo '
                  'electrónico para este dominio sea gestionado por este servidor. '
                  'Puedes averiguar cual es la configuración de DNS en esta pagina: ')
        dns_str =  _("Ver DNS")
        link =" <a href=\"%(link)s\">" + str(dns_str) +"</a>." % ({'link': self.din_url})
        help_msg += link
        mailman_domains = utils.get_mailman_domain_names()
        if self.domain_name in mailman_domains:
            self.fields['mail_server'].help_text = _('No se puede activar el servidor de correo. Este dominio está en suo por la aplicación Mailman')
        else:
            self.fields['mail_server'].help_text = help_msg
        # Dynamic help_textx content
        ip= utils.get_server_ip()
        ip_text = _('Para poder activar el servidor web es necesario que los DNS para este dominio apunten a la IP de este servidor: ')
        self.fields['webServer'].help_text= ip_text + ip
        self.fields['deleteWebroot'].help_text=_('Si el servidor web está desactivado puedes eliminar la carpeta del dominio en /var/www/html/.\
                                     Se guardará una copia en la papelera hasta que la elimines definitivamente.')

    def clean(self):
        # Form has changed ?
        if not self.changed_data:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
            )

        # If domain puppet module is locked, nootify user that another process is in progrees.
        domain_status = utils.get_cpanel_local_status(self.request.ldap, 'domains')
        if self.cleaned_data.get('webServer') and not self.cleaned_data.get('webmaster'):
            raise forms.ValidationError(
                _("Asigna una cuenta Webmaster para el dominio")
              )
        if self.cleaned_data.get('wordpressInstall') and not self.cleaned_data.get('webServer'):
            raise forms.ValidationError(_("Para instalr Wordpress tienes que activar el servidor web"))
            # If web server has been activated, need to check DNS for let's encrypt
        if self.cleaned_data.get('webServer'):
            dns_records = utils.get_dns_records(self.domain_name)
            record_message = utils.check_dns_A_record(dns_records)
            if record_message['error']:
                if record_message['msg'] == 'NOT_OK':
                    link = reverse_lazy('dns')+'?domain='+self.domain_name
                    dns_message = mark_safe(_("La configuración de los DNS no es correcta para activar el servidor web. Consulta instrucciones para corregirla <a href=\"%s\">aquí</a>" % link))
                else:
                    dns_message = record_message['msg']
                raise forms.ValidationError(dns_message)
        #Worpdress Checking - If wordpress installation is checked, require a password
        if self.cleaned_data.get('wordpressInstall'):
            wpPass1 = self.cleaned_data.get("wpPass1")
            wpPass2 = self.cleaned_data.get("wpPass2")
            if not wpPass1 or not wpPass2:
                raise forms.ValidationError(_("Inserta dos veces la contraseña para la cuenta de administración del Wordpress"))
            if wpPass1 != wpPass2:
                raise forms.ValidationError(_("Las contraseñas para la cuenta de administración del Wordpress no coiniciden"))
            else:
                 validate_password(wpPass1)

        # Force user to choose a webmaster
        if ('webServer' in self.changed_data or 'webmaster' in self.changed_data or 'phpPool' in self.changed_data or  \
            'deleteWebroot' in self.changed_data or 'wordpressInstall' in self.changed_data or 'acl' in self.changed_data) and (domain_status!='ready'):
            raise forms.ValidationError(
                _("Hay otro proceso de configuración de dominio en curso. Para evitar perdida de datos o errores no se puede añadir otra tarea hasta que acabe el proceso en curso. Vuelve a hacer clic en el botón 'Guardar' pasado un minuto.")
            )
            dns_records = utils.get_dns_records(self.domain_name)

        cleaned_data = super().clean()

class EmailForm(GenericForm):
    """ Form to add a new email account """

    email      = forms.CharField(label=_('Nombre de la cuenta'), required=True,
                                 help_text=_("Introduce el nombre de la cuenta. Por ejemplo si quieres tener un correo "
                                             "'user@ejemplo.com' el nombre sería 'user'"))
    at         = forms.CharField(label="",required=False)
    domain     = forms.ChoiceField(label=_('Dominio'), required=True,
                                   help_text=_("Introduce el dominio del correo. En el ejemplo anterior "
                                               "escogerías 'ejemplo.com'"))
    password   = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(), required=True)
    password_2 = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=True)
    name       = forms.CharField(label=_('Nombre'), required=True)
    surname    = forms.CharField(label=_('Apellidos'), required=True)
    quota      = forms.CharField(label=_('Quota'), required=False, help_text=_("Espacio máximo asignado en GB. Deja vacío o inserta 0 para no asignar límite"))
    def clean(self):
        super().clean()
        email = self.cleaned_data.get("email")
        quota = self.cleaned_data.get("quota")
        qu = quota.replace(",",".")
        if not qu.replace(".","").isdigit() or float(qu) < 0.0:
            raise forms.ValidationError(
                _("El valor Quota tiene que ser un número mayor o igual a cero.")
            )
        if not utils.validate_username(email):
            raise forms.ValidationError(
                _("El nombre que has escogido usa caracteres no permitidos.")
            )
            context['display_form'] = True

    def __init__(self, *args, **kwargs):
        domains = kwargs.pop('domains')
        role = kwargs.pop('role')
        super(EmailForm,self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs = {
            'pattern' : '^[\w]{1,}[\w.+-_]{0,}'
        }
        self.fields['domain'].choices = ((domain, domain) for domain in domains)
        self.fields['at'].widget.attrs['readonly'] = True


class EditEmailForm(GenericForm):
    """ Form to add a new email account """

    forward_help_text  = _("Puedes reenviar los correos electrónicos entrantes a "
                         "una o más cuentas de correo alternativas. Si quieres "
                         "que se reenvíen a múltiples cuentas, separa cada una "
                         "de ellas con una coma (user1@example.com,user2@example.com. "
                         "Recuerda que si quieres seguir recibiendo una copia de los "
                         "correos entrantes en tu cuenta actual "
                         "también tendrás que incluirla en el listado.")

    givenName      = forms.CharField(label=_('Nombre'), required=True)
    sn             = forms.CharField(label=_('Apellidos'), required=True)
    password       = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(),
                                         required=False)
    password_2     = forms.CharField(label=_('Vuelve a introducir la contraseña'),
                                         widget=forms.PasswordInput(), required=False)
    quota      = forms.CharField(label=_('Quota'), required=False, help_text=_("Espacio máximo asignado en GB. Deja vacío o inserta 0 para no asignar límite"))

    forwardActive  = forms.BooleanField(label=_("Reenvío automático"), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Activar renvío automático')))
    maildrop       = forms.CharField(label=_('Cuentas de destino para el reenvío automático'),
                                         help_text=forward_help_text, required=False)
    vacationActive = forms.BooleanField(label=_("Respuesta automática"), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Activar respuesta automática')))
    vacationinfo   = forms.CharField(label=_('Mensaje de respuesta automática'), required=False,
                                        widget=forms.Textarea,
                                        help_text=_('Inserta el texto del mensaje de respuesta automática.'))

    def clean(self):
        super().clean()
        # Form has changed ?
        if not self.changed_data:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
            )
        quota = self.cleaned_data.get("quota")
        qu = quota.replace(",",".") 
        if not qu.replace(".","").isdigit() or float(qu) < 0.0:
            raise forms.ValidationError(
                _("El valor Quota tiene que ser un número mayor o igual a cero.")
            )

        forward_active = self.cleaned_data.get("forwardActive")
        maildrop = self.cleaned_data.get("maildrop")
        validate_email(self.get_email)
        if (forward_active):
            emails   = maildrop.split(",")
            for email in emails:
                email = email.strip()
                """
                if email and not re.match("[^@]+@[^@]+\.[^@]+", email):
                    raise forms.ValidationError(
                        _("Parece que uno de los correos que has introducido no es válido.")
                    )
                """
                # Check forward email format with django builin
                validate_email(email)

    def __init__(self, *args, **kwargs):
        self.get_email = kwargs.pop('mail_account')
        role = kwargs.pop('role')
        super(EditEmailForm,self).__init__(*args, **kwargs)
        if role == "email":
            self.fields['quota'].widget.attrs = {
                'readonly' : True,
                'class'    : 'disabled'
            }
            self.fields['quota'].help_text = _("Espacio máximo permitido en GB")

        self.fields['forwardActive'].widget.attrs = { 'data-link-display' : 'id_maildrop' }
        self.fields['vacationActive'].widget.attrs = { 'data-link-display' : 'id_vacationinfo' }

class GenericUserForm(GenericForm):
    """ Form to add a new email account """
    
    sftp_help_text      = _("Requerido para que las cuentas webmaster puedan acceder al servidor para editar los archivos.")
    sftp_help_text_full = _("""Requerido para que las cuentas webmaster puedan editar los archivos.
                             Para poder desactivarlo necesitas antes quitarle el rol de webmaster
                             para los siguientes dominios: """)
    sshSftp_help_text   = _("SFTP otrorga acceso al servidor sin terminal (solo clientes sftp tipo FileZilla), limitando la cuenta a su directorio personal. Con SSH la cuenta dispondrá de la terminal para lanzar comandos. Una vez asignada una de las dos opciones solo la podrás desactivar pero no cambiar una por otra.")
    acces_type_label    =   _('Activar acceso')
    jitsi_help_text      = _("Requerido para poder iniciar salas de videoconferencia en Jisti Meet.")
    username       = forms.CharField(label=_('Nombre de usuarix'), required=True)
    name           = forms.CharField(label=_('Nombre'), required=False)
    surname        = forms.CharField(label=_('Apellidos'), required=False)
    email          = forms.EmailField(label=_('Correo electrónico'), required=True,
                                      help_text=_("Introduce un correo electrónico válido. "
                                                  "Este campo se autocompletará con correos existentes en el sistema. "
                                                  "Si quieres ver la lista completa haz doble clic en el campo de texto (vacío)."))
    password       = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(), required=True)
    password_2     = forms.CharField(label=_('Vuelve a introducir la contraseña'),
                                     widget=forms.PasswordInput(), required=True)
    # Leave this field for user edition form.
    sshd           = forms.BooleanField(label=acces_type_label, required=False,
                                        help_text=sftp_help_text,
                                        widget=widgets.LabelledCheckbox(label=acces_type_label))
    sshSftp        = forms.ChoiceField(label=_("Acceso SSH o SFTP al servidor"),required=False,help_text=sshSftp_help_text,widget=forms.Select) 
    sshkey           = forms.CharField(label=_('Clave SSH'), required=False, widget=forms.Textarea,
                                       help_text=_('Inserta la clave pública SSH (una por línea).'))
    home_dir       = forms.CharField(label='Directorio personal', required=False)
    openvpn       = forms.BooleanField(label=_('Activar cuenta VPN'), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Activar cuenta VPN')))
    instructions   = forms.BooleanField(label=_('Instrucciones de configuración VPN'), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Enviar instrucciones')),
                                        help_text=_("Envía un correo con los archivos de configuración y las instrucciones "
                                                    "para configurar el cliente VPN. <br/>Advertencia: Las instrucciones incluyen todos los "
                                                    "datos necesarios menos la contraseña, que por razones de seguridad debes proporcionar "
                                                    "por otro canal."))
    apache         = forms.BooleanField(label=_('Activar PHPMyAdmin'), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Activar PHPMyAdmin')))
    jitsi           = forms.BooleanField(label=_('Activar Jitsi Meet '), required=False,
                                        help_text=jitsi_help_text,
                                        widget=widgets.LabelledCheckbox(label=_('Activar Jitsi Meet')))

    def clean(self):
        super().clean()
        username     = self.cleaned_data.get("username")
        openvpn     = self.cleaned_data.get("openvpn")
        instructions = self.cleaned_data.get("instructions")
        email        = self.cleaned_data.get("email")
        existing_user = False
        # Form has changed ?
        if not self.changed_data:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
            )
        # check if user already exists
        # when creating a new user
        if not self.is_edit_form:
            # if user is not present in ldap, also check in system before validate
            if not utils.validate_username(username):
                raise forms.ValidationError(_("Nombre no válido. Solo se aceptan letras mayúsculas, minúsculas, cifras y los signos \'-',\'_\' y \'.\' siempre que no estén al principio")
            )
            try:
                existing_user = pwd.getpwnam(username)
            except Exception as e:
                utils.p("✕ view_users.py", "User does not exist.: ", e)
            if existing_user:
                raise forms.ValidationError(
                    _("Ya existe una cuenta con ese nombre. Has de escoger un nombre distinto.")
                )
                context['display_form'] = True
        # check if there's email to send notifications if checked
        if instructions and not email:
        ### It should never happen....email is a required field !!!!!!
            raise forms.ValidationError(
                _("Para poder recibir las instrucciones de configuración de la VPN, es necesaria una dirección de correo. "
                  "Introduce una dirección de correo o desactiva el envío de instrucciones.")
            )
            context['display_form'] = True
        if not openvpn:
            self.cleaned_data['instructions'] = False
        return {} 


class UserForm(GenericUserForm):
    #sftp_help_text      = "Requerido para que las cuentas webmaster puedan acceder al servidor para editar los archivos."
    sftp_help_text_full = _("""Requerido para que las cuentas webmaster puedan editar los archivos.
                             Para poder desactivarlo necesitas antes quitarle el rol de webmaster
                             para los siguientes dominios: """)
    """
    acces_type_label    =   _('Activar acceso SFTP')
    sshd           = forms.BooleanField(label=acces_type_label, required=False,
                    help_text=sftp_help_text,
                    widget=widgets.LabelledCheckbox(label=acces_type_label))
    """
    def __init__(self, *args, **kwargs):
        choices=[
            ("none",_("Sin Acceso")),
            ("sftp",_("Acceso solo por sftp")),
            ("ssh",_("Acceso  SFTP y SSH")),
        ]

        emails       = kwargs.pop('emails')
        self.is_edit_form  = kwargs.pop('edit_form')
        self.usertype =  kwargs.pop('usertype')
        # Store ldap in the form itself to make it accesible from clean method
        # only when creating a new user
        domains = []
        if 'domains' in kwargs:
            domains = kwargs.pop('domains')
        available_services = kwargs.pop('services')
        super(UserForm,self).__init__(*args, **kwargs)
        self.fields['email'].widget = widgets.ListTextWidget(
            data_list=emails,
            name='mail',
            attrs={
                'autocomplete':'off'
            }
        )
        self.fields['openvpn'].widget.attrs = {
            'data-link-display' : 'id_instructions'
        }

        # if the user is webmaster disable sftp field and
        # show a different help_text
        if domains:
            self.sftp_help_text = self.sftp_help_text_full
            for domain in domains:
                self.sftp_help_text += "%s, " % domain
            self.fields['sshd'].widget.attrs = {
                'readonly' : True,
                'class'    : 'disabled'
            }
            self.fields['sshd'].help_text = _(self.sftp_help_text)

        # if the user hasn't a sftp account don't show home_dir
        self.fields['home_dir'].widget.attrs = {
            'readonly' : True,
            'class'    : 'disabled'
        }
        has_home= kwargs.get('initial').get('home_dir')
        sshd = kwargs.get('initial').get('sshd')
        if not sshd:
            del self.fields['home_dir']

        # if openvpn is not activated in the server hide related_fields
        if not 'openvpn' in available_services:
            del self.fields['openvpn']
            del self.fields['instructions']
        # if phpmyadmin is not activated in the server hide related_fields
        if not 'phpmyadmin' in available_services:
            del self.fields['apache']
	# if jitsi is not activated in the server hide related_fields
        if not 'jitsi' in available_services:
            del self.fields['jitsi']

        # if editing a user instead of creating it
        if self.is_edit_form:
            # dont force to input password again if not needed
            self.fields['password'].required   = False
            self.fields['password_2'].required = False
            # set username as a hidden and disabled field
            self.fields['username'].widget.attrs['readonly'] = True 
            self.fields['username'].widget.attrs['class'] = 'disabled'

            # If is a new user check if has already a user type
            if self.usertype=='none':
                self.fields['sshSftp'].choices = choices
                del self.fields['sshd']
            else:
                acces_type_label    =   _('Activar acceso SFTP')
                del self.fields['sshSftp']
                if self.usertype=='ssh':
                    acces_type_label    =   _('Activar acceso SSH')
                self.fields['sshd'].label=acces_type_label
        else:
            self.fields['sshSftp'].choices = choices
            del self.fields['sshd']

        """
        elif 'home_dir' in self.fields:
            del self.fields['home_dir']
        """

class SuperuserForm(GenericForm):
    """ Form to edit superuser account """

    username         = forms.CharField(label=_('Nombre de la cuneta no editable (Para autenticación SFTP/SSH)'), required=True)
    name             = forms.CharField(label=_('Nombre'), required=True)
    surname          = forms.CharField(label=_('Apellidos'), required=True)
    email            = forms.EmailField(label=_('Correo electrónico'), required=True,
                                      help_text=_("Introduce un correo electrónico válido. "
                                                  "Este campo se autocompletará con correos existentes en el sistema. "
                                                  "Si quieres ver la lista completa haz doble clic en el campo de texto (vacío)."))
    password         = forms.CharField(label=_('Contraseña nueva del superuser'), widget=forms.PasswordInput(), required=False)
    password_2       = forms.CharField(label=_('Vuelve a introducir la contraseña nueva del superuser'), widget=forms.PasswordInput(), required=False)
    home_dir         = forms.CharField(label=_('Directorio personal'), required=False)
    sshkeys          = forms.CharField(label=_('Clave SSH'), required=False, widget=forms.Textarea,
                                      help_text=_('Inserta la clave pública SSH.'))

    openvpn          = forms.BooleanField(label=_('Activar cuenta VPN'), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Activar VPN')))
    instructions     = forms.BooleanField(label=_('Instrucciones de configuración VPN'), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Enviar instrucciones')),
                                        help_text=_("Envía un correo con los archivos de configuración y las instrucciones "
                                                    "para configurar el cliente VPN. <br/>Advertencia: Las instrucciones incluyen todos los "
                                                    "datos necesarios menos la contraseña, que por razones de seguridad debes proporcionar "
                                                    "por otro canal."))
    current_password = forms.CharField(label=_('Introduce tu contraseña de acesso al panel de control'), widget=forms.PasswordInput(), required=True,  help_text=_("Para editar esta cuenta es necesario que insertes la contraseña que utilizas para acceder al panel de control"))

    apache           = forms.BooleanField(label=_('Activar PHPMyAdmin'), required=False,
                                                    widget=widgets.LabelledCheckbox(label=_('Activar PHPMyAdmin')))

    def __init__(self, *args, **kwargs):
        emails = kwargs.pop('emails')
        available_services = kwargs.pop('services')
        super(SuperuserForm,self).__init__(*args, **kwargs)
        self.fields['email'].widget = widgets.ListTextWidget(
            data_list=emails,
            name='mail',
            attrs={'autocomplete':'off'}
        )
        self.fields['openvpn'].widget.attrs = {
            'data-link-display' : 'id_instructions'
        }
        self.fields['username'].widget.attrs['readonly'] = True
        self.fields['username'].widget.attrs['class'] = 'disabled'
        self.fields['home_dir'].widget.attrs['readonly'] = True
        self.fields['home_dir'].widget.attrs['class'] = 'disabled'
        if not 'openvpn' in available_services:
            del self.fields['openvpn']
            del self.fields['instructions']
        if not 'phpmyadmin' in available_services:
            del self.fields['apache']

    def clean(self):
        super().clean()
        """
        If noting has changed do not perform save post
        Admin password is required, so just check if changed_data is ==1
        """
        if len(self.changed_data) == 1:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
                )
        current_pwd  = self.cleaned_data.get('current_password')
        new_password = self.cleaned_data.get('password')

class PostmasterForm(GenericForm):
    """ Form to edit postmaster account """

    username   = forms.CharField(label=_('Nombre de la cuenta'), required=True)
    password   = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(), required=True)
    password_2 = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=True)

    def __init__(self, *args, **kwargs):
        super(PostmasterForm,self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['readonly'] = True
        self.fields['username'].widget.attrs['class'] = 'disabled'


class NotificationForm(GenericForm):
    """ Form to add a new email account """
    log_help_text =""
    log_custom_help_text = _("Tu sistema envia periódcamente correos con información "
                              "sobre su estado como fallos de los servicios, errores o actualizaciones (Logs del sistema).<br>"
                              "A continuación puedes elegir quién recibirá dichos correos."
                              "Si cambias esta configuración se bloqueará el acceso al panel de control durante unos minutos. "
                              "Todos las personas que tengan una sesión activa serán forzados a salir y redireccionados a una página "
                              "en la que se mostrará el estado de la operación. Cuando el proceso termine, se activará el formulario para volver a acceder." )

    email_help_text = _("Puedes cambiar esta configuración y elegir una de las cuentas de correo activadas "
                        "en tu sistema para que sea el remitente de las notificaciones. "
                        "Para cambiar este valor, elige un correo electrónico disponible en el listado") 
    email = forms.ChoiceField(label=_('Remitente de las notificaciones'), required=True,help_text=email_help_text)
    log_server= forms.ChoiceField(label=_("Configurar destinatario de Logs"),help_text=log_custom_help_text,widget=forms.RadioSelect)
    emailAddress = forms.EmailField(label=_('Correo electrónico'), required=False,
                            help_text=_('Introduce una dirección de correo a la que quieres recibir los logs.'
                            'Si no introduces ninguna, se utilizará el correo asociado a la cuenta de administración del panel de control'))

    def __init__(self, *args, **kwargs):
        emails = kwargs.pop('emails')
        maintenance = kwargs.pop( 'maintenance')
        request =  kwargs.pop('request')
        both_string="Yo y el equipo técnico de Maadix"
        if 'zeyple' in  request.enabled_services:
            both_string+=" - Esta opción no envía logs cifrados"
        mailchoices=[
            ('false',_("Solo el equipo técnico de MaadiX")),
            ('true',_("Solo yo")),
            ('both',_(both_string)),
        ]

        # Store this value to compare in valid_form if it has changed
        # if so, a lock_panel is needed
        #self.log_mail_status = kwargs.pop('log_mail_status')
        super(NotificationForm,self).__init__(*args, **kwargs)
        self.fields['log_server'].choices = mailchoices
        self.fields['email'].choices = [ (email,email) for email in emails ]
        #self.fields['email'].choices.insert(0,(None, _('Selecciona un correo')))
        # If puppet status is not ready do not show the receive logs checkbos
        # It requires puppet to be run
        if maintenance != 'ready':
            del self.fields['log_server']

    def clean(self):
        super().clean()
        # Form has changed ?
        if not self.changed_data:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
            )


class FqdnForm(GenericForm):
    """ Form to add a new domain """
    fullname = utils.get_server_hostname()+'.example.com'
    name_help_text = _("inserta el dominio que quieres asignar al servidor. Para %s inserta solo example.com" % fullname)
    log_help_text = _("Activa esta casilla si quieres recibir a tu cuenta de correo, "
                      "informes diarios sobre el estado del sistema, fallos en los servicios, "
                      "errores o actualizaciones. Podrás cambiar esta configuración "
                      "en cualquier momento desde la página de Notificaciones.")

    name        = forms.CharField(label=_('Nuevo dominio del servidor'),
                                 max_length=100, help_text=name_help_text, required=True)
    #log_server = forms.BooleanField(label=_('Recibir Logs del sistema por correo electrónico'),
    #                                 help_text=log_help_text, required=False,
    #                                 widget=widgets.LabelledCheckbox(label=_('Recibir Logs')))

    def clean(self):
    ##############esto viene de get_form
        cleaned_data = super().clean()
        servername = self.servername
        domain  = self.cleaned_data.get('name')
        name = '%s.%s' % (servername, domain)
        request = self.request
        installed_apps = request.enabled_services
        memory = psutil.virtual_memory()
        # We want to checck available memory, so user has the chance to reboot server to increase free memory
        # Some free memory is needed to restart services impacted by FQDN change (Apache, Postfix, VPN)
        if(memory.available/(1024*1024) < 200 ):
            raise forms.ValidationError(
                _("No tienes suficiente memoria. Reinicia eñ servidor para liberar memoria y vuelve a intentarlo")
            )
        if not re.match("^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,20}$", name):
            context['display_form'] = True
            raise forms.ValidationError(
                _("%s no es un nombre de dominio válido" % name )
            )
        if name in self.domains:
            raise forms.ValidationError(
                _("El dominio %s ya está activado" % name)
            )
            context['display_form'] = True
        if name in utils.get_server_host():
            raise forms.ValidationError(
                _("%s ya es el dominio actual del sistema" % name)
          )
            context['display_form'] = True
        if name in self.domains_in_use:
            raise forms.ValidationError(
                _("El dominio %s está en uso por otra aplicación" % name)
          )
            context['display_form'] = True
        mailman_domains = utils.get_mailman_domain_names()
        if name in mailman_domains and mail_server:
            raise forms.ValidationError(
                _("El dominio %s está en uso por la aplicación Mailman." % name)
          )
        dns_records = utils.get_dns_records(name)
        record_a = utils.check_dns_A_record(dns_records)
        if record_a['error']:
            raise forms.ValidationError(
                _("La configuración de los DNS para El dominio %s no es correcta." % name)
          )
            context['display_form'] = True


    def __init__(self, *args, **kwargs):
        """ Send data to clean method for validation """
        self.servername = kwargs.pop('servername')
        self.domains = kwargs.pop('domain_list')
        self.domains_in_use = kwargs.pop('domains_used_list')
        self.request = kwargs.pop('request')
        super(FqdnForm,self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs = { 'placeholder' : 'example.com (sin %s)' % self.servername }


class EditAppForm(forms.Form):
    """ Form to edit user input dependencies for applications """

    def __init__(self,data=None, *args, **kwargs):
        self.request        = kwargs.pop('request')
        self.application_id = kwargs.pop('application_id')
        self.fields_data    = kwargs.pop('fieldlist')
        maintenance         = kwargs.pop('maintenance')
        super(EditAppForm, self).__init__(data,*args, **kwargs)
        
        for field in self.fields_data:
            fid      = field['fid']
            label    = field['label']
            helptext = field['helptext']
            widget   = field['widget']
            editable = field['editable']
            choices  = []
            field['required'] = field['required_if_installed']

            if widget == 'text':
                self.fields[ fid ] = forms.CharField(
                    label    = label,
                    required = field['required'],
                    help_text = helptext
                )
            elif widget == 'password': 
                self.fields[ fid ] = forms.CharField(
                    required = field['required'],
                    widget   = forms.PasswordInput(render_value=True),
                    help_text = helptext
                )
            elif widget == 'select':
                choices = [ line.split(':') for line in field['default_value'].split('\n') ]
                default_value = ((
                    choice[0].strip(), choice[1].strip() 
                ) for choice in choices )
                self.fields[ fid ] = forms.ChoiceField(
                     label = label,
                     required = field['required'],
                     choices = default_value,
                     widget = forms.Select(),
                     help_text = helptext
                 )
            elif widget == 'url':
                self.fields[ fid ] = forms.URLField(
                    required = field['required'],
                    label = label,
                    help_text = helptext
                )
            elif widget == 'fqdn':
                self.fields[ fid ] = forms.CharField(
                    label = label,
                    required = field['required'],
                    help_text = helptext
                )
           
            else:
                self.fields[ fid ] = forms.CharField(
                    label = label,
                    required = field['required'],
                    help_text = helptext

                )

            self.fields[fid].widget.attrs = {
                'data-app' : self.application_id, 
            } if not maintenance else {
                'readonly' : 'readonly' 
            }

    def clean(self):
        if not self.changed_data:
            raise forms.ValidationError(_("No has cambiado ningún parámetro"))
        message      = ''
        cleaned_data = super().clean()
        for field in self.fields_data:
            value = self.cleaned_data.get(field['fid']) 
            if value:
                widget = field['widget']
                if field['fid'] in  self.changed_data:
                    if widget== 'fqdn_nodns':
                        if not utils.validate_domain_format(value):
                            message = _("Formato de dominio no válido")
                    elif widget.startswith('fqdn'):
                        message = utils.is_valid_domain(
                            self.request.ldap,
                            domain    = self.cleaned_data.get("domain"),
                            check_dns = widget == 'fqdn'
                        )
                    elif widget == 'password' and ('coturn' in self.application_id or 'onlyoffice' in self.application_id):
                        if not utils.only_alphanumeric_characters(value):
                            message = _("Los caracteres especiales no están permitidos para esta contraseña")
                        elif len(value)<10:
                            message = _(
                                'La contraseña ha de tener 10 caracteres '
                                'como mínimo'
                            )      
        if message:
             raise forms.ValidationError(_(message))
        return cleaned_data
        
class InstallAppForm(GenericForm):
    """ Form to edit user input dependencies for applications """
    
    def __init__(self,data=None, *args, **kwargs):
        self.services_available = kwargs.pop('services_available')
        self.request = kwargs.pop('request')
        super(InstallAppForm, self).__init__(data,*args, **kwargs)
        for service in self.services_available:
            id = service['settings']['id']
            self.fields[id] = forms.BooleanField(
                label="",  
                required=False, 
                widget=widgets.LabelledCheckbox(
                    label=_('Seleccionar')
                )
            ) 
            self.fields[id].widget.attrs = { 'data-show' : id }
            
            # App dependencies
            # Apps
            for app in service['dependencies']:
                self.fields['dependency-%s' % id ] = forms.CharField(
                    widget=forms.HiddenInput(), 
                    required=False
                )
                self.fields['dependency-%s' % id ].widget.attrs={
                    'class' : 'dependency_%s_group' % id,
                    'value' : app['id'],
                }
            # Fields
            for field in service['fieldlist']:
                # Fieldname
                fieldname = 'input-%s-%s' % ( id, field['fid'] )
                widget = field['widget']
                default_value = field['default_value'] 
                # Set up fields according to provided widget
                if widget == 'password': 
                    self.fields[ fieldname ] = forms.CharField(
                        required = False,
                        widget = forms.PasswordInput(render_value=True),
                        initial = default_value,
                    )
                    if (id != 'coturn'):
                        fieldname = 'input-%s-%s-2' % ( id, field['fid'] )
                        self.fields[ fieldname ] = forms.CharField(
                            required = False,
                            label=_("Repetir Contraseña"),
                            widget = forms.PasswordInput(render_value=True),
                            initial = default_value,
                        )

                elif widget == 'select':
                    choices = [ line.split(':') for line in default_value.split('\n') ]
                    default_value = ((
                        choice[0].strip(), choice[1].strip() 
                    ) for choice in choices )
                    self.fields[ fieldname ] = forms.ChoiceField(
                         required = False,
                         choices = default_value,
                         widget = forms.Select()
                     )
                elif widget == 'url':
                    self.fields[ fieldname ] = forms.CharField(
                        required = False,
                        widget = forms.URLInput(),
                        initial = default_value
                    )
                # Rest of fields (char, fqdn, fqdn_nodns)
                else:
                    self.fields[ fieldname ] = forms.CharField(
                        required = False,
                        initial = default_value
                    )
                self.fields[ fieldname ].widget.attrs = {
                    'data-service' : id,
                    'class'        : 'dependency__input',
                }

            # Users 
            for user in service['users']:
                fieldname = 'system-%s-%s' % ( id, user['name'] )
                self.fields[ fieldname ]= forms.CharField(
                    widget   = forms.HiddenInput(), 
                    required = False
                )
                self.fields[ fieldname ].widget.attrs = {
                    'value' : user['name'],
                }
                

    """ This is a dynamically generated form and we the clean method is not aware of
    the fields it is containing.
    We get all available apps form the 'services' dict and check which
    have been marked for installation, as well as dependecies for each of them, to 
    get their input fields and values.
    """
        
    def clean(self):
        
        errors          = {}
        cleaned_data    = super().clean()
        form_domains =[]
        ram_required = 0
        disk_required = 0
        #Store dependencies in list , to finalyy add resources required
        deps_names = []
        total_apps = self.request.enabled_services
        apps_to_install = []
        for service in self.services_available:
            
            appname     = service['settings']['id']
            resources   = service['resources']
            
            if self.cleaned_data.get(appname):
                # Sum resources required by app
                apps_to_install.append(appname)
                #ram_required = ram_required + int(resources['ram'])
                #disk_required = disk_required + int(resources['disk'])
                # Apps
                for app in service['dependencies']:
                    fieldname = 'dependency-%s'% appname
                    dep_group = self.cleaned_data.get(fieldname)
                    # Sum resources requires by dependencies
                    if app['id'] not in total_apps:
                        apps_to_install.append(app['id'])
                    #if app['id'] not in deps_names:
                    #    deps_names.append(app['id'])
                # Fields
                for field in service['fieldlist']:
                    fieldname  = 'input-%s-%s' % ( appname, field['fid'] )
                    value      = self.cleaned_data.get(fieldname)
                    widget     = field['widget']
                    message    = None
                    if value:
                        if widget== 'fqdn_nodns':  
                            if not utils.validate_domain_format(value):
                                message = _("Formato de dominio no válido")
                        elif widget.startswith('fqdn'):
                            message = utils.is_valid_domain( 
                                self.request.ldap, 
                                domain    = value,
                                check_dns = widget == 'fqdn'
                            )
                            if value in form_domains:
                                message = _('No puedes utilizar el mismo dominio/subdominio para diferentes aplicaciones. Cada aplicación necesita su propio dominio'
                                 ) 
                            else:
                                form_domains.append(value)
                        elif widget == 'url' :
                            try:
                                forms.URLField().clean(value)                              
                            except:
                                message = _(
                                    'Has de introducir una URL válida, cómo http://example.com'
                                )
                        elif widget == 'password' and (appname == 'coturn' or appname == 'onlyoffice'):
                            if not utils.only_alphanumeric_characters(value):
                                message = _(
                                    'Los caracteres especiales no están permitidos '
                                    'para esta contraseña'
                                )
                            elif len(value)<10:
                                message = _(
                                    'La contraseña ha de tener 10 caracteres '
                                    'como mínimo'
                                )
                        elif widget == 'password':
                            field1= 'input-%s-%s' % ( appname, field['fid'] ) 
                            field2= 'input-%s-%s-2' % ( appname, field['fid'] )
                            value1 = self.cleaned_data.get(field1)
                            value2 = self.cleaned_data.get(field2)
                            if(value1 != value2):
                                message = _(
                                    'Las contraseñas no coinicden'
                                )
                            else:
                                message = validate_password(value1)

                            
                        elif utils.not_valid_characters(value): 
                            message = _("El formato que has introducido no es válido")

                    elif field['required']: 
                        message = _("Campo requerido.")

                    if message:
                        errors[fieldname] = message
                        print(errors)
                
                # Users 
                for user in service['users']:
                    fieldname     = 'system-%s-%s' % ( id, user['name'] )
                    existing_user = '';
                    sysuser       = self.cleaned_data.get(fieldname)
                    if sysuser: 
                        try:
                            existing_user = pwd.getpwnam(sysuser)
                        except Exception as e:
                            print(e)
                    if existing_user:
                        errors[fieldname] = _(
                            'La cuenta %s, reservada para esta aplicación, '
                            'ha sido creada en el sistema. Elimínala para '
                            'poder proceder' % sysuser
                        )
        # Get data about apps from API
        available_resources = utils.get_remaining_resources(self.request.release.get('configurations'), apps_to_install ) 
        if available_resources['memory'] <=0:
            raise forms.ValidationError(_("No tienes memoria suficiente para proceder con la instalación. "
                                        "Para liberar memoria puedes desactivar alguna aplicación que no estés utilizando"))
        if available_resources['disk'] <=0:
            raise forms.ValidationError(_("No tienes espacio en disco suficiente para proceder con la instalación."
                                        "Elimina archivos en desuso o entra en la pestaña Papelera para liberar espacio") )

        if errors.keys():    
            self.add_error( None, _("Algún dato no es válido. Corrige los errores marcados en rojo") )
            for key, value in errors.items():
                self.add_error(key, value)
        return self.cleaned_data


class UpdateAppForm(GenericForm):

    """ Form to edit user input dependencies for applications """
    
    def __init__(self,data=None, *args, **kwargs):
        all_services      = kwargs.pop('all_services')
        enabled_services  = kwargs.pop('enabled_services')
        disabled_services = kwargs.pop('disabled_services')
        removable_servs   = kwargs.pop('removable_servs')
        not_to_disable    = kwargs.pop('not_to_disable')
        self.php_version       = kwargs.pop('php_version')
        super(UpdateAppForm, self).__init__(data,*args, **kwargs)
        for service in all_services:
            service_id = service['settings']['id'] 
            if service_id in enabled_services and service_id not in not_to_disable:
                self.fields[ 'disable-%s' % service_id ] = forms.BooleanField(
                    label = "",  
                    required = False, 
                    widget = widgets.LabelledCheckbox(
                        label=_('Desactivar')
                    )
                )
            elif service_id in disabled_services:
                self.fields[ 'enable-%s' % service_id ] = forms.BooleanField(
                    label = "",  
                    required = False, 
                    widget = widgets.LabelledCheckbox(
                        label=_('Activar')
                    )
                )
                if service_id in removable_servs and service_id not in not_to_disable:
                    self.fields[ 'remove-%s' % service_id ] = forms.BooleanField(
                        label = "",  
                        required = False, 
                        widget = widgets.LabelledCheckbox(
                        label=_('Eliminar')
                        )
                    )
 
            for dependency in service['dependencies']:
                self.fields[ 'dependency-%s' % service_id ] = forms.CharField(
                    widget=forms.HiddenInput(), 
                    required=False
                )
                self.fields[ 'dependency-%s' % service_id ].widget.attrs = {
                    'class' : 'dependency_%s_group' % service_id,
                    'value' : dependency['id'],
                }
    def clean(self):
        cleaned_data = super().clean()
        if self.cleaned_data.get('enable-owncloud') == True and self.php_version == '8.1':
            raise forms.ValidationError(_("No puedes activar Owncloud con la versión 8.1 de PHP porque no es compatible. Deberías volver a la versión 7.4 para poder reactivar la aplicación")) 

class SystemConfigForm(GenericForm):
    """ Form to add a new email account """
     
    header1             = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("SSH"),tag="h3"))
    ssh_port_help_text  = _("Puedes cambiar el puerto del servidor SSH, para incrementar la seguridad de tu servidor. "
                            "El puerto por defecto es el 22. Elije otro puerto entre el 2001 y 2010")
    ssh_port            = forms.ChoiceField(label=_('Puerto de escucha del servidor SSH'), required=False,help_text=ssh_port_help_text)
    ssh_key_help_text   = _("Activa esta casilla si quieres permitir conexiones ssh únicamente con clave privada. "
                            "Si la activas ya no podrás identificarte con la contraseña para acceder por ssh. Deberás en su lugar  añadir la clave púbila para cada cuenta a la que quieras permitir acceder al servidor por SSH o SFTP")
    protocol_sshd = forms.BooleanField(label=_('Impedir autenticación SSH con contraseña. Se necesitará conectar con clave privada'),help_text = ssh_key_help_text, required=False, widget=widgets.LabelledCheckbox(label=_('Deshabilitar acceso ssh con contraseña')))

    tls_help_text       = _("Configura las versiones de TLS admitidas por tu servidor de correo. TLS 1.0 ya no se considera seguro y se ha eliminado como opción. La opción \"Media\" que acepta TLS 1.2 y 1.3 es la opción recomendada. Si seleccionas la opción \"Alta\", solo se aceptará la TLS 1.3 y será más seguro, pero puedes tener problemas a la hora de enviar correos a servidores que funcionan con versiones inferiores, además <b>Rainloop (webmail) no es compatible con TLS 1.3.</b>")
    headeremail         = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Correo Electrónico"),tag="h3"))
    protocol_email            = forms.ChoiceField(label=_("Versiones de TLS soportadas"),help_text=tls_help_text,widget=forms.RadioSelect) 
    tls_apache_help_text       = _("Configura las versiones de TLS admitidas por tu servidor web. TLS 1.0 ya no se considera seguro y se ha eliminado  como opción. La opción \"Media\" que acepta TLS 1.2 y 1.3 es la opción recomendada. Si seleccionas la opción \"Alta\", solo se aceptará la TLS 1.3 y será más seguro, pero tienes que considerar que algunos servicios pueden dejar de funcionar ya que todavía funcionan con versiones inferiores. Recuerda: La app <b>OnlyOffice de Nextcloud no es compatible con TLS 1.3.</b>")
    headerapache         = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Servidor web"),tag="h3"))
    protocol_apache            = forms.ChoiceField(label=_("Versiones de TLS soportadas"),help_text=tls_apache_help_text,widget=forms.RadioSelect)


    def __init__(self, *args, **kwargs):
        #ssh_port = kwargs.pop('ssh_port')
        #ssh_key_required = kwargs.pop('ssh_key_required')
        #maintenance = kwargs.pop( 'maintenance')
        # Store this value to compare in valid_form if it has changed
        # if so, a lock_panel is needed
        #self.log_mail_status = kwargs.pop('log_mail_status')
        choices=[
                (int(22),'Default(22)'),
                (int(2001),'2001'),
                (int(2002),'2002'),
                (int(2003),'2003'),
                (int(2004),'2004'),
                (int(2005),'2005'),
                (int(2006),'2006'),
                (int(2007),'2007'),
                (int(2008),'2008'),
                (int(2009),'2009'),
                (int(2010),'2010'),
                ]
        tlschoises=[
                (4,_("Alta (TLS 1.3)")),
                (3,_("Media (TLS 1.3, TLS 1.2) - Recomendada")),
                (2,_("Baja TLS 1,3, TLS 1.2, TLS 1.1")),
              ]
        super(SystemConfigForm,self).__init__(*args, **kwargs)
        self.fields['ssh_port'].choices = choices
        self.fields['protocol_email'].choices = tlschoises
        self.fields['protocol_apache'].choices = tlschoises
        #self.fields['email'].choices.insert(0,(None, _('Selecciona un correo')))
        # If puppet status is not ready do not show the receive logs checkbos
        # It requires puppet to be run
        #if maintenance != 'ready':
	#del self.fields['log_server']
    def as_p(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<div class="form__field"><p%(html_class_attr)s>%(label)s %(help_text)s %(field)s</p></div>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True)

    def clean(self):
        cleaned_data = super().clean()
        if not self.changed_data:
            raise forms.ValidationError(_("No has cambiado ningún parámetro"))
        ssh_port = int(self.cleaned_data.get('ssh_port'))
        ssh_key_required = self.cleaned_data.get('protocol_sshd')

class FreeSystemForm(GenericForm):
    def __init__(self,data=None, *args, **kwargs):
        self.AllItems= kwargs.pop('AllItems')
        self.request = kwargs.pop('request')
        super(FreeSystemForm, self).__init__(data,*args, **kwargs)
        for item in self.AllItems:
            # We have just backups items, but in the future we may add new ones
            if(item == 'backups'):
                for folder in self.AllItems[item]['files']:
                    labelname= "%s-%s" %(item,folder['inode'])
                    self.fields['%s' % folder['fname']] =forms.BooleanField(label=labelname, required=False )
                    self.fields['%s' % folder['fname']].widget.attrs = {
                        'value' : "%s-%s" %(item,folder['inode']),
                    }

    def set_field_html_name(cls, new_name):
        """
        This creates wrapper around the normal widget rendering, 
        allowing for a custom field name (new_name).
        """
        old_render = cls.widget.render
        def _widget_render_wrapper(name, value, attrs=None):
            return old_render(new_name, value, attrs)

        cls.widget.render = _widget_render_wrapper

    def as_p(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<tr%(html_class_attr)s><td>%(field)s</td><td>%(label)s</td><td>%(help_text)s</td></tr>',
            error_row='%s',
            row_ender='</tr>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True)

    def clean(self):
        cleaned_data = super().clean()
        """
        If noting has changed do not perform save post
        """
        trash_status = utils.get_cpanel_local_status(self.request.ldap, 'trash')
        if (len(self.changed_data) == 0 and trash_status != 'error'):
            raise forms.ValidationError(
                _("No has seleccionado ninguna carpeta.")
            )
        # Check if cpanel trash status is ready or if exixts (first trash use)
        if (trash_status and trash_status != 'ready' and trash_status != 'error'):
            raise forms.ValidationError(
                 _("Hay otra operación de borrado en curso. Espera unos minutos para que termine y poder mover a la papelera más elementos.")
            )

class TrashForm(GenericForm):
    """ Form to list and permanently delete forlders from Trash"""
    def __init__(self,data=None, *args, **kwargs):
        self.trashContent= kwargs.pop('trashContent')
        self.request = kwargs.pop('request')
        super(TrashForm, self).__init__(data,*args, **kwargs)
        for item in self.trashContent:
            if item == 'clean':
                for folder in self.trashContent[item]:
                    # Menas is an element in tras tree
                    
                    self.fields['%s' % folder] =forms.BooleanField(label="", required=False )
            else:
                for elem in self.trashContent[item]:
                    for k in self.trashContent[item][elem]:
                        if(k=='files'):
                            for folder in self.trashContent[item][elem]["files"]:
                                if elem=='backups':
                                    labelname= "%s-%s" %(elem,folder['inode'])
                                    self.fields['%s' % folder['cn']] =forms.BooleanField(label=labelname, required=False )
                                    self.fields['%s' % folder['cn']].widget.attrs = {
                                        'value' : "%s-%s" %(item,folder['type']),
                                    }  

                                else:
                                    self.fields['%s' % folder['cn']] =forms.BooleanField(label="", required=False )
    def as_p(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<tr%(html_class_attr)s><td>%(field)s</td><td>%(label)s</td><td>%(help_text)s</td></tr>',
            error_row='%s',
            row_ender='</tr>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True)

    def clean(self):
        cleaned_data = super().clean()
        """
        If noting has changed do not perform save post
        """
        trash_status = utils.get_cpanel_local_status(self.request.ldap, 'trash')
        if (len(self.changed_data) == 0 and trash_status != 'error'):
            raise forms.ValidationError(
                _("No has seleccionado ninguna carpeta.")
	    )
        trash_status = utils.get_cpanel_local_status(self.request.ldap, 'trash')        
        # Check if cpanel trash status is ready or if exixts (first trash use)
        if (trash_status and trash_status != 'ready' and trash_status != 'error'):
            raise forms.ValidationError(
                 _("Hay otra operación de borrado en curso. Espera unos minutos para que termine y poder eliminar de forma permanente otras carpetas.")
            )
class BorgForm(GenericForm):

    def __init__(self,data=None, *args, **kwargs):
        # We build the form dynamically from the json result
        backupfiles= kwargs.pop('backupfiles')
        self.request = kwargs.pop('request')
        super(BorgForm, self).__init__(data,*args, **kwargs)
        for item in backupfiles["backups"]:
            labelname= "%s" % item['name'][:-9]
            self.fields['%s' % item['name']] =forms.BooleanField(label=labelname, label_suffix="",required= False)
            self.fields['%s' % item['name'] ].widget.attrs = {
                'value' : "%s" % item['name'],
            }
            if item['status'] == "mount":
                item['status'] = _("Montando carpeta")
            elif item['status'] == "umount":
                item['status']= _("Desmontando carpeta")
            
            self.fields['%s' % item['name']].help_text = item['status'] 
        if len(backupfiles["mountedpoints"]) > 0:
        # customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("SSH"),tag="h3"))
            self.fields['header1'] = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Carpetas Montadas en Caché"),tag="h4"))
            self.fields['header2'] = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Estas Carpetas siguen montada aunque ya no exista el backup que ha sido eliminado por el ciclo de rotación. En cuanto las desmontes se liberará espacio pero no podrás volver a montarlas"),tag="p"))

        for item in backupfiles["mountedpoints"]:
            labelname= "%s" % item['name'][:-9]
            self.fields['%s' % item['name']] =forms.BooleanField(label=labelname, label_suffix="",required= False)
            self.fields['%s' % item['name'] ].widget.attrs = {
                'value' : "%s" % item['name'],
                'class' : "borgfs",
            }
            if item['status'] == "mount":
                item['status'] = _("Montando carpeta")
            elif item['status'] == "umount":
                item['status']= _("Desmontando carpeta")
            
            self.fields['%s' % item['name']].help_text = item['status'] 

    def clean(self):
        cleaned_data = super().clean()
        """
        If noting has changed do not perform save post
        """
        borg_status = utils.get_cpanel_local_status(self.request.ldap, 'borgbackup')
        if (len(self.changed_data) == 0 and borg_status != 'error'):
            raise forms.ValidationError(
                _("No has seleccionado ninguna carpeta.")
            )
        # Check if cpanel trash status is ready or if exixts (first trash use)
        if (borg_status and borg_status != 'ready' and borg_status != 'error'):
            raise forms.ValidationError(
                 _("Hay otra operación de borrado en curso. Espera unos minutos para que termine y poder eliminar de forma permanente otras carpetas.")
            )

class ShowBorgKeyForm(forms.Form):
    current_password = forms.CharField(label=_('Introduce tu contraseña de acesso al panel de control'), widget=forms.PasswordInput(), required=True,  help_text=_("Para mostrar la clave necesario que insertes la contraseña que utilizas para acceder al panel de control"))

    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=False
        )

# Overrides method in django/contrib/postgres/forms/array.py
class SimpleArrayField(SimpleArrayFieldForm):
    default_error_messages = { 
      'item_invalid': _('El elemento %(nth)s del la lista de IPs que has introducdo no es válido:'),
    } 

class UnbanForm(forms.Form):
    header1             = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Fail2ban - Desbloqueo por IP"),tag="h3"))
    descrption1         = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(
                            label=_("Para desbloquear una IP tienes que averiguar cuál es la IP pública desde la que se está experimentando el bloqueo. "
                            "Puedes buscar  «cual es mi ip» con el buscador para encontrar alguna web que te proporcione esta información, por ejemplo "),tag="p"))
    link1               = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(
                            label=mark_safe ("<a href=\"https://cualesmiip.com\" target=\"_blank\"> https://cualesmiip.com</a>."),tag="p"))
    unban_ip_help_text  = _("Inserta una o más IPs separadas por coma (ej: 1.1.1.1, 2.2.2.2)")
    unban_ip            =  SimpleArrayField(forms.GenericIPAddressField(protocol='IPv4',error_messages={'invalid': _("Inserta una IP válida")}), required=False,label=_('Desbloquear IP'),help_text=unban_ip_help_text)
    header2             =  customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Tally - Desbloqueo por nombre de usuarix"),tag="h3"))
    descrption2         = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(
                            label=_("El bloqueo por nombre de usuarix solo afecta al acceso por SSH/SFP"),tag="p"))
    unban_user_help_text  = _("Inserta uno o más nombres separados pos coma (ej: user1, user2)")
    unban_user            = SimpleArrayField(forms.CharField,required=False,label=_('Desbloquear cuenta'),help_text=unban_user_help_text)
    unban_user            =  forms.CharField(label=_('Desbloquear Cuenta'), required=False,help_text=unban_user_help_text)

    def clean(self):
        #SimpleArrayField not working for Charfield without model
        # So we validate it manually
        if not self.changed_data:
            raise forms.ValidationError(_("No has introducido ningún parámetro"))
        cleaned_data = super().clean()
        users = self.cleaned_data.get('unban_user')
        items = users.split(',')
        values = []
        if users:
            for index, item in enumerate(items):
                if not utils.validate_username(item):
                    raise forms.ValidationError(
                    _("El nombre de usuarix %s no es válido" % item)
                    )

    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True
        )

class NotificationPgpForm(forms.Form):
    pgpkey           = forms.CharField(label=_('Clave PGP'), required=True, widget=forms.Textarea,
                       help_text=_('Inserta una clave pública PGP. Las notificaciones enviadas a esta cuenta se cifrarán'))

    def __init__(self,*args, **kwargs):
        self.request = kwargs.pop('request')
        super(NotificationPgpForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        check_module = utils.get_cpanel_local_status(self.request.ldap, 'zeyple')
        if check_module and check_module != 'ready':
            raise forms.ValidationError(
                    _("Se están actualizando las claves. Espera un minuto para poder añadir otra")
                    )

    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=False
        )

class ActivateLuksForm(GenericForm):
    lks0_help_text = mark_safe("<button type='button' data-copy-clipboard='id_luks0' class='page__action--copy'><i class='fa fa-copy'></i> %s</button>"% _('Copiar contraseña sugerida para el Slot 0'))
    luks0=forms.CharField(label=_('Contraseña para el Slot 0'), widget=forms.Textarea(attrs={"rows":"auto", "cols":"auto", "wrap":"hard"}), help_text=lks0_help_text, required=True)
    luks0_confirm=forms.CharField(label=_('Confirma la contraseña para el Slot 0'),widget=forms.Textarea(attrs={"rows":2, "cols":40}), required=True)
    lks1_help_text = mark_safe("<button type='button' data-copy-clipboard='id_luks1' class='page__action--copy'><i class='fa fa-copy'></i> %s</button>"% _('Copiar contraseña sugerida para el Slot 1'))

    luks1=forms.CharField(label=_('Contraseña para el Slot 1'), widget=forms.Textarea(attrs={"rows":"auto", "cols":"auto", "wrap":"soft"}),help_text=lks1_help_text,required=True)
    luks1_confirm=forms.CharField(label=_('Confirma la ontraseña para el Slot 1'),widget=forms.Textarea(attrs={"rows":2, "cols":40}), required=True)
    current_password_helptext = _("Inserta la  contraseña que utilizas para acceder al panel de control")

    current_password = forms.CharField(label=_('Contraseña actual'), widget=forms.PasswordInput(), help_text=current_password_helptext,required=False)

    def __init__(self,*args, **kwargs):
        self.request = kwargs.pop('request')
        self.username = kwargs.pop('username')
        super(ActivateLuksForm, self).__init__(*args, **kwargs)

    def clean(self):
        errors={}
        cleaned_data = super().clean()
        luks0_pass1 = self.cleaned_data.get('luks0')
        luks0_pass2 = self.cleaned_data.get('luks0_confirm')
        luks1_pass1 = self.cleaned_data.get('luks1')
        luks1_pass2 = self.cleaned_data.get('luks1_confirm')
        if luks0_pass1 != luks0_pass1:
            errors[luks0_pass1] = _("Las nuevas contraseñas para el Slot 0 no coinciden" )
        if luks1_pass1 != luks1_pass1:
            errors[luks0_pass1] = _("Las nuevas contraseñas para el Slot 1 no coinciden" )
        # For lusk we prefer to use cutom validation and avoid special chars
        # The dropbear console may have different keyboad
        if luks0_pass1 and luks1_pass1 :
            message = utils.validate_string_chars(luks0_pass1, 32)
            if message:
                errors['luks0'] = message
            message = utils.validate_string_chars(luks1_pass1, 32)
            if message:
                errors['luks1'] = message
        if errors.keys():
            self.add_error( None, _("Algún dato no es válido. Corrige los errores marcados en rojo") )
            for key, value in errors.items():
                self.add_error(key, value)
        return self.cleaned_data

    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=False
        )

class ManageLuksForm(forms.Form):
    luks0=forms.CharField(label=_('Contraseña para el slot 0'), widget=forms.PasswordInput(), required=False)
    luks1=forms.CharField(label=_('Contraseña para el slot 1'), widget=forms.PasswordInput(), required=False)

    def __init__(self,*args, **kwargs):
        #self.request = kwargs.pop('request')
        super(ManageLuksForm, self).__init__(*args, **kwargs)


class AddPoolForm(forms.Form):
    # Validation of this form is done in View post method
    newpools = forms.CharField(label=_('Inserta el número de Pools que quieres crear'), required=False, widget=forms.TextInput(attrs={'size':4}))

class PhpFpmForm(forms.Form):


    def __init__(self,*args, **kwargs):
        tootltip_text = _("Para poder desactivar este pool tienes antes que quitar el dominio asociado desde la página de edición del dominio")
        self.allowed_pools = kwargs.pop("allowed_pools")
        self.fpms = kwargs.pop("user_fpms")
        self.deletable_fpms = []
        greater_fpm = int(0)
        choices_full = [(None,_('Desactivado')),('dynamic',_('Activar')),('delete',_('Eliminar'))]
        choices_partial = [(None,_('Desactivado')),('dynamic',_('Activar'))]
        super(PhpFpmForm, self).__init__(*args, **kwargs)
        if(self.fpms):
            for fpm in self.fpms:
                deletable = 'False'
                domains=''
                name = fpm["ou"]
                fpm_id = name.replace("fpm","",1)
                if fpm["type"] == 'dynamic':
                    # We set the field name as the status we will give
                    # to the fpm if the Checkbox is selecetd.
                    #So if current status is dynamic (which means enabled) we only can disable the fpm
                    name = 'disabled-%s' % name
                    # As  we can not delete fmps with is lower that actives fpms
                    # we store the greater enabled id found among enabled fpms
                    if fpm_id.isdigit() and int(fpm_id) > int(greater_fpm):
                        greater_fpm = fpm_id
                    # If there is any domain assigned to this pool we can not disable it
                    # We are creating 3 pools by default. So do not let user delete a fmp pool lower than 3
                    if fpm["domains"] or int(fpm_id) < 4:
                        if int(fpm_id) < 4:
                            tootltip_text = _("Los primeros 3 Pools se crean por defecto y no se pueden eliminar")
                        self.fields[name]= customfileds.HeaderField(
                                label=fpm["ou"],required=False,label_suffix='', 
                                widget=widgets.HeaderWidget(
                                label=mark_safe(
                                "<div class='tooltip'>&#9432;<span class='tooltiptext'>%s</span></div>" % tootltip_text
                                ),
                                tag="span"))
                        for d in fpm["domains"]:
                            domains=domains + '\n '+ d
                        self.fields[name].help_text = domains
                    else:
                        self.fields[name] =forms.BooleanField(label=fpm["ou"], label_suffix="",required= False)

                else:
                    #If current status is disabled there are two possible actions:
                    # Delete the fpm or enable it.
                    # We set a generic edit field name and the action will be given
                    # by the choices option
                    name = 'edit-%s' % name
                    deletable = 'True'
                    # We first include both actions : delete and enable
                    self.fields[name]=forms.CharField(label=fpm["ou"], required=False, widget=forms.Select(choices=choices_full))
                    # Store yjis fpm_id into deleteable list. If we then find a greater enabled fpm
                    # We will remove the delete option
                    self.deletable_fpms.append(fpm_id) 

        # Cmpare each id in deletable_fpms with greater_fpm removing delete option if it s greater 
        for fpmid in self.deletable_fpms:
            if int(fpmid) <= int(greater_fpm):
                self.fields['edit-fpm%s' % fpmid]=forms.CharField(label='fpm%s' % fpmid,required= False, widget=forms.Select(choices=choices_partial))

    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(help_text)s %(field)s %(label)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=False
        )

class PhpVersionForm(forms.Form):
    php_version_help_text = _("Puedes elegir cual versión de PhP quieres utilizar. Algunas versiones antiguads de ciertas plataformas (Drupal, Laravel etc) pdorían no funcionar con versiones más nuevas de PHP.")
    version = forms.ChoiceField(label=_("Selecciona la versión de Php"),help_text=php_version_help_text,widget=forms.RadioSelect)

    def __init__(self,*args, **kwargs):

        versionchoices=[
            ('7.4',"7.4"),
            ('8.1',"8.1")
        ]
        self.request = kwargs.pop("request")
        super(PhpVersionForm,self).__init__(*args, **kwargs)
        if 'owncloud' in self.request.enabled_services:
            versionchoices=[
                ('7.4',"7.4"),
            ]
            self.fields['version'].help_text = _("No puedes utilizar otra versión de Php porque Owncloud está instalado y actualmente no tiene soporte para versiones mayores de PHP. Si quieres utilizar Php 8.1 deberías antes desactivar/desinstalar Owncloud") 
        self.fields['version'].choices = versionchoices

    def clean(self):
        cleaned_data = super().clean()
        """
        If noting has changed do not perform save post
        """
        if len(self.changed_data) == 0:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
            )

""" Wizard fors """
class MxcpWizardForm(forms.Form):
    name = forms.CharField()
