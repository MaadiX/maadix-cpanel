import random
from django.utils.translation import gettext_lazy as _
from django import views
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.contrib import messages
from django.urls import reverse, reverse_lazy
# contrib
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA, MODIFY_ADD
from ldap3.utils.hashed import hashed
from ldap3.core.exceptions import LDAPSocketOpenError
# project
from django.conf import settings
from . import utils, forms
import gnupg

class ActivateLuksSlots(FormView):
    form_class    = forms.ActivateLuksForm
    template_name = 'pages/luks-activate.html'

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context               = super(ActivateLuksSlots, self).get_context_data(**kwargs)
        context['username'] = self.username
        context['show_modal'] = 'form' in kwargs
        context['fqdn'] = utils.get_server_host()

        return context

    def get_form(self):
        kwargs = self.get_form_kwargs()
        
        kwargs['initial'] = {
            'luks0' : utils.generate_string(random.randrange(32, 64, 3)), 
            'luks1' : utils.generate_string(random.randrange(32, 64, 3)),
        }
        
        self.username = self.request.user.username
        return self.form_class(request=self.request,username=self.username, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        context = self.get_context_data(**kwargs)
        if form.is_valid() and request.method=='POST':
            context['show_modal'] = True
        if request.method=='POST' and 'writeldap' in request.POST:
            pwd = form['current_password'].value()
            if not pwd:
                messages.error(self.request,_("Tienes que insertar la cotraseña de acceso al panel de control"))
            dn = 'cn=%s,%s' %(self.username, settings.LDAP_TREE_BASE)
            ldapobj = utils.connect_ldap(dn, pwd)
            update_error=_("Se ha producido un error grabando los datos")
            if ldapobj['connection']:
                ldap= ldapobj['connection']
                luks_fields =['luks0', 'luks1']
                gpg = gnupg.GPG(gnupghome="/usr/share/mxcp/.gnupg")
                public_keys = gpg.list_keys()
                fingerprint = public_keys[0]['fingerprint']

                for luks in luks_fields:
                    mypwd = form[luks].value() 
                    try:
                        encrypted_password = gpg.encrypt(mypwd, fingerprint,always_trust=True)
                        mypwd = str(encrypted_password)
                        dn="ou=%s,ou=luks,ou=credentials,%s" % (luks,settings.LDAP_TREE_BASE)
                        objectClass = ['organizationalUnit', 'metaInfo']
                        update_data={'status' : [ (MODIFY_REPLACE, mypwd) ]}
                        create_data= {'status' : mypwd}
                        create_error=update_error 
                        utils.generic_update_or_create_ldap_entry(ldap, dn,objectClass,update_data, update_error, create_data, update_error)
                    except Exception as e:
                        messages.error(self.request,update_error)
                        utils.p("✕ view_luks.py", "There's a problem in  LDAP: ", e)
                context['show_modal'] = False
                utils.lock_cpanel_local(ldap, 'luks')
                utils.lock_cpanel(request)
                return HttpResponseRedirect( reverse('logout') )
            elif ldap['error'] == 'LDAPInvalidCredentialsResult':
                 messages.error(self.request,_("La contraseña actual que has insertado no es correcta" ))
            elif ldap['error']:
                messages.error(self.request,_("Error de conexión a la base de datos" ))
        return self.render_to_response(context)
class ManageLuksSlots(FormView):
    form_class    = forms.ManageLuksForm
    template_name = 'pages/luks-manage.html'


    def get_form(self):
        return self.form_class(**self.get_form_kwargs())

