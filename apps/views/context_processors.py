# python
import json, re
from os.path import exists
# django
from django.utils.text import slugify
# project
from django.conf import settings
from ldapobj.utils import p, get_puppet_status, get_release_info, get_php_version
from . import debug

def site_info_processor(request):
    """Injects into global context information about the site"""

    html_document_title = settings.DOCUMENT_TITLE
    local = settings.LOCAL

    return locals()

def active_section_processor(request):
    """Injects into global context the slug of the path as an section identifier"""
    # Check if django is runnung in subpath
    prefix = settings.FORCE_SCRIPT_NAME
    active_section = slugify(request.path.replace(prefix, "", 1).lstrip('/'))
    return locals()

def ldap_status(request):
    """Injects into global context installed services"""

    enabled_services     = []
    #remove apps not compatible with creent php version
    # We can probably get rid of this in the futrure
    # In release 202302 we remove php 7.4. Owmcloud only works whit this
    # We can re-enable php 7.4 ondeman to recover brokens php app.
    # In this case Phpmyadmin 5.2 and Nextcloud >=26 won't work and will be disabled
    # To avoid errors
    # We don't want to show disabled apps in the left menu
    php_version       = get_php_version()
    if php_version == '7.4':
        not_to_enable = settings.NOT_7_4
    else:
        not_to_enable =settings.NOT_8_1

    enabled_services_info = []
    system_reboot        = False
    system_update        = False
    rocketchat_url       = ''
    notifications_number = 0
    apps_name_domain     = []
    maintenance          = False
    is_authenticated     = False
    status_data          =''
    borg_started         = False

    if hasattr(request.user, 'role') and request.user.role == 'admin':
        is_authenticated = True
        status_data=request.status
        enabled_services = request.enabled_services
        compatible_enabled_services = [app for app in enabled_services if app not in not_to_enable]
        enabled_services_info = [ 
            conf for conf in request.release['configurations'] 
            if conf['settings']['id'] in compatible_enabled_services
        ]
        if(request.system_reboot == 'reboot'):
            system_reboot=True 
        if system_reboot:
            notifications_number += 1
        if(settings.NO_API):
            vm_status = debug.STATUS
            updates   = debug.UPDATES
        else:
            vm_status = request.status
            updates   = get_release_info(request)
        system_update = vm_status == 'pending' or vm_status == 'error' or updates['update'] 
        if system_update:
            notifications_number+=1
        #maintenance = vm_status == 'pending' or vm_status == 'error'
        puppetstatus= vm_status.get('puppetstatus', None)
        maintenance = False if puppetstatus=='ready'  else True 
        #print('ENABLED SERVCE', enabled_services)
        #Check if backups have started already 
        borg_started = exists(settings.BORG_JSON)

    return {
        'enabled_services'      : enabled_services,
        'enabled_services_info' : enabled_services_info,
        'system_reboot'         : system_reboot,
        'system_update'         : system_update,
        'notifications_number'  : notifications_number,
        'maintenance'           : maintenance,
        'is_authenticated'      : is_authenticated,
        'status_data'           : status_data,
        'borg_started'          : borg_started, 
    }
