from django.contrib.auth.mixins import UserPassesTestMixin,LoginRequiredMixin
from django.shortcuts import render


class AdminUserPassesTestMixin(LoginRequiredMixin, UserPassesTestMixin):
    
    def test_func(self):
        return self.request.user.role == "admin"

    def handle_no_permission(self):
        return render(self.request,"403.html")


class PostmasterUserPassesTestMixin(LoginRequiredMixin, UserPassesTestMixin):

    def test_func(self):
        role = self.request.user.role
        return role=='admin' or role=='postmaster'

    def handle_no_permission(self):
        return render(self.request,"403.html")


class EmailUserPassesTestMixin(LoginRequiredMixin, UserPassesTestMixin):

    def test_func(self):
        role = self.request.user.role
        return role=='admin' or role=='postmaster' or role=='email'

    def handle_no_permission(self):
        return render(self.request,"403.html")

