# python
import  random, os,  re 
from spf_validator import validator
# django
from django.utils.translation import gettext_lazy as _, get_language
from django.contrib import messages
# contrib
import dns.resolver
import psycopg2
# project
from django.conf import settings

from .models.domain import Dkim
from . import utils

# direct query suggested By Evilham:
# issue: https://gitlab.com/MaadiX/maadix-cpanel/-/issues/49#note_1377410958

def get_ns(domain: str) -> str:
    """
    Get a domain's NS by using the regular resolver.
    """
    zone = dns.resolver.zone_for_name(domain)
    res = dns.resolver.resolve(zone, rdtype='NS').response.answer[0]
    return [str(random.choice(normal_dns_query(str(ns), rdtype='A'))) for ns in res]

def normal_dns_query(domain: str, rdtype: str) -> str:
    return dns.resolver.resolve(domain, rdtype=rdtype).response.answer[0]

def direct_dns_query(domain: str, rdtype: str) -> str:
    ns = get_ns(domain)
    resolver = dns.resolver.Resolver(configure=False)
    resolver.nameservers = ns
    return resolver.resolve(domain, rdtype=rdtype)

def check_unique_entry(domain: str, rdtype: str) -> str:
    records = direct_dns_query(domain, rdtype)
    if len(records)==1:
        return True


def get_dns_records(domain: str, mode="direct") -> dict:
    """ Retrieve dns info using dns-python """
    # Use domain NS to query records to avoid unbound cache
    answers        = { }
    for record in ['A', 'AAAA','MX', 'TXT']:
        try:
            # Keep indirect query in Details page to no be banned
            if mode=="normal":
                answers[record] = dns.resolver.query(domain, record)
            else:
                answers[record] = direct_dns_query(domain, record)
        except Exception as e:
            print(str(e))
    return answers

def check_dns_A_record(domain_records: list, mode="normal") -> dict:



    MESSAGES = { 
        'NOT_A_RECORD'   : _("No se ha encontrado ningún registro de tipo A para el dominio."),
        'A_RECORDS'      : _("El dominio tiene configurado más de un registro de tipo A. "
                             "Esta configuración puede provocar anomalías. A menos que sepas "
                             "exactamente lo que estás haciendo es aconsejable que dejes un "
                             "solo registro."),
        'AAAA_RECORD'    : _("El dominio tiene configurado un registro de tipo AAAA. "
                            "De momento MaadiX no tiene soprte para ipv6 por lo que deberías "
                            "eliminar este registro."),
        'OK'             : _("La configuración de DNS para activar el servidor web es correcta"),
        'NOT_OK'         : 'NOT_OK',
        'NOT_MAILSERVER' : _("El servidor de correo no está activado para este dominio. "
                             "En el caso quisieras activarlo la siguiente tabla te muestra los valores "
                             "DNS correctos."),
        #'CHANGE_IP'      : _("Edita el registro de tipo A cambiando la actual IP %s por %s"),
    } 
    ip              = utils.get_server_ip()
    msg             = '' 
    has_A           = 'A' in domain_records
    has_AAAA        = 'AAAA' in domain_records
    record          = domain_records['A'][0] if has_A else None
    records_A       = domain_records['A'] if has_A else [None]

    error           = True
    results         = {}
    if not has_A:
        msg = MESSAGES['NOT_A_RECORD']
    else:
        A_records_number = len(domain_records['A'])
        # mark error if there is a AAAA record until we enable ipv6
        # Otherwise Let's Encrypt will fail
        if has_AAAA:
            msg = MESSAGES['AAAA_RECORD']
        elif  A_records_number == 1 and '%s' % record == '%s' % ip: 
            msg = MESSAGES['OK']
            error = False
        elif A_records_number > 1 and ip in records_A:
            msg = MESSAGES['A_RECORDS']
        else:
            msg = MESSAGES['NOT_OK']
    results = { 'msg': msg, 'error' : error }
    return results

def check_single_A_record(record: str, mode="normal") -> bool:
    ip = utils.get_server_ip()
    if  record == '%s' % ip:
        return True
    else:
        return False

def check_SPF_record(record: str, mode="fqdn") -> bool:
    #Check syntax
    errors_list = validator.validate_spf_string(str(record))
    if errors_list:
        return False
    else:
        # Split the record by empty spaces to check if has correct values. It may have more
        parts = record.split(" ")
        req= ["mx", "all"]
        res = all(ele in record for ele in req)
        if not  mode=="fqdn":
            # Need to check That ip or domain are specified
            if not any(x == "ip4:%s" % utils.get_server_ip() or x== "a:%s" % utils.get_server_host() for x in parts):
                res = False
    return res

def get_all_dns(host: str) -> list:

    ip             = utils.get_server_ip()
    localhost           = utils.get_server_host()
    valid_SPF = "v=spf1 mx ip4:%s a:%s ~all" %(ip, localhost)
    fqdn = False
    is_mailman_domain = False
    # Chefk if is a domain query or fqdn query
    if str(localhost) == str(host):
        fqdn = True
        valid_SPF = "v=spf1 a mx ~all"

    domain_records = get_dns_records(host)
    # What about CNAME?
    #for rdata in dns.resolver.query(site, 'CNAME') :
    #    print rdata.target

    dns = {} 
    # A
    records_A      = domain_records.get('A')
    dns["A"] = []
    if records_A:
        for record in records_A:
            status = check_single_A_record(record.to_text())
            dns["A"].append({"type":"A", "name": host,"Requerido": ip, "Actual": record.to_text(), "Estado": status})
    else:
        dns["A"].append({"type":"A", "name": host,  "Requerido": ip, "Actual": "", "Estado": False})

    # Currently no support for ipv6
    records_AAAA  = domain_records.get('AAAA')
    dns["AAAA"] = []
    if records_AAAA: 
        for record in records_AAAA:
            dns["AAAA"].append({"type":"AAAA", "name": host,  "Requerido": _("Ninguno"), "Actual": record, "Estado": False})

    # MX
    valid_MX       = localhost +'.'
    
    records_MX     = [ mx.exchange for mx in domain_records['MX']] if domain_records.get('MX')  else []
    dns["MX"] = []
    if len(records_MX) > 0:
        for record in records_MX:
            status = record.to_text() == valid_MX
            dns["MX"].append({"type":"MX", "name": host, "Requerido": valid_MX , "Actual": record.to_text(), "Estado": status})
    else:
        dns["MX"].append({"type":"MX", "name": host, "Requerido": valid_MX , "Actual": "", "Estado": False})

    # SPF
    dns["SPF"] = []
    spf = domain_records.get('TXT')
    record_SPF = ""
    if spf:
        for record in spf:
            record_text = "".join([a.decode("utf-8") for a in record.strings])
            if "v=spf" in record_text:
                record_SPF = record_text

    if record_SPF:
        #This returns issues
        status = check_SPF_record(record_SPF)
        curr = record_SPF
    else:
        status = False
        curr = ""
    dns["SPF"].append({"type":"TXT", "name": host,  "Requerido": valid_SPF , "Actual":  curr,  "Estado": status})

    #DKIM
    dns["DKIM"]=[]
    status = False
    record_DKIM    = get_dkim(host)
    required_dkim = get_local_dkim_key(host)
    # Check if dkim has been created or not by user,  except for fqdn
    if not required_dkim and fqdn == False:
        try:
            obj = Dkim.objects.get(ou=host)
            # Dkim has been enabled by user, but not generated yet
            required_dkim =  _('Todavía no se ha generado ninguna clave dkim para el dominio %s. Este proceso puede tardar unos minutos.' % host) 
        except Dkim.DoesNotExist:
            required_dkim =  _("La clave DKIM no está activada para este dominio")

    dkim_domain    = "default._domainkey.%s" % host
    if record_DKIM and required_dkim:
        status= compare_dkim_values(record_DKIM,required_dkim)
        # Check if is mailmain domain
        # mailman_domains = utils.get_mailman_domain_names()
    if not record_DKIM:
        record_DKIM = _("No hay registro DKIM")
    dns["DKIM"].append({"type":"TXT", "name": dkim_domain, "Requerido": required_dkim , "Actual": record_DKIM, "Estado": status})
    return dns

def get_dkim_status_cpanel(ldap) -> str:
    """ Check If cpaneldkim_status is locked """
    try:
        ldap.search(
            settings.LDAP_TREE_DKIM,
            settings.LDAP_FILTERS_DKIM,
            attributes=['status']
        )
        status = ldap.entries[0]['status']
        return status

    except Exception as e:
        utils.p("utils.py", "✕ There's a problem connecting with LDAP", e)
        return None


def get_dkim(domain: str)-> str:
    """ Retrieve DKIM info using dns-python """
    domain = "default._domainkey.%s" % domain
    try:
        answer = dns.resolver.query(domain, 'TXT')
        #return answer.to_text()
        return str(answer[0])
    except Exception as e:
        utils.p("dns_utils.py · get_dkim", "✕ There's a problem checking the DKIM register of the domain %s:" % domain, e)
    return None

def get_local_dkim_key(domain):
    regex = r"\(\s((?s)(.+?))\s\)"
    dkim_path      = "/etc/opendkim/keys/%s/default.txt" % domain 
    if os.path.isfile(dkim_path):
        test_str = open(dkim_path).read(1000)
        matches = re.search(regex, test_str)
        if matches:
            dkim = matches.group(1)
    else:
        dkim = None
    return dkim

def replaceMultiple(mainString: str, toBeReplaces: list, newString: str) -> str:
    # Iterate over the strings to be replaced
    for elem in toBeReplaces :
        # Check if string is in the main string
        if elem in mainString :
            # Replace the string
            mainString = mainString.replace(elem, newString)
    
    return  mainString

def compare_dkim_values(dns_dkim: str, local_dkim: str) -> bool:
    #Need to remove sapece and " to compare strings
    delete = [" ", "\"","\n", "\r\n","\r", "\t"]
    # If dkmin is none use empty string 
    dns_dkim_net = '' if not dns_dkim else replaceMultiple(dns_dkim, delete, "")
    local_dkim_net = '' if not local_dkim else replaceMultiple(local_dkim, delete, "")
    if dns_dkim_net == local_dkim_net:
        return True
    else:
        return False

def domain_pointed_to(domain:str, ip: str) -> bool:
    """ Checks if a domain is pointed to a given IP """
    try:
        domain_ip = dns.resolver.query(domain, 'A')
        return domain_ip == ip
    except Exception as e:
        utils.p("utils.py · domain_pointed_to", "✕ There's a problem checking the A register of the domain %s:" % domain, e)
    return None
