import sys, os
from django.core.management.base import BaseCommand, CommandError
#from ldap3 import MODIFY_ADD
#from ldap3.core.exceptions import LDAPNoSuchObjectResult
from django.conf import settings
from django.utils.crypto import get_random_string
#from ldapobj.models.user import LdapUser as User
#from apps.views  import utils

class Command(BaseCommand):
    help = "Create KeY_file if it does not exist"

    def handle(self, *args, **options):
        try:
            """ This is to generate a SECRET_KEY
            automatically, without having to install django on a separate
            way. if the file key_file.py containing the key doesn' exists, let's genereate it
            """

            filename = settings.PROJECT_STATIC_FOLDER + '/key_file.py'
            if ( not os.path.isfile(filename)):
                print("Generating key")
                chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
                key = get_random_string(50, chars)
                key_string = 'SECRET_KEY = \'%s\'' % key 
                with os.fdopen(os.open(filename, os.O_WRONLY | os.O_CREAT, 0o600), 'w') as keyfile:
                    keyfile.write(key_string)
                    keyfile.close()
                sys.exit(1)
            else:
                print("Key file already exists")
        except Exception as e:
            print("There was a problem writing the keyfile", str(e), sep="\n")
            sys.exit(0)

