import sys
from django.core.management.base import BaseCommand, CommandError
from ldap3 import MODIFY_ADD
from ldap3.core.exceptions import LDAPNoSuchObjectResult
from django.conf import settings
from ldapobj.models.user import LdapUser as User
from apps.views  import utils

class Command(BaseCommand):
    help = "Add objectClass: ldapPublicKey for former users where missing"

    def handle(self, *args, **options):
        try:
            ld     = utils.connect_ldap(
               settings.AUTH_LDAP_BIND_DN,
               settings.AUTH_LDAP_BIND_PASSWORD
            )
            bade_dn = "ou=sshd,ou=People,dc=example,dc=tld"
            search_filter = "(&(!(objectClass=ldapPublicKey))(objectClass=person)(uid=*))"
            ldap = ld['connection']
            ldap.search(
            search_base   = bade_dn,
            search_filter = search_filter,
            attributes    = ['uid']
            )
            users = ldap.entries
            for u in users:
                print(u.uid)
                dn = "uid=%s,ou=sshd,ou=People,dc=example,dc=tld" % u.uid
                attr = {'objectClass' : [(MODIFY_ADD,'ldapPublicKey')]}
                ldap.modify(dn, attr) 
        except LDAPNoSuchObjectResult:
            pass
        except Exception as e:
            print("ERROR", e)
            sys.exit(1)
        print("Users updated")
        sys.exit(0)
