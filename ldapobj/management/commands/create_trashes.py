import sys
from django.core.management.base import BaseCommand, CommandError
from ldap3 import MODIFY_ADD
from ldap3.core.exceptions import LDAPNoSuchObjectResult, LDAPAttributeOrValueExistsResult, LDAPEntryAlreadyExistsResult
from django.conf import settings
from apps.views  import utils

class Command(BaseCommand):
    help = "Create trash main trees"
    trash_objects = ['mails', 'users', 'backups', 'domains', 'onions']

    def handle(self, *args, **options):
        ld     = utils.connect_ldap(
           settings.AUTH_LDAP_BIND_DN,
           settings.AUTH_LDAP_BIND_PASSWORD
        )
        ldap = ld['connection']
        base_dn = settings.LDAP_TREE_TRASH
        classes =  ['organizationalUnit']
        for item  in self.trash_objects:
            try:
                dn = "ou={model},{base_dn}".format(model = item, base_dn= base_dn)
                ldap.add(dn, classes, {"ou" : item})
                print("Created", dn)
            except LDAPEntryAlreadyExistsResult:
                print("DN already exists", dn)
        sys.exit(0)
