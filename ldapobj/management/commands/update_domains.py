import sys, logging
from django.core.management.base import BaseCommand, CommandError
from ldap3 import MODIFY_ADD
from ldap3.core.exceptions import LDAPNoSuchObjectResult, LDAPAttributeOrValueExistsResult
from django.conf import settings
from apps.views  import utils

logger = logging.getLogger("domains")

class Command(BaseCommand):
    help = "Add objectClass: Yap and top for former domains where missing"

    def handle(self, *args, **options):
        try:
            ld     = utils.connect_ldap(
               settings.AUTH_LDAP_BIND_DN,
               settings.AUTH_LDAP_BIND_PASSWORD
            )
            base_dn = settings.LDAP_TREE_HOSTING
            search_filter = "(&(objectClass=VirtualDomain)(vd=*))"
            ldap = ld['connection']
            ldap.search(
            search_base   = base_dn,
            search_filter = search_filter,
            attributes    = ['vd']

            )
            items = ldap.entries
            if items:
                for u in items:
                    print(u.vd.value)
                    dn = "vd=%s,%s" % (u.vd.value,base_dn)
                    # YAP
                    try:
                        ldap.modify(dn, {'objectClass' : [(MODIFY_ADD,'Yap')],
                                        'otherPath'     :[(MODIFY_ADD,'default')]})
                        self.stdout.write(self.style.SUCCESS('Yap Added'))
                    except LDAPAttributeOrValueExistsResult:
                        self.stdout.write(self.style.SUCCESS('Yap already exist'))
                    # Top
                    try:
                        ldap.modify(dn, {'objectClass' : [(MODIFY_ADD,'top')]})
                        self.stdout.write(self.style.SUCCESS('Top Added'))
                    except LDAPAttributeOrValueExistsResult:
                        self.stdout.write(self.style.SUCCESS('Top already exists'))
                    # MetaInfo
                    try:
                        ldap.modify(dn, {'objectClass' : [(MODIFY_ADD,'metaInfo')]})
                        self.stdout.write(self.style.SUCCESS('MetaInfo Added'))
                    except LDAPAttributeOrValueExistsResult:
                        self.stdout.write(self.style.SUCCESS('MetaInfo already exists'))

            else:
                print("All up to date")
        except LDAPNoSuchObjectResult:
            print("All up to date")
        except Exception as e:
            print("ERROR", e)
            sys.exit(1)
        sys.exit(0)
