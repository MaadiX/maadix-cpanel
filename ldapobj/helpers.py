from django.contrib import messages
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

@login_required
def delete_objects(request, model, selected_ids):
    '''
    capsulate a bulk delete method
    delete all objects found for the given model
    fails silently since model.delete() always fails silently
    '''
    object_list = model.objects.filter(pk__in=selected_ids)
    count = object_list.count()
    if count == 1:
        name = model._meta.verbose_name.title()
    else:
        name = model._meta.verbose_name_plural.title()
    object_list.delete()
    request.user.message_set.create(message='Successfully deleted %s %s' % (count,name))
    return

