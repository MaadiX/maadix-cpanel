from xmlrpc.client import boolean
import ldap,re, json
#from django.core.exceptions import ObjectDoesNotExist, EmptyResultSet
from django.utils.translation import gettext_lazy as _
from django.core.management import utils as django_management_utils
from ldap3 import  HASHED_SALTED_SHA
from ldap3.utils.hashed import hashed

import ldapdb.models
from ldapdb.models import fields
from .mail import  LdapMail as Mail
from .trash import DomainTrash
from .modules import ModuleTop
from django.conf import settings
from . import utils as utils
from ..views.json_tor import get_json_data

# Path model
class DomainPath(ldapdb.models.Model):
    # LDAP meta-data
    #base_dn         = settings.LDAP_TREE_HOSTING
    object_classes  = ['organizationalUnit', 'metaInfo', 'top' ]
    ou           = fields.CharField(db_column='ou', default='path',primary_key=True )
    pathType      = fields.CharField(db_column='type',  null=True, blank=True)

    def __str__(self):
        return self.pathType

class LdapDomain(ldapdb.models.Model):

    # LDAP meta-data
    base_dn         = settings.LDAP_TREE_HOSTING
    object_classes  = ['VirtualDomain', 'metaInfo', 'top', 'Yap']
    # Attrs
    name          = fields.CharField(_('Dominio') ,db_column='vd', primary_key=True )                                               
    adminID         = fields.CharField(_("Webmaster"),db_column='adminID')                                                               
    accountActive   = fields.BooleanField(_("Servidor de correo "),db_column='accountActive')
    lastChange      = fields.IntegerField(db_column='lastChange', default=utils.unix_timestamp())
    delete          = fields.BooleanField(db_column='delete', default=False)
    otherPath       = fields.CharField(db_column='otherPath', default='defaut')
    status          = fields.CharField(_("Servidor Web"),db_column='status')
    phpPool         = fields.CharField(db_column='type', null=True, blank=True)

    def __str__(self):
        return self.name     

    @property
    def acl(self) -> bool:
        dn = "ou=acl,%s" %(self.dn)
        try:
            acl = Acl.objects.get(dn=dn)
            status = True if acl.active.lower() == 'true' else False
        except Exception as e:
            status = False
        return status 

    @property
    def postmaster(self) -> object:
        full_dn="cn=postmaster,%s" % (self.dn)
        try:
            pm = Postmaster.objects.get(dn=full_dn)
        except Exception as e:
            pm= None
        return pm

    @property
    def wordpress(self) -> object:
        dn = "ou=cms,%s" %(self.dn)
        try:
            wp = Cms.objects.get(dn=dn)
        except:
            wp = None
        return wp 
    
    @property
    def webserevser_type(self) -> str:
        dn = "ou=webserver,%s" %(self.dn)
        ws_type = "apache"
        try:
            ws = WebServer.objects.get(dn=dn)
            if ws:
                ws_type = ws.webserverType
        except:
            ws_type
            
        return ws_type

    @property
    def tor(self) -> bool:
        dn = "ou=onion,%s" %(self.dn)
        res = False
        try:
            obj = OnionDomain.objects.get(dn=dn)
            if obj.status.lower() == "true":
                res = True
        except Exception:
            res = False
        return res 

    @property
    def path(self) -> str:
        try:
            dn = "ou=path,vd=%s,%s" %(self.name, settings.LDAP_TREE_HOSTING)
            path = DomainPath.objects.get(dn=dn)
            path = path.pathType
        except:
            path = ""
        return "/var/www/html/%s/%s" % (self.name,path)

    @property
    def dkim(self) -> str:
        try:
            return Dkim.objects.get(ou=self.name)
        except Dkim.DoesNotExist:
            return None
    
    def is_valid(self) -> bool:
        return re.match("^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,20}$", self.name)

    def set_postmaster(self) -> object:
        try:
            postmaster_pwd = django_management_utils.get_random_secret_key()
            passw =  hashed(HASHED_SALTED_SHA, postmaster_pwd)
            email = "postmaster@{}".format(self.name)
            full_dn="cn=postmaster,%s" % (self.dn)
            postmaster = Postmaster.objects.create(
                    dn =full_dn,
                    password = passw,
                    mail = email
					)
            return postmaster
        except Exception as e:
            print(e)

    def del_postmaster(self):
        try:
            dn = "cn=postmaster,%s" % self.dn
            obj = Postmaster.objects.filter(dn=dn).first()
            if obj:
                obj.delete()
        except Exception as e:
            print(e)

    def set_webserver_type(self, server_type: str)-> object:
        #ou=webserevr,vd=example.net,o=hosting,dc=example,dc=tld
        full_dn = "ou=webserver,%s" % self.dn
        try:
            obj=  WebServer.objects.get(dn=full_dn)
            obj.webserverType=server_type
            obj.save()
        except Exception as e:
            print("EX", e)
            obj= WebServer.objects.create(
            dn=full_dn, 
            webserverType=server_type

        )
        return obj

    def del_webserver_type(self):
        try:
            full_dn = 'ou=webserver,%s' % self.dn 
            ws = WebServer.objects.filter(dn=full_dn)
            if ws:
                ws.delete()
        except Exception as e:
            print(e)

    def set_tor(self, status: str) -> object:

        full_dn = "ou=onion,%s" % self.dn
        try:
            obj = OnionDomain.objects.create(dn=full_dn, status=status)
        except Exception as e:
            obj=  OnionDomain.objects.get(dn=full_dn)
            obj.status = status
            obj.save()
            print("EX", e)
        return obj

    def del_tor(self):
        try:
            full_dn = 'ou=onion,%s' % self.dn 
            obj = OnionDomain.objects.filter(dn=full_dn).first()
            if obj:
                obj.delete()
        except Exception as e:
            print(e)


    def set_wordpress(self, password: str) -> object:
        try:
            #ou=cms,vd=webxes.gadix.net,o=hosting,dc=example,dc=tld
            rand_string = utils.generate_string(6)
            note = self.name.replace('.', '_')
            note = 'wordpress_' + note + '_' + rand_string
            full_dn = 'ou=cms,%s' % self.dn
            print("FULL DN", full_dn)
            wp_object = Cms.objects.create(dn=full_dn, note=note)
	        # Store user input password- No model for this
            print("FIRST dn")
            wp_pass = utils.encrypt_password(password)
            """
            dn = 'vd=%s,o=hosting,%s' % (self.name, settings.LDAP_TREE_BASE)
            wp_dn = 'ou=cms,%s' % dn
            """
            wp_pass_dn = 'ou=password,%s' % full_dn
            
            print("wp pass dn", wp_pass_dn)
            CmsPassword.objects.create(dn=wp_pass_dn, status=wp_pass)
            print("second dn")
            return wp_object
        except Exception as e:
            print("Error creating wordpress objects", e)

    def del_wordpress(self):
        try:
            dn = 'ou=cms,%s' % self.dn 
            self.del_wp_pass(dn)
            obj = Cms.objects.filter(dn=dn).first()
            if obj:
                obj.delete()
        except Exception as e:
            print(e)

    def del_wp_pass(self, dn):
        pass_d = "ou=password,%s" % dn
        try:
            pas = CmsPassword.objects.filter(dn=pass_d)
            if pas:
                pas.delete()
        except Exception as e:
            pass

    def set_path(self, documentRoot) -> object:
        # This value can not be edited. only created
        path = documentRoot.strip("/")
        full_dn="ou=path,%s" % (self.dn) 
        path_obj = DomainPath.objects.create(dn=full_dn, pathType=path)
        return path_obj

    def del_path(self):
        try:
            full_dn="ou=path,%s" % (self.dn)
            print("full dn", full_dn)
            obj = DomainPath.objects.filter(dn=full_dn).first()
            if obj:
                obj.delete()
            return 1
        except Exception as e:
             print(e)
        return 0

    def set_dkim(self) -> object:
        # Make sure base opendkim tree exists and set to locked
        ModuleTop.objects.update_or_create(
            ou="opendkim", defaults={"status": "locked"}
        )
        obj = Dkim.objects.create(ou=self.name)
        return obj
    
    def del_dkim(self):
        try:
            obj = Dkim.objects.filter(ou=self.name).first()
            if obj:
                obj.delete()
        except Exception as e:
            print(e)

    def set_acl(self, status: str) -> object:

        full_dn = "ou=acl,%s" % self.dn
        try:
            obj=  Acl.objects.get(dn=full_dn)
            obj.active = status
            obj.status = "pending"
            obj.save()
        except Exception as e:
            # Create with default values
            print("EX", e)
            obj = Acl.objects.create(dn=full_dn)
        return obj



    def del_acl(self):
        try:
            dn = "ou=acl,%s" % self.dn 
            obj = Acl.objects.filter(dn=dn).first()
            if obj:
                obj.delete()
        except Exception as e:
            print(e)
    
    def get_domain_mails(self):
        all_mails = []
        try:
            # TODO : try to get objetcs with filter.
            # It is not working for some reason
            objs = Mail.objects.all()
            # Get only mails belonging to the domain
            for obj in objs:
                if self.dn in obj.dn:
                    all_mails.append(obj)
        except Exception as e:
            print(e)

        return all_mails

    def del_mails(self):
        mails = self.get_domain_mails()
        if mails:
            try:
                for mail in mails:
                    Mail.objects.filter(pk=mail.mail).delete()
                    mail.move_to_trash()
            except Exception as e:
                print(e)

    def del_former_abuse(self):
        # from is mail=abuse@a.com,vd=a.com,o=hosting,dc=example,dc=tld
        try:
            dn = "mail=abuse@%s,%s" % (self.name, self.dn)
            obj =  FormerAbuse.objects.filter(dn=dn).first() 
            if obj:
                obj.delete()
        except Exception as e:
            print(e)

    def get_onion_address(self, request) -> str:
        try:
            all_items =get_json_data(request).content.decode('utf-8')
            all_dom = json.loads(all_items).get('maadix_tor_hidden_services')
            print("maadix_tor_hidden_services", all_dom)
            for key, val in all_dom.items():
                if key == self.name:
                    return val
        except Exception as e:
            return None
        return None

    # Need to override delete, as we have to delete all
    # subordinate objects must be deleted first

    def delete_recursive(self):
        try:
            self.del_dkim()
            self.del_path()
            self.del_webserver_type()
            self.del_wordpress()
            self.del_postmaster()
            self.del_acl()
            self.del_mails()
            self.del_former_abuse()
            self.del_tor()
        except Exception as e:
            print(e)

    def move_to_trash(self):
        if self.is_valid():
            cn = "%s-%s" % (self.name, utils.unix_timestamp())
            try:
                obj = DomainTrash.objects.create(
                    cn=cn,
                    otherPath=self.path,
                    typename=self.name,
                    description = utils.ldap_creation_dateTime(),
                    status = 'totrash' )
                return obj
            except Exception as e:
                print("ERROR creating domain trash object", e)
                return None
        else:
            #messages.error(self.request, _('Error moviendo %s a la papelera' % self.name))
            return None



class Postmaster(ldapdb.models.Model):
    base_dn             = settings.LDAP_TREE_HOSTING
    object_classes      = ['VirtualMailAlias', 'top']
    cn                  = fields.CharField(db_column='cn', default='postmaster', primary_key=True)
    sn                  = fields.CharField(db_column='sn', default='Postmaster' )
    maildrop            = fields.CharField(db_column='maildrop',default='postmaster' )
    mail                = fields.CharField(db_column='mail' )
    password            = fields.CharField(db_column='userPassword')
    accountActive       = fields.BooleanField(db_column='accountActive', default=True)
    creationDate        = fields.IntegerField(db_column='creationDate', default=utils.ldap_creation_date())
    lastChange          = fields.IntegerField(db_column='lastChange', default=utils.unix_timestamp())

    def __str__(self):
        return self.mail
    
    @property
    def role(self):
        return "postmaster"

    
    def get_domain(self):
        return self.mail.split('@')[1]

    @property
    def domain(self):
        return self.get_domain()

class Acl(ldapdb.models.Model):

    object_classes  = ['organizationalUnit', 'metaInfo', 'top' ]
    ou              = fields.CharField(db_column='ou', primary_key=True, default='acl' )
    active         =  fields.CharField(db_column='type',default='true', null=True, blank=True)
    status       = fields.CharField(db_column='status', default='pending')

    base_dn         = settings.LDAP_TREE_HOSTING
    search_scope = ldap.SCOPE_ONELEVEL

    def __str__(self):
        return self.status

class Cms(ldapdb.models.Model):
    # LDAP meta-data
    base_dn         = settings.LDAP_TREE_HOSTING
    object_classes  = ['organizationalUnit', 'metaInfo', 'top' ]
    # Attrs             
    ou           = fields.CharField(db_column='ou', primary_key=True, default="cms")
    note         = fields.CharField(db_column='note')                                                 
    cmsType      = fields.CharField(db_column='type', default='wordpress')
    status       = fields.CharField(db_column='status', default='install')

    def __str__(self):
        return self.note

    @property
    def domain(self):
        string  = self.note.replace('_','.')
        items = string.split('@')[1] 
        domain = items[1:-1]
        return domain 
    """
    def build_rdn(self):
        bits = []
        for field in self._meta.fields:
            if field.db_column == 'note':
                val =  getattr(self, field.name)
                string  = self.note.replace('_','.')
                items = string.split('@')[1]
                domain = items[1:-1]
                bits.append("ou=cms,vd=%s" % (domain))
        if not len(bits):
            raise Exception("Could not build Distinguished Name")
        return '+'.join(bits)
    """

class CmsPassword(ldapdb.models.Model):
    object_classes  = ['organizationalUnit', 'metaInfo']
    ou              = fields.CharField(db_column='ou', primary_key=True, default="password" )
    status       = fields.CharField(db_column='status')


class Dkim(ldapdb.models.Model):
    base_dn     =   settings.LDAP_TREE_DKIM
    object_classes  = ['organizationalUnit', 'top' ]
    ou              = fields.CharField(db_column='ou', primary_key=True )

class FormerAbuse(ldapdb.models.Model):
    base_dn         = settings.LDAP_TREE_HOSTING
    object_classes  = ['VirtualMailAlias', 'top' ]
    cn              =  fields.CharField(db_column='cn', null=True, blank=True)
    sn              = fields.CharField(db_column='sn', null=True, blank=True)
    mail                    = fields.CharField(db_column='mail', primary_key=True)
    maildrop                = fields.CharField(db_column='maildrop',default="")
    accountActive           = fields.BooleanField(db_column='accountActive',default=True)
    lastChange              = fields.CharField(db_column='lastChange', default=utils.unix_timestamp())
    creationDate            = fields.IntegerField(db_column='creationDate', default=utils.ldap_creation_date())

# Default is ou=webserver.
class WebServer(ldapdb.models.Model):
    # LDAP meta-data
    base_dn         = settings.LDAP_TREE_HOSTING
    object_classes  = ['organizationalUnit', 'metaInfo' ]
    # Attrs             
    ou           = fields.CharField(db_column='ou', primary_key=True, default="webserver")
    webserverType = fields.CharField(db_column='type', default='apache')
    status       = fields.CharField(db_column='status',null=True, blank=True)

# Model for onion domains
class OnionDomain(ldapdb.models.Model):
    # LDAP meta-data
    base_dn         = settings.LDAP_TREE_HOSTING
    object_classes  = ['organizationalUnit', 'metaInfo','Top' ]
    # Attrs             
    ou           = fields.CharField(db_column='ou', primary_key=True, default="onion")
    status       = fields.CharField(db_column='status',default="TRUE")
