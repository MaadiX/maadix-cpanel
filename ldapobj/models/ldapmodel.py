import ldapdb.models
from ldapdb.models import fields

"""
Class to provide a default delete_recursive method, needed only
if a Ldap objet has subtrees
"""
class LdapModel(ldapdb.models.Model):

    class Meta:
       abstract = True

    def delete_recursive(self):
        pass
    def move_to_trash(self):
        pass