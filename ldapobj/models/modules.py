from django.utils.translation import gettext_lazy as _
import ldapdb.models
from ldapdb.models import fields
from django.conf import settings



"""Models for modules to be locked"""

""" Modules belonging to this Module Class are:
    luks, mysql, trash, reboot, domains,keycopy, borgbackup, customfqdn, cleandocker : info, status
    tally, fail2ban : type, status
"""

class Module(ldapdb.models.Model):
    base_dn         = settings.LDAP_TREE_CPANEL
    object_classes  = ['organizationalUnit', 'metaInfo','extensibleObject']
    ou              = fields.CharField(db_column='ou', primary_key=True)
    info            = fields.CharField(db_column='info',default="ready", null=True, blank=True)
    status          = fields.CharField(db_column='status', default="ready")
    typename        = fields.CharField(db_column='type',null=True, blank=True)

    def __str__(self):
        return self.ou

    def lock(self):
        self.status = "locked"
        self.save()

# Need to create different model for modules with Top ObjectClass and missing extensibleObject
""" Modules belonging to  ModuleTop Class are:
    zeyple, borgkey, opendkim, rkhunter
"""
class ModuleTop(ldapdb.models.Model):
    base_dn         = settings.LDAP_TREE_CPANEL
    object_classes  = ['organizationalUnit', 'metaInfo','top']
    ou              = fields.CharField(db_column='ou', primary_key=True)
    status          = fields.CharField(db_column='status', default="ready")

    def __str__(self):
        return self.ou

    def lock(self):
        self.status = "locked"
        self.save()

