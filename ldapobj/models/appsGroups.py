from django.utils.translation import gettext_lazy as _
import ldap
import ldapdb.models
from ldapdb.models import fields
from django.conf import settings

"""
ONE_LEVEL, objects inmediately subordinated
BASE, the base object itself
SCOPE_SUBTREE , for all subordinates
"""

class App(ldapdb.models.Model):
    search_scope    = ldap.SCOPE_ONELEVEL
    base_dn         = settings.LDAP_TREE_SERVICES
    object_classes  = ['organizationalUnit','metaInfo']
    ou              = fields.CharField(db_column='ou', primary_key=True)
    status          = fields.CharField(db_column='status', default="ready")
    typename        = fields.CharField(db_column='type',null=True, blank=True)
    
    def set_dependency(self, dep, status):

        full_dn="ou=%s,%s" % (dep, self.dn)
        # update_or_create throws error . We have a dinamically built dn
        # so we can not just crate the object with ou.
        # If we put ou in defaults it works for create object but fails for
        # update, and the other way around. Use get first and the create if not exists
        try:
            obj=  AppDependency.objects.get(dn=full_dn)
            obj.status=status
            obj.save()
        except Exception as e:
            print("EX", e)
            obj= AppDependency.objects.create(
            dn=full_dn, 
            ou= dep,
            status= status,
        )
        return obj


    def get_dependency(self, dep):
        full_dn="ou=%s,%s" % (dep, self.dn)
        # update_or_create throws error . We have a dinamically built dn
        # so we can not just crate the object with ou.
        # If we put ou in defaults it works for create object but fails for
        # update, and the other way around. Use get first and the create if not exists
        try:
            obj=  AppDependency.objects.get(dn=full_dn)
        except Exception as e:
            obj = None
        return obj 

#TODO: subclassing is giving error
class AppDependency(ldapdb.models.Model):
    search_scope    = ldap.SCOPE_SUBTREE
    base_dn         = settings.LDAP_TREE_SERVICES
    object_classes  = ['organizationalUnit','metaInfo']
    ou              = fields.CharField(db_column='ou', primary_key=True)
    status          = fields.CharField(db_column='status', default="ready")
    typename        = fields.CharField(db_column='type',null=True, blank=True, default="")



