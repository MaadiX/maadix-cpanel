import ldap, re
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
import ldapdb.models
from ldapdb.models import fields
from .domain import  LdapDomain as Domain
from .. import utils
from django.conf import settings
from .ldapmodel import LdapModel
from .trash import UserTrash

"""Model for the control panel admin user"""
class AdminUser(LdapModel):
    base_dn         = "dc=example,dc=tld"
    object_classes  = ['organizationalRole', 'simpleSecurityObject','extensibleObject']
    username        = fields.CharField(_("Nombre"),db_column='cn', primary_key=True)
    mail           = fields.CharField(_("Email"),db_column='email', null=True)
    password        = fields.CharField(_("Contraseña"),db_column='userPassword')
    status          = fields.CharField(db_column='status') 
    def __str__(self):
        return self.username
        
    @property
    def role(self):
        return "admin"

class BaseUser(LdapModel):
    class Meta:
        abstract = True

    """
    Class for representing an LDAP user entry.
    """
    # LDAP meta-data
    base_dn = settings.LDAP_TREE_USERS
    search_scope = ldap.SCOPE_ONELEVEL
    object_classes = ['person', 'organizationalPerson','posixAccount', 'shadowAccount', 'inetOrgPerson', 'top', 'authorizedServiceObject','ldapPublicKey']
    # inetOrgPerson
    first_name = fields.CharField(db_column='cn', verbose_name=_("Nombre"), blank=True,null=True)
    last_name = fields.CharField(db_column='sn',verbose_name=_("Apellidos"), blank=True, null=True)
    email = fields.CharField(_("Email"),db_column='mail', null=True)

    # posixAccount
    uid = fields.IntegerField(db_column='uidNumber', verbose_name=_("Correo electrónico"),unique=True)
    #group = fields.IntegerField(db_column='gidNumber')
    gecos = fields.CharField(db_column='gecos')
    home_directory = fields.CharField(db_column='homeDirectory')
    login_shell = fields.CharField(db_column='loginShell', default='/bin/bash')
    password = fields.CharField(_("Contraseña"),db_column='userPassword' )

    # shadowAccount
    last_password_change = fields.IntegerField(db_column='shadowLastChange', default=utils.unix_timestamp())
    shadow_max = fields.IntegerField(db_column='shadowMax', default=99999)
    shadow_warning = fields.IntegerField(db_column='shadowWarning', default=7)
    #authorized_service = fields.ListField(_("Servicios activados"),db_column='authorizedService')
    ssh_key = fields.ListField(_("Clave SSH"),db_column='sshPublicKey',max_length=30000, blank=True,null=True)

    def __str__(self):
        return self.username

    def __unicode__(self):
        return self.full_name

    @property
    def is_webmaster(self):
        web_domains =[]
        domains = Domain.objects.filter(adminID = self.username)
        for d in domains:
            web_domains.append(d.name)
        return web_domains

    @property
    def ssh_sftp(self):
        res=None
        if 'sshd' in self.authorized_service:
            if int(self.goup) == int(settings.SFTP_GUID):
                res='sftp'
            else:
                res='ssh'
        return res
    def is_valid(self):
        # For unix username string must start with alphabetic character.
        # Only -, _ and . are allowed
        return re.match("^[a-zA-Z0-9]+([\-\.\_]{1}[a-z0-9]+)*$", self.username)


""" Tis model works both for superuser and current users. The only difference is the guid. Superuser has 27"""
class LdapUser(BaseUser):
    group = fields.IntegerField(db_column='gidNumber')
    authorized_service = fields.ListField(_("Servicios activados"),db_column='authorizedService')
    username = fields.CharField(db_column='uid', verbose_name=_("Nombre de usuarix"), primary_key=True)

    def move_to_trash(self):
        if self.is_valid():
            cn = "%s-%s" % (self.username, utils.unix_timestamp())
            try:
                obj = UserTrash.objects.create(
                    cn=cn,
                    otherPath=self.home_directory,
                    typename=self.username,
                    description = utils.ldap_creation_dateTime(),
                    status = 'totrash' )
                return obj

            except Exception as e:
                print("Error moving user to trash", e)
                return None
        else:
            print("invalid username", self.username)
            return None

class SuperUser(BaseUser):
    group = fields.IntegerField(db_column='gidNumber',default=27 )
    authorized_service = fields.ListField(_("Servicios activados"),db_column='authorizedService', default=['cron', 'sshd', 'sudo'])
    username = fields.CharField(db_column='uid', verbose_name=_("Nombre de usuarix"), primary_key=True)
    @property
    def ssh_sftp(self):
        return 'ssh'
