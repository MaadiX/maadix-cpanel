from re import L
from django.utils.translation import gettext_lazy as _
import ldapdb.models
from ldapdb.models import fields
from django.conf import settings

"""Model for modules to be locked"""
class Trash(ldapdb.models.Model):
    object_classes  = ['applicationProcess','top', 'metaInfo','Yap']
    cn              = fields.CharField(db_column='cn', primary_key=True)
    otherPath       = fields.CharField(db_column='otherPath')
    typename        = fields.CharField(db_column='type',null=True)
    description     = fields.CharField(db_column='description')
    status          = fields.CharField(db_column='status')
    l               = fields.CharField(db_column='l', null=True, blank=True, default=None)
    def __str__(self):
        return self.typename
    class Meta:
        abstract = True

class DomainTrash(Trash):
    base_dn = "ou=domains,%s" % settings.LDAP_TREE_TRASH

class UserTrash(Trash):
    base_dn = "ou=users,%s" % settings.LDAP_TREE_TRASH

class MailTrash(Trash):
    base_dn = "ou=mails,%s" % settings.LDAP_TREE_TRASH

class BackupsTrash(Trash):
    base_dn = "ou=backups,%s" % settings.LDAP_TREE_TRASH

class OnionTrash(Trash):
    base_dn = "ou=onions,%s" % settings.LDAP_TREE_TRASH