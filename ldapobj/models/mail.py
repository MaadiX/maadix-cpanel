import re
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
import ldapdb.models
from ldapdb.models import fields
from django.conf import settings
from . import utils
from .trash import MailTrash
from .ldapmodel import LdapModel

class LdapMail(LdapModel):
    # LDAP meta-data
    # associatedDomain is a core class that could be used to solve query by domain
    # Querying by @property is breaking search
    base_dn                 = settings.LDAP_TREE_HOSTING 
    # Nees a custom base_dn to avoid duplicting vd= on editing
    custom_base_dn          = settings.LDAP_TREE_HOSTING
    object_classes          = ['VirtualMailAccount', 'Vacation', 'top', 'VirtualForward', 'amavisAccount']
    forward_help_text  = _("Puedes reenviar los correos electrónicos entrantes a "
                         "una o más cuentas de correo alternativas. Si quieres "
                         "que se reenvíen a múltiples cuentas, separa cada una "
                         "de ellas con una coma (user1@example.com,user2@example.com. "
                         "Recuerda que si quieres seguir recibiendo una copia de los "
                         "correos entrantes en tu cuenta actual "
                         "también tendrás que incluirla en el listado.")
    mail                    = fields.CharField(db_column='mail', primary_key=True)
    uid                     = fields.CharField(_('Email') ,db_column='uid')
    creationDate            = fields.IntegerField(db_column='creationDate', default=utils.ldap_creation_date())
    amavisSpamTagLevel      = fields.FloatField(db_column='amavisSpamTagLevel', default=3.0)
    amavisSpamTag2Level     = fields.FloatField(db_column='amavisSpamTag2Level', default= 5.5)
    amavisSpamKillLevel     = fields.FloatField(db_column='amavisSpamKillLevel', default= 6.0 )
    amavisBypassVirusChecks = fields.BooleanField(db_column='amavisBypassVirusChecks', default=True)
    amavisBypassSpamChecks  = fields.BooleanField(db_column='amavisBypassSpamChecks', default=True)
    smtpAuth                = fields.BooleanField(db_column='smtpAuth', default=True)
    delete                  = fields.BooleanField(db_column='delete', default=False)
    mailbox                 = fields.CharField(db_column='mailbox', default="")
    givenName               = fields.CharField(_("Nombre"),db_column='givenName')
    sn                      = fields.CharField(_("Apellidos"),db_column='sn', null=True, blank=True)
    cn                      = fields.CharField(_("Nombre completo"),db_column='cn')
    accountActive           = fields.BooleanField(db_column='accountActive',default=True)
    lastChange              = fields.CharField(db_column='lastChange', default=utils.unix_timestamp())
    forwardActive           = fields.BooleanField(_("Reenvío automático"), db_column='forwardActive', default=False,blank=True)
    maildrop                = fields.CharField(_("Cuentas de destino"),db_column='maildrop',help_text = forward_help_text, max_length=500,default="",blank=True, null=True)
    vacationActive          = fields.BooleanField(_("Respuesta automática"),db_column='vacationActive', default=False,blank=True)
    vacationInfo            = fields.CharField(_("Mensaje respuesta automática"),db_column='vacationInfo',blank=True, max_length=9999999999999999999 )
    vdHome                  = fields.CharField(db_column='vdHome', default="/home/vmail/domains")
    otherTransport          = fields.CharField(db_column='otherTransport')
    mailAutoreply           = fields.CharField(db_column='mailAutoreply')
    quota                   = fields.CharField("Quota (GB)",db_column='quota', help_text=_("Espacio máximo asignado en GB. Deja vacío o inserta 0 para no asignar límite"),default=0,null=True, blank=True ) 
    password                = fields.CharField(db_column='userPassword')
  

    def __str__(self):
        return self.mail

    @property
    def role(self):
        return "email"

    @property
    def domain(self):
        return self.mail.split('@')[1]

    def build_dn(self):
        """
        Build the Distinguished Name for this entry.
        """
        return "%s,%s" % (self.build_rdn(), self.custom_base_dn)

    def build_rdn(self):
        bits = []
        for field in self._meta.fields:
            if field.db_column and field.primary_key:
                val =  getattr(self, field.name)
                print("VAL", val)
                bits.append("%s=%s,vd=%s" % (field.db_column, val,val.split('@')[1]))
                print("BTS", bits)
        if not len(bits):
            raise Exception("Could not build Distinguished Name")
        res = '+'.join(bits)
        print("RES", res)
        return '+'.join(bits)

    def move_to_trash(self):
        if self.is_valid():
            mail_parts = self.mail.split('@')
            directory = '/home/vmail/%s/%s' % (mail_parts[1],self.mail)
            try:
                cn = "%s-%s" % (self.mail, utils.unix_timestamp())
                obj = MailTrash.objects.create(
                    cn=cn,
                    otherPath=directory,
                    typename=self.mail,
                    description = utils.ldap_creation_dateTime(),
                    status = 'totrash' )
                
                return obj
                
            except Exception as e:
                print("ERROR creating mail trash object", e)
                return None
        else:
            print("Invalid email format")
            return None

    def is_valid(self):
        # just need to make sure we do not delete unwanted folder
        # apply very lax rule, to check that frst part is a valid username
        mail_parts = self.mail.split('@')
        return re.match("^[a-zA-Z0-9]+([\-\.\_]{1}[a-z0-9]+)*$",mail_parts[0])
    
