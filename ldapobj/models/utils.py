# python
import datetime, time 
# django
from django.utils.translation import gettext_lazy as _, get_language
from django.utils.crypto import get_random_string
import gnupg


def unix_timestamp():
    """ Returns Unix timestamp """
    return int(round(time.time() * 1000))

def ldap_creation_date():
    """ Returns creation date in the format used by LDAP """
    return datetime.datetime.now().strftime('%Y%m%d')
def ldap_creation_dateTime():
    """ Returns creation dateTime in the format used by LDAP """
    return datetime.datetime.now().strftime('%Y-%m-%d-%H:%M:%S') 

def encrypt_password(password):
  gpg = gnupg.GPG(gnupghome="/usr/share/mxcp/.gnupg")
  public_keys = gpg.list_keys()
  fingerprint = public_keys[0]['fingerprint']
  encrypted_password = gpg.encrypt(password, fingerprint,always_trust=True)
  return str(encrypted_password)

def generate_string(key_len):
    """ Generate a large password without special chars """
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
    key = get_random_string(key_len,chars)
    return key