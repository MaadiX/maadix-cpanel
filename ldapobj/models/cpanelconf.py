import ldapdb.models
from ldapdb.models import fields
from django.conf import settings


class Cpanel(ldapdb.models.Model):
    base_dn= "dc=example,dc=tld"
    object_classes = ["organizationalUnit", "extensibleObject"]
    ou = fields.CharField(db_column='ou', primary_key=True, default="cpanel")
    organizationalStatus = fields.CharField(db_column='organizationalStatus')
    info = fields.CharField(db_column='info')
    typename= fields.CharField(db_column='type')
    status= fields.CharField(db_column='status')

    def lock(self):
        self.status = "locked"
        self.save()


CPANEL_CONF_TREE = "ou=conf,%s" % settings.LDAP_TREE_CPANEL

"""Basic class for ou=buckets,ou=conf,ou=cpanel,dc=example,dc=tld
    This only creates the firt level object requiered for inserting 
    each application bucket data
"""
class BasicCpanelTree(ldapdb.models.Model):
    base_dn         = CPANEL_CONF_TREE
    object_classes  = ['organizationalUnit']
    ou              = fields.CharField(db_column='ou', primary_key=True)


"""Basic class for php, timereboot, fqdn_domain, logmail_custom"""
class CpanelBasicConf(ldapdb.models.Model):
    """Model for modules configuration"""
    base_dn         = CPANEL_CONF_TREE
    object_classes  = ['organizationalUnit','metaInfo']
    ou              = fields.CharField(db_column='ou', primary_key=True)
    postalAddress   = fields.CharField(db_column='postalAddress', null=True, blank=True, default=None)
    typename        = fields.CharField(db_column='type',null=True, blank=True)
    status          = fields.CharField(db_column='status')

""" Class form email, apache, sshd, which need IPs data"""
class CpanelAdvancedConf(ldapdb.models.Model):
    base_dn             = CPANEL_CONF_TREE
    object_classes      = ['organizationalUnit','metaInfo', 'extensibleObject']
    ou                  = fields.CharField(db_column='ou', primary_key=True)
    ipServiceProtocol   = fields.CharField(db_column='ipServiceProtocol', null=True, blank=True)
    ipServicePort       = fields.CharField(db_column='ipServicePort',null=True, blank=True)
    status              = fields.CharField(db_column='status', null=True, blank=True, default=None)

""" Class with Top used for: fqdn_domain_old """
class CpanelAdvancedTopConf(ldapdb.models.Model):
    base_dn             = CPANEL_CONF_TREE
    object_classes      = ['organizationalUnit','metaInfo', 'top']
    ou                  = fields.CharField(db_column='ou', primary_key=True)
    status              = fields.CharField(db_column='status', null=True, blank=True, default=None)
    typename            = fields.CharField(db_column='type',null=True, blank=True)

"""class for fpmpools"""
"""
class Pools(ldapdb.models.Model):

    base_dn             = CPANEL_CONF_TREE
    object_classes      = ['organizationalUnit','metaInfo', 'top']
    ou                  = fields.CharField(db_column='ou',default="fpmpools" primary_key=True)
    status              = fields.CharField(db_column='status')
"""
"""class for fpm"""
class Fpm(ldapdb.models.Model):
    base_dn             = settings.LDAP_TREE_POOLS
    object_classes      = ['organizationalUnit','metaInfo', 'top']
    ou                  = fields.CharField(db_column='ou', primary_key=True)
    typename            = fields.CharField(db_column='type',null=True, blank=True)

"""class for ou=example.com,ou=opendkim"""
class DkimDomain(ldapdb.models.Model):
    base_dn             = settings.LDAP_TREE_DKIM
    object_classes      = ['organizationalUnit','metaInfo', 'top']
    ou                  = fields.CharField(db_column='ou', primary_key=True)
    typename            = fields.CharField(db_column='type',null=True, blank=True)


"""class for ou=mastodon,ou="buckets"""
class Buckets(ldapdb.models.Model):
        base_dn             = "ou=buckets,%s" % CPANEL_CONF_TREE
        object_classes      = ['organizationalUnit','metaInfo' ]
        ou                  = fields.CharField(db_column='ou', primary_key=True)
        typename            = fields.CharField(db_column='type',null=True, blank=True)
        status              = fields.CharField(db_column='status', null=True, blank=True, default=None)
