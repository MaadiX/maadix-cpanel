from django.utils.translation import gettext_lazy as _
from django import forms
from ldapdb.models import fields
from django.forms import ModelForm        
#Project
from ..models.modules import ModuleTop
from ..models.cpanelconf import CpanelAdvancedConf
from .. import widgets

class RkhunterForm(ModelForm):

    typename = forms.CharField(widget=forms.HiddenInput(),  required=False )
    status  = forms.CharField(widget=forms.HiddenInput(),  required=False )

    class Meta:
        model=ModuleTop
        fields=["typename","status"]


class SSHForm(ModelForm):
    ssh_port_help_text  = _("Puedes cambiar el puerto del servidor SSH, para incrementar la seguridad de tu servidor. "
                            "El puerto por defecto es el 22. Elije otro puerto entre el 2001 y 2010")
    ssh_key_help_text   = _("Activa esta casilla si quieres permitir conexiones ssh únicamente con clave privada. "
                            "Si la activas ya no podrás identificarte con la contraseña para acceder por ssh. Deberás en su lugar  añadir la clave púbila para cada cuenta a la que quieras permitir acceder al servidor por SSH o SFTP")
    ipServicePort = forms.ChoiceField(label=_('Puerto de escucha del servidor SSH'), required=False,help_text=ssh_port_help_text)
    ipServiceProtocol = forms.BooleanField(label=_('Impedir autenticación SSH con contraseña. Se necesitará conectar con clave privada'),help_text = ssh_key_help_text, required=False, widget=widgets.LabelledCheckbox(label=_('Deshabilitar acceso ssh con contraseña')))
    typename = forms.CharField(widget=forms.HiddenInput(),  required=False )
    status  = forms.CharField(widget=forms.HiddenInput(),  required=False )
            
    class Meta:
        model=CpanelAdvancedConf
        fields=["ipServicePort", "ipServiceProtocol","typename","status"]

    def __init__(self, *args, **kwargs):
        choices=[
                (int(22),'Default(22)'),
                (int(2001),'2001'),
                (int(2002),'2002'),
                (int(2003),'2003'),
                (int(2004),'2004'),
                (int(2005),'2005'),
                (int(2006),'2006'),
                (int(2007),'2007'),
                (int(2008),'2008'),
                (int(2009),'2009'),
                (int(2010),'2010'),
                ]
        super().__init__(*args, **kwargs)
        self.fields['ipServicePort'].choices = choices

    def as_p(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<div class="form__field"><p%(html_class_attr)s>%(label)s %(help_text)s %(field)s</p></div>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True)

    def clean(self):
        cleaned_data = super().clean()
        if not self.changed_data:
            raise forms.ValidationError(_("No has cambiado ningún parámetro"))
        ipServicePort = int(self.cleaned_data.get('ipServicePort'))
        #ssh_key_required = self.cleaned_data.get('ipServiceProtocol')

class TlsForm(ModelForm):
    tls_help_text       = _("Configura las versiones de TLS admitidas por tu servidor de correo. TLS 1.0 ya no se considera seguro y se ha eliminado como opción. La opción \"Media\" que acepta TLS 1.2 y 1.3 es la opción recomendada. Si seleccionas la opción \"Alta\", solo se aceptará la TLS 1.3 y será más seguro, pero puedes tener problemas a la hora de enviar correos a servidores que funcionan con versiones inferiores, además <b>Rainloop (webmail) no es compatible con TLS 1.3.</b>")
    ipServiceProtocol_email            = forms.ChoiceField(label=_("Versiones de TLS soportadas para Correo Electrónico"),help_text=tls_help_text,widget=forms.RadioSelect)
    tls_apache_help_text       = _("Configura las versiones de TLS admitidas por tu servidor web. TLS 1.0 ya no se considera seguro y se ha eliminado  como opción. La opción \"Media\" que acepta TLS 1.2 y 1.3 es la opción recomendada. Si seleccionas la opción \"Alta\", solo se aceptará la TLS 1.3 y será más seguro, pero tienes que considerar que algunos servicios pueden dejar de funcionar ya que todavía funcionan con versiones inferiores. Recuerda: La app <b>OnlyOffice de Nextcloud no es compatible con TLS 1.3.</b>")
    ipServiceProtocol_apache            = forms.ChoiceField(label=_("Versiones de TLS soportadas para Servidor web"),help_text=tls_apache_help_text,widget=forms.RadioSelect)

    typename = forms.CharField(widget=forms.HiddenInput(),  required=False )
    status  = forms.CharField(widget=forms.HiddenInput(),  required=False )

    class Meta:
        model=CpanelAdvancedConf
        fields=["typename","status"]

    def __init__(self, *args, **kwargs):
        tlschoises=[
                (4,_("Alta (TLS 1.3)")),
                (3,_("Media (TLS 1.3, TLS 1.2) - Recomendada")),
                (2,_("Baja TLS 1,3, TLS 1.2, TLS 1.1")),
              ]
        super(TlsForm,self).__init__(*args, **kwargs)
        self.fields['ipServiceProtocol_email'].choices = tlschoises
        self.fields['ipServiceProtocol_apache'].choices = tlschoises

    def as_p(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<div class="form__field"><p%(html_class_attr)s>%(label)s %(help_text)s %(field)s</p></div>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True)

    def clean(self):
        cleaned_data = super().clean()
        if not self.changed_data:
            raise forms.ValidationError(_("No has cambiado ningún parámetro"))


