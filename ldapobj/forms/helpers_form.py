from django import forms
from django.contrib.auth.password_validation import validate_password

class GenericForm(forms.Form):
    """
    Generic form to be inherited by regular forms on the site.
    """

    required_css_class = 'required'
    error_css_class    = 'error'

    #  Clean method that check if passwords coincide
    #  In case the form doesn't have both password fields
    #  it will work because both fields will be None
    def clean(self):
        cleaned_data = super(GenericForm, self).clean()
        password           = cleaned_data.get("password")
        confirmed_password = cleaned_data.get("password_2")
        if password and not confirmed_password:
            raise forms.ValidationError(
                _("Por favor confirma la contraseña que has introducido.")
            )

        if password != confirmed_password:
            raise forms.ValidationError(
                _("Las contraseñas no coinciden.")
            )

        if password:
            validate_password(password)


    #  Render fields as divs
    #  and adds a css class to these divs, as we've removed %(html_class_attr)s
    #  this rendering method won't include any class defined by field in the form
    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True
        )
