# django
from django import forms
from django.contrib.auth.password_validation import validate_password
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django.urls import reverse, reverse_lazy
from django.utils.html import escape
from django.forms import ModelForm

#Project
from django.conf import settings
from ..models.domain import LdapDomain as Domain
from ..models.user import LdapUser as User
from ..models.modules import Module
# TODO user ldapobj.utils intead of apps utils
from apps.views import widgets, utils, customfileds


class DomainForm(ModelForm):
    # Headers field
    headerWeb             = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Web"),tag="h3"))
    headerPerm             = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Permisos"),tag="h3"))
    headerMail             = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Correo"),tag="h3"))
    # Help Text field
    name_help_text = _("Inserta un nombre de dominio válido (o un subdominio). ")
    mail_help_text = _("Activa la siguiente casilla si quieres que el correo electrónico "
                      "para este dominio sea gestionado por este servidor. Si el correo está "
                      "gestionado por otro servidor "
                      "deja esta casilla desactivada. Podrás cambiar esta opción en cualquier momento "
                      "desde la página de edición del dominio.")
    webmaster_help_text = _("Concede permisos de edición y escritura a una cuenta que tengas creda "
                            "para que pueda editar o subir archivos a la carpeta del dominio "
                           "que estará ubicada en /var/www/html/.<br> "
                           "Si en el desplegable no aparece ninguna opción, crea antes una cuenta "
                           "SSH (para acceso a la terminal) o SFTP (sin acceso a la terminal y enjaulada en su directorio personal)")
    dkim_help_text=_("Advertencia: si activas la siguiente casilla tendrás que incluir el registro "
                         "DKIM en los DNS de tu dominio. La falta del registro DKiM "
                         "puede generar problemas de entrega de tu correo electrónico.")
    webserver_type_help_text = _("La opción por defecto es Apache.")
    # Fields
    name        = forms.CharField(label=_('Nombre de dominio'),
                 max_length=200, help_text=name_help_text, required=True)
    webServer    = forms.BooleanField(label=_('Servidor Web'),
                     help_text=_('Marca esta casilla si quieres usar este dominio para publicar tu aplicación web.'),
                     widget=widgets.LabelledCheckbox(label=_('Activar Servidor Web')), required=False)
    webserver_type = forms.ChoiceField(label=_("Tipo"),help_text=webserver_type_help_text,widget=forms.RadioSelect)
    onion_service    = forms.BooleanField(label=mark_safe("<i class=\"ti ti-onion-alt\"></i> %s" % _('Tor')),
                     help_text=_('Marca esta casilla si quieres habilitar el dominio .onion'),
                     widget=widgets.LabelledCheckbox(label=_('Activar dominio .onion')), required=False)
    documentRoot     = forms.CharField(label=_('Carpeta raíz'), required=False,  help_text=_("Si dejas este campo en blanco la carpeta raíz será /vaw/www/html/nombre.de.dominio/ . Puedes añadir subcarpetas a  este directorio, por ejemplo insertando public_html/web. En este caso la carpeta raíz será /vaw/www/html/nombre.de.dominio/public_html/web/.  Este valor no se puede editar. Si quieres cambiarlo puedes elimiar el dominio y volverlo a crear. En caso de dudas, deja el espacio en blanco."))
    wordpressInstall    = forms.BooleanField(label= mark_safe("<i class=\"fa fa-wordpress medium-icon\"></i> %s" % _('Instalar Wordpress')),
                                     help_text=_('Si marcas esta casilla se instalará wordpress de forma automatizada. Si ya existe una carpeta no vacía para este dominio en /var/www/html/, la instalación no se llevará a cabo para no borrar de forma accidental ningún contenido.. Tendrias entonces que eliminar o mover manualmente los archivos que contiene para completar la instalación'),
                                     widget=widgets.LabelledCheckbox(label=_('Instalar Wordpress')), required=False)
    
    wpPass1 = forms.CharField(label=_('Contraseña para la cuenta de administración del Wordpress'), widget=forms.PasswordInput(), required=False,  help_text=_("El nombre de la cuenta será el mismo que utilizas para acceder al panel de control"))
    wpPass2 = forms.CharField(label=_('Repite la contraseña'), widget=forms.PasswordInput(), required=False )

    phpPool =  forms.ChoiceField( required = False, widget = forms.Select())
    adminID   = forms.ChoiceField(label=_('Webmaster (Cuenta con acceso de lectura/escrtura a los archivos)'),
                                    help_text=webmaster_help_text,required=True)
    acl = forms.BooleanField(label=  _('Activar ACL (Access Control List)'),
                                     help_text=_('Si marcas esta casilla se añadirán reglas de acceso a la carpeta del dominio. De esta forma tanto la cuenta webmaster como el servidor web tendrán permisos de escritura/lectura y podrás así modifcar archivos creados por la aplicación (por ejemplo, plugins instalados desde la interfaz gráfica de Wordpress).'),
                                     widget=widgets.LabelledCheckbox(label=_('Activar ACL')), required=False)

    accountActive = forms.BooleanField(label=_('Activar servidor de correo para este dominio'),
                                     help_text=mail_help_text, required=False,
                                     widget=widgets.LabelledCheckbox(label=_('Activar servidor de correo')))
    dkim        = forms.BooleanField(label=_('Activar DKIM para este dominio'), help_text = dkim_help_text,
                                     required=False,
                                     widget=widgets.LabelledCheckbox(label=_('Activar DKIM')))

    class Meta:
        model=Domain
        fields=['headerWeb', 'name', 'webServer', 'webserver_type', 'onion_service', 'documentRoot', 'wordpressInstall', 'wpPass1', 'wpPass2', 'phpPool', 'headerPerm', 'adminID','acl' , 'headerMail', 'accountActive','dkim'] 

    #  Render fields as divs
    #  and adds a css class to these divs, as we've removed %(html_class_attr)sshSftps
    #  this rendering method won't include any class defined by field in the form

    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True
        )
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        users_obj= User.objects.all().exclude(group=27)
        super(DomainForm, self).__init__(*args, **kwargs) 
        if "nginx" in self.request.enabled_services:
            webserverchoises=[
                ("apache",_("Apache")),
                ("nginx",_("Nginx")),
              ]
            self.fields['webServer'].widget.attrs = {
                'data-link-display' : 'id_webserver_type'
            }
            self.fields['webserver_type'].choices = webserverchoises
            self.fields['webserver_type'].initial = "apache"
        else:
            del self.fields['webserver_type']
        if not 'tor' in self.request.enabled_services:
            del self.fields['onion_service']

        users = [ user.username for user in users_obj ]
        users.insert(0,'nobody')
        self.fields['adminID'].choices = ((user, user if user != "nobody" else _("Ninguno")) for user in users)
        self.fields['adminID'].choices.insert(0,(None, _('Asignar Webmaster')))

        self.fields['wpPass1'].widget.attrs = {
            'class'    : 'targetToggle'
        }
        self.fields['wpPass2'].widget.attrs = {
            'class'    : 'targetToggle'
        }
        self.fields['wordpressInstall'].widget.attrs = { 'data-multilink-display' : 'targetToggle' }
        pools = utils.get_available_pools(self.request.ldap)
        if pools:
            choices=[(None,_("Ninguno"))]
            for pool in pools:
                if pool["type"] == 'dynamic':
                    domains = len(pool["domains"])
                    choices.append((pool["ou"],pool["ou"]+' (%d)' % domains))
            self.fields[ 'phpPool' ].choices =choices
            # Crazy string creation to preserve transaltions
            # TOD: find a better way to preserevr __proxy__ format
            fpm_str = _('Si instalas un Wordpress o una aplicación PhP en este dominio '
                         'asígnale un pool propio para que se ejecute como un proceso separado reforzando así la seguridad. '
                         'El número entre parentesis indica cuantos dominios están ya asignados al pool. '
                         'Puedes añadir nuevos pools en esta pagina: ')
            link = '<a href="%s">+ Pools</a>' % reverse_lazy('php-fpm')
            self.fields[ 'phpPool' ].help_text= str(fpm_str) + link

    def clean(self):
        cleaned_data = super().clean()
        name  = self.cleaned_data.get('name')
        wordpressInstall = self.cleaned_data.get("wordpressInstall")
        mail_server = self.cleaned_data.get("accountActive")

        # If domain puppet module is locked, nootify user that another process is in progress.
        domain_status = Module.objects.get(ou="domains")
        # For Editform, we only lock domains if one of the following field has been changed
        if ('webServer' in self.changed_data or 'adminID' in self.changed_data or 'phpPool' in self.changed_data or  \
            'deleteWebroot' in self.changed_data or 'wordpressInstall' in self.changed_data or 'onion_service' in  self.changed_data or \
            'acl' in self.changed_data or 'webserver_type' in self.changed_data) and (domain_status.status!='ready'):
            raise forms.ValidationError(
                _("Hay otro proceso de configuración de dominio en curso. Para evitar perdida de datos o errores no se puede añadir otra tarea hasta que acabe el proceso en curso. Vuelve a hacer clic en el botón 'Guardar' pasado un minuto.")
            )

        # If web server has been activated, need to check DNS for let's encrypt
        if self.cleaned_data.get('webServer'):
            dns_records = utils.get_dns_records(name)
            record_message = utils.check_dns_A_record(dns_records)
            if record_message['error']:
                link = reverse_lazy('domains-instructions')
                dns_message = mark_safe(_("La configuración de los DNS no es correcta para poder activar el servidor web. Desactiva el servidor web o consulta las instrucciones para corregirla <a href=\"%s\">aquí</a>." % link))
                raise forms.ValidationError(dns_message)
        # Do not allow activating web_server withou a webmaster
        if self.cleaned_data.get('webServer') and not self.cleaned_data.get('adminID'):
            raise forms.ValidationError(
                _("Asigna una cuenta Webmaster para el dominio")
          )

        # Only allow Wordpress installation if webservers is checked
        if wordpressInstall and not self.cleaned_data.get('webServer'):
            raise forms.ValidationError(_("Para instalr Wordpress tienes que activar el servidor web"))

        #Worpdress Checking - If wordpress installation is checked, require a password
        if wordpressInstall:
            wpPass1 = self.cleaned_data.get("wpPass1")
            wpPass2 = self.cleaned_data.get("wpPass2")
            if not wpPass1 or not wpPass2:
                raise forms.ValidationError(_("Inserta dos veces la contraseña para la cuenta de administración del Wordpress"))
            if wpPass1 != wpPass2:
                raise forms.ValidationError(_("Las contraseñas para la cuenta de administración del Wordpress no coiniciden"))
            else:
                 validate_password(wpPass1)


class AddDomainForm(DomainForm):
    """ Form to add a new domain """

    model=Domain
    fields=['headerWeb', 'name', 'webServer','webserver_type', 'onion_service', 'documentRoot', 'wordpressInstall', 'wpPass1', 'wpPass2', 'phpPool', 'headerPerm', 'adminID','acl' , 'headerMail', 'accountActive','dkim']

    def clean_documentRoot(self):
        documentRoot = self.cleaned_data.get('documentRoot')
        if (documentRoot and utils.not_valid_path(documentRoot)):
            raise forms.ValidationError(
                _("Los caracteres especiales no están permitidos para la carpeta raíz. Solo pueses utilizar los caracteres / - _ ")
        )  
        return documentRoot

    def clean_name(self):
        name  = self.cleaned_data.get('name')
        mail_server = self.cleaned_data.get("accountActive") 
        exists = Domain.objects.filter(name=name)
        if exists:
            raise forms.ValidationError(
                _("El dominio %s ya está activado" % name)
            )

        if name == utils.get_server_host():
            raise forms.ValidationError(
                _("%s es el dominio actual del sistema" % name)
          )
        # TODO: domain_is_in_use is checking if the domain is used by another application
        # in ou=domain,ou=mumble,ou=groups,dc=example,dc=tld 
        # Try to do this query with model 
        if name in utils.domain_is_in_use(self.request.ldap):
            raise forms.ValidationError(
                _("El dominio %s está en uso por otra aplicación" % name)
          )
        if 'mailman' in self.request.enabled_services:
            mailman_domains = utils.get_mailman_domain_names()
            if name in mailman_domains and mail_server:
                raise forms.ValidationError(
                    _("No puedes activar el servidor de correo para el dominio %s. Ya  está en uso por la aplicación Mailman." % name)
                )
        return name

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        super(AddDomainForm,self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['pattern'] = "^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,20}$"

class EditDomainForm(DomainForm):
    old_dkim    = forms.BooleanField(widget=forms.HiddenInput(), required=False)
    name        = forms.CharField(widget=forms.HiddenInput(), required=False)
    deleteWebroot = forms.BooleanField(label=_('Eliminar Carpeta del dominio'),
                                          widget=widgets.LabelledCheckbox(label=_('Eliminar')), required=False)
    wpinstalled = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label= mark_safe("<i class='fa fa-wordpress medium-icon'></i> Wordpress"),tag="h5"))

    class Meta:
        model=Domain
        fields=['headerWeb', 'name', 'webServer', 'webserver_type', 'onion_service','documentRoot','deleteWebroot', 'wpinstalled','wordpressInstall', 'wpPass1', 'wpPass2', 'phpPool', 'headerPerm', 'adminID','acl' , 'headerMail', 'accountActive','dkim', 'old_dkim']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        super(EditDomainForm, self).__init__(*args, **kwargs)
        self.domain_name = self.instance.name
        path = self.instance.path
        self.fields["documentRoot"] = customfileds.HeaderField(
                label=_("Carpeta raíz"),
                required=False,
                label_suffix='',
                widget=widgets.HeaderWidget(label = mark_safe("<i class='fa fa-folder medium-icon'></i> %s" % path),
                tag="pre"))
        self.fields['documentRoot'].widget.attrs['readonly'] = True
        # Set initial value for WebServer
        self.fields['webServer' ].initial = True if (self.instance.status ==  'webon' or self.instance.status == 'online') else False
        # Set initial value for fpm
        self.fields['phpPool'].initial = self.instance.phpPool
        # Set initial vlaue for acl - without a model for acls.
        acl = True if self.instance.acl else False
        self.fields['acl'].initial = acl
        # Set initial value for dkim
        self.fields['dkim'].initial = utils.has_dkim(self.request.ldap, self.domain_name)
        # Check wordpress install status
        self.has_wordpress = False
        wp = self.instance.wordpress 
        if(wp):
            self.has_wordpress = True if (wp.status == 'enabled' or wp.status == 'install') else False
        if self.has_wordpress:
            del self.fields['wordpressInstall']
            del self.fields['wpPass1']
            del self.fields['wpPass2']
            # Need to concatenate strings to apply translation
            dom_str =  _("La carpeta del Wordpress está en /var/www/html/")
            self.fields['wpinstalled'].help_text = dom_str + self.domain_name
        else:
            del self.fields['wpinstalled']
            self.fields['wordpressInstall'].help_text=_('Si la carpeta del dominio ya contiene archivos la instalación no se llevará a cabo para no borrar de forma accidental ningún contenido. Tendrías entonces que eliminar o mover manualmente los archivos que contiene. Recibirás un correo electrónico una vez completada la instalación.')
        # Need to oveeride super class stting for trigger  display
        if 'tor' in self.request.enabled_services:
            self.fields['onion_service' ].initial = True if self.instance.tor else False
            if self.instance.get_onion_address(self.request):
                self.fields['onion_service' ].help_text =  mark_safe("<pre>{}</pre>".format(self.instance.get_onion_address(self.request)))

        if 'nginx' in self.request.enabled_services:
            self.fields['webserver_type' ].initial = self.instance.webserevser_type
            self.fields['webServer'].widget.attrs = { 
                'data-link-hide' : 'id_deleteWebroot',
                'data-link-display' : 'id_webserver_type'}
        else:
            self.fields['webServer'].widget.attrs = { 
            'data-link-hide' : 'id_deleteWebroot',
            }
        self.fields['documentRoot'].widget.attrs['readonly'] = True
        self.din_url = reverse_lazy('checkdns', args=[self.domain_name])
        help_msg = _('Activa esta casilla si quieres que el correo '
                  'electrónico para este dominio sea gestionado por este servidor. '
                  'Puedes averiguar cual es la configuración de DNS en esta pagina: ')
        dns_str =  _("Ver DNS")
        link =" <a href=\"%(link)s\">" + str(dns_str) +"</a>." % ({'link': self.din_url})
        help_msg += link
        mailman_domains = utils.get_mailman_domain_names()
        if self.domain_name in mailman_domains:
            self.fields['accountActive'].help_text = _('No se puede activar el servidor de correo. Este dominio está en suo por la aplicación Mailman')
        else:
            self.fields['accountActive'].help_text = help_msg
        # Dynamic help_textx content
        ip= utils.get_server_ip()
        ip_text = _('Para poder activar el servidor web es necesario que los DNS para este dominio apunten a la IP de este servidor: ')
        self.fields['webServer'].help_text= ip_text + ip
        self.fields['deleteWebroot'].help_text=_('Si el servidor web está desactivado puedes eliminar la carpeta del dominio en /var/www/html/.\
                                     Se guardará una copia en la papelera hasta que la elimines definitivamente.')


    def clean(self):
        cleaned_data = super().clean()
        domain_status = utils.get_cpanel_local_status(self.request.ldap, 'domains')
        # Form has changed ?
        
        if not self.changed_data:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
            )
        
        # Force user to choose a webmaster
        if ('webServer' in self.changed_data or 'adminID' in self.changed_data or 'phpPool' in self.changed_data or  \
            'deleteWebroot' in self.changed_data or 'wordpressInstall' in self.changed_data or 'acl' in self.changed_data) and (domain_status!='ready'):
            raise forms.ValidationError(
                _("Hay otro proceso de configuración de dominio en curso. Para evitar perdida de datos o errores no se puede añadir otra tarea hasta que acabe el proceso en curso. Vuelve a hacer clic en el botón 'Guardar' pasado un minuto.")
            )
            #dns_records = utils.get_dns_records(self.domain_name)

"""This form is to set domains module into locked status when it was in error.
With this we avoid users to modify ldap if a previous transaction failed, so
we preserev consistency"""
class ResetDomainForm(forms.Form):
    header  = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Reanudar Operación Previa"),tag="h3"))
    status        = forms.CharField(widget=forms.HiddenInput(), required=False)
    class Meta:
        model=Module
        fields=["header"]
    
    def __init__(self, *args, **kwargs):
        try:
            self.request = kwargs.pop("request")
            self.instance = kwargs.pop("instance")
        except Exception as e:
            pass
        super().__init__(*args, **kwargs)
        super(ResetDomainForm, self).__init__(*args, **kwargs)
    
    def save(self):
        obj = Module.objects.get(pk="domains")
        obj.lock()
