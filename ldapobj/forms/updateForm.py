from django import forms
from django.forms import ModelForm        
#Project
from ..models.cpanelconf import Cpanel

class UpdateForm(ModelForm):

    typename = forms.CharField(widget=forms.HiddenInput(),  required=False )
    status  = forms.CharField(widget=forms.HiddenInput(),  required=False )

    class Meta:
        model=Cpanel
        fields=["typename","status"]

