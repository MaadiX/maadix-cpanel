import pwd
from urllib import request
from django import forms
from django.forms import ModelForm
from django.contrib.auth.password_validation import validate_password

from django.utils.translation import gettext_lazy as _

import ldapdb.models
from ldapdb.models import fields
from apps.views import widgets, customfileds
from ..models.appsGroups import App, AppDependency
from .helpers_form import GenericForm
from ..utils import is_valid_domain, get_puppet_status, validate_domain_format,only_alphanumeric_characters, \
    not_valid_characters, get_remaining_resources, get_server_ip
from ..dns_utils import check_dns_A_record, get_dns_records
from django.conf import settings


class EditAppForm(ModelForm):

    #headerWeb = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Configurar aplicación"),tag="h3"))

    class Meta:
        model = App
        fields = []

    """ Form to edit user input dependencies for applications 
    This form is dynamically generated from data received from API"""

    def __init__(self,data=None, *args, **kwargs):
        self.request        = kwargs.pop('request')
        self.fields_data    = kwargs.pop('fieldlist')
        self.release_info = get_puppet_status(self.request)
        super(EditAppForm, self).__init__(data,*args, **kwargs)
        obj = self.instance
        self.maintenance = False
        puppet_status = get_puppet_status(self.request)
        if puppet_status['puppetstatus'] == 'error' or puppet_status['puppetstatus'] == 'pending':
            self.maintenance = True
        
        for field in self.fields_data:
            fid      = field['fid']
            label    = field['label']
            helptext = field['helptext']
            widget   = field['widget']
            editable = field['editable']
            choices  = []
            if widget == 'text':
                self.fields[ fid ] = forms.CharField(
                    label    = label,
                    required = field['required'],
                    help_text = helptext
                )
            elif widget == 'password': 
                self.fields[ fid ] = forms.CharField(
                    required = field['required'],
                    widget   = forms.PasswordInput(render_value=True),
                    help_text = helptext
                )
            elif widget == 'select':
                choices = [ line.split(':') for line in field['default_value'].split('\n') ]
                default_value = ((
                    choice[0].strip(), choice[1].strip() 
                ) for choice in choices )
                self.fields[ fid ] = forms.ChoiceField(
                     label = label,
                     required = field['required'],
                     choices = default_value,
                     widget = forms.Select(),
                     help_text = helptext
                 )
            elif widget == 'url':
                self.fields[ fid ] = forms.URLField(
                    required = field['required'],
                    label = label,
                    help_text = helptext
                )
            elif widget == 'fqdn':
                # S3 Proxy fileds are required only if s3 is active 
                # SO it is set to False in API and need to be set TRue 
                if fid == 'proxy':
                    req= True
                else:
                    req = field['required']
                self.fields[ fid ] = forms.CharField(
                    label = label,
                    required =req, 
                    help_text = helptext
                )
            elif widget == 'boolean':
                self.fields[ fid ] = forms.BooleanField(
                    label = label,  
                    help_text = helptext,
                    required = field['required'], 
                    widget = widgets.LabelledCheckbox(
                    label=_('')
                ))
                           
            else:
                self.fields[ fid ] = forms.CharField(
                    label = label,
                    required = field['required'],
                    help_text = helptext

                )

            self.fields[fid].widget.attrs = {
                'data-app' : obj.ou, 
            } if not self.maintenance else {
                'readonly' : 'readonly' 
            }
            # get dependencies values for initial
            fid = field['fid']
            dn = 'ou=%s,ou=%s,%s' % (
                fid, 
                obj.ou, 
                settings.LDAP_TREE_SERVICES
            )
            # Some dependencies has not ab oject in ldap
            # eg: svrypted passwords that cab ne edted baut are not stored
            try:    

                dep = AppDependency.objects.get(dn=dn)
                self.fields[fid ].initial = dep.status
            except Exception as e:
                pass


            field['required'] = field['required_if_installed']

    def clean(self):
        if not self.changed_data:
            raise forms.ValidationError(_("No has cambiado ningún parámetro"))
        message      = ''
        cleaned_data = super().clean()
        for field in self.fields_data:
            value = self.cleaned_data.get(field['fid']) 
            if value:
                widget = field['widget']
                if field['fid'] in  self.changed_data:
                    if widget == 'decimal':
                        qu = self.cleaned_data.get("score")
                        if not qu.replace(".","").isdigit() or float(qu) < 1.0: 
                            raise forms.ValidationError(
                            _("Score tiene que ser un valor decimal separado por coma mayor o igual a uno.")
                        )

                    if widget== 'fqdn_nodns':
                        if not validate_domain_format(value):
                            message = _("Formato de dominio no válido")
                    elif widget.startswith('fqdn'):
                        message = is_valid_domain(
                            domain    = self.cleaned_data.get(field['fid']),
                        )
                        # Do not allow user to add a s3 domain without s3 enabled
                        if field['fid'] == "proxy" and self.instance.ou in settings.S3_APPS:
                            txt = "s3_{}_enabled".format(self.instance.ou)
                            if not self.release_info[txt]:
                                message = _("No tienes activado el almacenamiento S3 para esta aplicacion")

                    elif widget == 'password' and ('coturn' in self.instance.ou or 'onlyoffice' in self.instance.ou):
                        if not only_alphanumeric_characters(value):
                            message = _("Los caracteres especiales no están permitidos para esta contraseña")
                        elif len(value)<14:
                            message = _(
                                'La contraseña ha de tener 14 caracteres '
                                'como mínimo'
                            )      
        if message:
             raise forms.ValidationError(_(message))


class InstallAppForm(GenericForm):
    """ Form to edit user input dependencies for applications """
    
    def __init__(self,data=None, *args, **kwargs):
        self.services_available = kwargs.pop('services_available')
        self.installed_services = kwargs.pop("installed_services")
        self.request = kwargs.pop('request')
        release_info = get_puppet_status(self.request)
        super(InstallAppForm, self).__init__(data,*args, **kwargs)
        for service in self.services_available:
            id = service['settings']['id']
            self.fields[id] = forms.BooleanField(
                label="",  
                required=False, 
                widget=widgets.LabelledCheckbox(
                    label=_('Seleccionar')
                )
            ) 
            self.fields[id].widget.attrs = { 'data-show' : id }
            if ((id == "mastodon" and not release_info['s3_mastodon_enabled']) and not release_info['s3_mastodon_filesystem']) or \
                    ((id == "peertube" and not release_info['s3_peertube_enabled']) and not release_info['s3_peertube_filesystem']):
                self.fields[id] =  customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Solicita instalación sin S3 o contrata S3 desde tu área cliente"),tag="p"))

            # App dependencies
            # Apps
            for app in service['dependencies']:
                self.fields['dependency-%s' % id ] = forms.CharField(
                    widget=forms.HiddenInput(), 
                    required=False
                )
                self.fields['dependency-%s' % id ].widget.attrs={
                    'class' : 'dependency_%s_group' % id,
                    'value' : app['id'],
                }
            # Fields
            for field in service['fieldlist']:
                # Fieldname
                fieldname = 'input-%s-%s' % ( id, field['fid'] )
                widget = field['widget']
                default_value = field['default_value'] 
                # Set up fields according to provided widget
                if widget == 'password': 
                    self.fields[ fieldname ] = forms.CharField(
                        required = False,
                        widget = forms.PasswordInput(render_value=True),
                        initial = default_value,
                    )
                    if (id != 'coturn'):
                        fieldname = 'input-%s-%s-2' % ( id, field['fid'] )
                        self.fields[ fieldname ] = forms.CharField(
                            required = False,
                            label=_("Repetir Contraseña"),
                            widget = forms.PasswordInput(render_value=True),
                            initial = default_value,
                        )

                elif widget == 'select':
                    choices = [ line.split(':') for line in default_value.split('\n') ]
                    default_value = ((
                        choice[0].strip(), choice[1].strip() 
                    ) for choice in choices )
                    self.fields[ fieldname ] = forms.ChoiceField(
                         required = False,
                         choices = default_value,
                         widget = forms.Select()
                     )
                elif widget == 'url':
                    self.fields[ fieldname ] = forms.CharField(
                        required = False,
                        widget = forms.URLInput(),
                        initial = default_value
                    )
                elif widget == 'boolean':
                    self.fields[ fieldname ] = forms.BooleanField(
                        label = "",  
                        required = False, 
                        widget = widgets.LabelledCheckbox(
                        label=_('')
                        ),
                        initial = False,
                    )
                # Rest of fields (char, fqdn, fqdn_nodns)
                else:
                    if (fieldname == "input-mastodon-proxy" and release_info['s3_mastodon_enabled']) or \
                        (fieldname == "input-peertube-proxy" and release_info['s3_peertube_enabled']):
                        req = True
                    else:
                        req = False
                    self.fields[ fieldname ] = forms.CharField(
                        required = req,
                        initial = default_value
                    )

                self.fields[ fieldname ].widget.attrs = {
                    'data-service' : id,
                    'class'        : 'dependency__input',
                }

                if (fieldname == "input-mastodon-proxy" and release_info['s3_mastodon_filesystem']) or \
                        (fieldname == "input-peertube-proxy" and release_info['s3_peertube_filesystem']):
                    del(self.fields[ fieldname ])

            # Hide migrate from mailtrain 1 if it is not installed
            if not "mailtrain" in self.installed_services and "mailtrain2" in self.fields and self.fields.get("input-mailtrain2-migrate"):
                del self.fields["input-mailtrain2-migrate"] 

            # Users 
            for user in service['users']:
                fieldname = 'system-%s-%s' % ( id, user['name'] )
                self.fields[ fieldname ]= forms.CharField(
                    widget   = forms.HiddenInput(), 
                    required = False
                )
                self.fields[ fieldname ].widget.attrs = {
                    'value' : user['name'],
                }
                

    """ This is a dynamically generated form and we the clean method is not aware of
    the fields it is containing.
    We get all available apps form the 'services' dict and check which
    have been marked for installation, as well as dependecies for each of them, to 
    get their input fields and values.
    """
            
    def clean(self):
        
        errors          = {}
        cleaned_data    = super().clean()
        form_domains =[]
        ram_required = 0
        disk_required = 0
        #Store dependencies in list , to finalyy add resources required
        deps_names = []
        total_apps = self.request.enabled_services
        apps_to_install = []
        for service in self.services_available:
            
            appname     = service['settings']['id']
            resources   = service['resources']
            
            if self.cleaned_data.get(appname):
                # Sum resources required by app
                apps_to_install.append(appname)
                #ram_required = ram_required + int(resources['ram'])
                #disk_required = disk_required + int(resources['disk'])
                # Apps
                for app in service['dependencies']:
                    fieldname = 'dependency-%s'% appname
                    dep_group = self.cleaned_data.get(fieldname)
                    # Sum resources requires by dependencies
                    if app['id'] not in total_apps:
                        apps_to_install.append(app['id'])
                    #if app['id'] not in deps_names:
                    #    deps_names.append(app['id'])
                # Fields
                for field in service['fieldlist']:
                    fieldname  = 'input-%s-%s' % ( appname, field['fid'] )
                    value      = self.cleaned_data.get(fieldname)
                    widget     = field['widget']
                    message    = None
                    if value:
                        if widget== 'fqdn_nodns':  
                            if not validate_domain_format(value):
                                message = _("Formato de dominio no válido")
                        elif widget.startswith('fqdn'):
                            message = is_valid_domain(value)
                            if value in form_domains:
                                message = _('No puedes utilizar el mismo dominio/subdominio para diferentes aplicaciones. Cada aplicación necesita su propio dominio'
                                ) 
                            else:
                                dns_records = get_dns_records(value)
                                record_a = check_dns_A_record(dns_records)
                                if record_a['error']:
                                    message = _("El registro DNS de tipo A debe apuntar a la IP %s" % get_server_ip() )
                                else:
                                    form_domains.append(value)
                        elif widget == 'url' :
                            try:
                                forms.URLField().clean(value)                              
                            except:
                                message = _(
                                    'Has de introducir una URL válida, cómo https://example.com'
                                )
                        elif widget == 'password' and (appname == 'coturn' or appname == 'onlyoffice'):
                            if not only_alphanumeric_characters(value):
                                message = _(
                                    'Los caracteres especiales no están permitidos '
                                    'para esta contraseña'
                                )
                            elif len(value)<10:
                                message = _(
                                    'La contraseña ha de tener 14 caracteres '
                                    'como mínimo'
                                )
                        elif widget == 'password':
                            field1= 'input-%s-%s' % ( appname, field['fid'] ) 
                            field2= 'input-%s-%s-2' % ( appname, field['fid'] )
                            value1 = self.cleaned_data.get(field1)
                            value2 = self.cleaned_data.get(field2)
                            if(value1 != value2):
                                message = _(
                                    'Las contraseñas no coinicden'
                                )
                            else:
                                message = validate_password(value1)

                            
                        elif widget != "boolean" and not_valid_characters(value): 
                            message = _("El formato que has introducido no es válido")

                    elif field['required']: 
                        message = _("Campo requerido.")

                    if message:
                        errors[fieldname] = message
                        print(errors)
                
                # Users 
                for user in service['users']:
                    fieldname     = 'system-%s-%s' % ( id, user['name'] )
                    existing_user = '';
                    sysuser       = self.cleaned_data.get(fieldname)
                    if sysuser: 
                        try:
                            existing_user = pwd.getpwnam(sysuser)
                        except Exception as e:
                            print(e)
                    if existing_user:
                        errors[fieldname] = _(
                            'La cuenta %s, reservada para esta aplicación, '
                            'ha sido creada en el sistema. Elimínala para '
                            'poder proceder' % sysuser
                        )
        # Get data about apps from API
        available_resources = get_remaining_resources(self.request.release.get('configurations'), apps_to_install ) 
        if available_resources['memory'] <=0:
            raise forms.ValidationError(_("No tienes memoria suficiente para proceder con la instalación. "
                                        "Para liberar memoria puedes desactivar alguna aplicación que no estés utilizando"))
        if available_resources['disk'] <=0:
            raise forms.ValidationError(_("No tienes espacio en disco suficiente para proceder con la instalación."
                                        "Elimina archivos en desuso o entra en la pestaña Papelera para liberar espacio") )

        if errors.keys():    
            self.add_error( None, _("Algún dato no es válido. Corrige los errores marcados en rojo") )
            for key, value in errors.items():
                self.add_error(key, value)
        return self.cleaned_data


class UpdateAppForm(GenericForm):

    """ Form to edit user input dependencies for applications """
    
    def __init__(self,data=None, *args, **kwargs):
        all_services      = kwargs.pop('all_services')
        enabled_services  = kwargs.pop('enabled_services')
        disabled_services = kwargs.pop('disabled_services')
        removable_servs   = kwargs.pop('removable_servs')
        not_to_disable    = kwargs.pop('not_to_disable')
        not_to_enable     = kwargs.pop('not_to_enable')
        self.php_version       = kwargs.pop('php_version')
        super(UpdateAppForm, self).__init__(data,*args, **kwargs)
        for service in all_services:
            service_id = service['settings']['id'] 
            if service_id in enabled_services and service_id not in not_to_disable:
                self.fields[ 'disable-%s' % service_id ] = forms.BooleanField(
                    label = "",  
                    required = False, 
                    widget = widgets.LabelledCheckbox(
                        label=_('Desactivar')
                    )
                )
            elif service_id in disabled_services:
                self.fields[ 'enable-%s' % service_id ] = forms.BooleanField(
                    label = "",  
                    required = False, 
                    widget = widgets.LabelledCheckbox(
                        label=_('Activar')
                    )
                )
                if service_id in removable_servs and service_id not in not_to_disable:
                    self.fields[ 'remove-%s' % service_id ] = forms.BooleanField(
                        label = "",  
                        required = False, 
                        widget = widgets.LabelledCheckbox(
                        label=_('Eliminar')
                        )
                    )
 
            for dependency in service['dependencies']:
                self.fields[ 'dependency-%s' % service_id ] = forms.CharField(
                    widget=forms.HiddenInput(), 
                    required=False
                )
                self.fields[ 'dependency-%s' % service_id ].widget.attrs = {
                    'class' : 'dependency_%s_group' % service_id,
                    'value' : dependency['id'],
                }
    def clean(self):
        cleaned_data = super().clean()
        if self.cleaned_data.get('enable-owncloud') == True and self.php_version == '8.1':
            raise forms.ValidationError(_("No puedes activar Owncloud con la versión 8.1 de PHP porque no es compatible. Deberías volver a la versión 7.4 para poder reactivar la aplicación")) 
