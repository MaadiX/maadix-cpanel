from django import forms
from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe
from apps.views import utils

from .. import utils as utils
from apps.views import customfileds, widgets
from django.contrib.postgres.forms import SimpleArrayField



class UnbanForm(forms.Form):
    header1             = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Fail2ban - Desbloqueo por IP"),tag="h3"))
    descrption1         = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(
                            label=_("Para desbloquear una IP tienes que averiguar cuál es la IP pública desde la que se está experimentando el bloqueo. "
                            "Puedes buscar  «cual es mi ip» con el buscador para encontrar alguna web que te proporcione esta información, por ejemplo "),tag="p"))
    link1               = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(
                            label=mark_safe ("<a href=\"https://cualesmiip.com\" target=\"_blank\"> https://cualesmiip.com</a>."),tag="p"))
    unban_ip_help_text  = _("Inserta una o más IPs separadas por coma (ej: 1.1.1.1, 2.2.2.2)")
    unban_ip            =  SimpleArrayField(forms.GenericIPAddressField(protocol='IPv4',error_messages={'invalid': _("Inserta una IP válida")}), required=False,label=_('Desbloquear IP'),help_text=unban_ip_help_text)
    header2             =  customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(label=_("Tally - Desbloqueo por nombre de usuarix"),tag="h3"))
    descrption2         = customfileds.HeaderField(label="",required=False,label_suffix='', widget=widgets.HeaderWidget(
                            label=_("El bloqueo por nombre de usuarix solo afecta al acceso por SSH/SFP"),tag="p"))
    unban_user_help_text  = _("Inserta uno o más nombres separados pos coma (ej: user1, user2)")
    unban_user            =  forms.CharField(label=_('Desbloquear Cuenta'), required=False,help_text=unban_user_help_text)

    def clean(self):
        #SimpleArrayField not working for Charfield without model
        # So we validate it manually
        if not self.changed_data:
            raise forms.ValidationError(_("No has introducido ningún parámetro"))
        cleaned_data = super().clean()
        users = self.cleaned_data.get('unban_user')
        items = users.split(',')
        if users:
            for index, item in enumerate(items):
                if not utils.validate_username(item.strip()):
                    raise forms.ValidationError(
                    _("El nombre de usuarix %s no es válido" % item)
                    )
        

    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True
        )
