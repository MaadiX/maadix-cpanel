import psutil
from django import forms
from django.utils.translation import gettext_lazy as _
from .. import utils
from .. import dns_utils

class ChangeFqdnForm(forms.Form):

    fullname         = utils.get_server_hostname()+'.example.com'
    status_help_text = _("inserta el dominio que quieres asignar al servidor. Para %s inserta solo example.com" % fullname)
    name           = forms.CharField(label=_('Nuevo dominio del servidor'),
                         max_length=200, help_text=status_help_text, required=True)

    def __init__(self, *args, **kwargs):
        """ Send data to clean method for validation """
        super(ChangeFqdnForm,self).__init__(*args, **kwargs)
        self.servername = utils.get_server_hostname()
        self.fields['name'].widget.attrs = { 'placeholder' : 'example.com (sin %s)' % self.servername }
    
    
    def clean(self):
        cleaned_data = super().clean()
        """
        If noting has changed do not perform save post
        """
        if len(self.changed_data) == 0:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
            )
        cleaned_data = super().clean()
        servername = utils.get_server_hostname()
        domain  = self.cleaned_data.get('name')
        name = '%s.%s' % (servername, domain)
        memory = psutil.virtual_memory()
        # We want to checck available memory, so user has the chance to reboot server to increase free memory
        # Some free memory is needed to restart services impacted by FQDN change (Apache, Postfix, VPN)
        if(memory.available/(1024*1024) < 200 ):
            raise forms.ValidationError(
                _("No hay suficiente memoria. Reinicia el servidor para liberar memoria y vuelve a intentarlo")
            )
        message = utils.is_valid_domain(name)
        if message:
            raise forms.ValidationError(message)
        mailman_domains = utils.get_mailman_domain_names()
        if name in mailman_domains:
            raise forms.ValidationError(
                _("El dominio %s está en uso por la aplicación Mailman." % name)
          )
        dns_records = dns_utils.get_dns_records(name)
        record_a = dns_utils.check_dns_A_record(dns_records)
        if record_a['error']:
            raise forms.ValidationError(
                _("La configuración de los DNS para El dominio %s no es correcta." % name)
          )

    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True
        )
