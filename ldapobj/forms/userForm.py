# django

import pwd
from django import forms
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth import authenticate
from django.utils.translation import gettext_lazy as _
from django.utils.html import escape
from django.forms import ModelForm
from django.contrib import admin
from django.contrib.auth.forms import UserCreationForm
import ldapdb.models
from ldapdb.models import fields
from ldap3 import  HASHED_SALTED_SHA
from ldap3.core.exceptions import LDAPInvalidCredentialsResult
from ldap3.utils.hashed import hashed

#Project
from django.conf import settings
from ..models.user import LdapUser as User, AdminUser, SuperUser
from ..models.domain import Postmaster
from apps.views import widgets, utils, customfileds

class GenericUserForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super(GenericUserForm, self).clean()
        password           = cleaned_data.get("password")
        confirmed_password = cleaned_data.get("password_2")
        if password and not confirmed_password:
            raise forms.ValidationError(
            _("Por favor confirma la contraseña que has introducido.")
            )
            context['display_form'] = True
        if password != confirmed_password:
            raise forms.ValidationError(
            _("Las contraseñas no coinciden.")
            )
            context['display_form'] = True
        if password:
            validate_password(password)
    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True
        )

class AdminUserForm(GenericUserForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'readonly': True}),disabled=True)
    current_password = forms.CharField(label=_('Contraseña Actual'), widget=forms.PasswordInput(), required=True)
    password       = forms.CharField(label=_('Nueva Contraseña'), widget=forms.PasswordInput(), required=False) 
    password_2     = forms.CharField(label=_('Vuelve a introducir la contraseña'),                                             
                     widget=forms.PasswordInput(), required=False)
    class Meta:
        model=AdminUser
        fields=["username","current_password","password", "password_2", "mail"]
    def __init__(self, *args, **kwargs):

        self.request = kwargs.pop('request')
        role = self.request.role
        super(AdminUserForm,self).__init__(*args, **kwargs)

    def clean_current_password(self):
        username = self.request.user.username
        old_pswd = self.cleaned_data.get('current_password')
        res = authenticate(username=username, password=old_pswd)
        if not res:
            raise forms.ValidationError(_('La contraseña antigua no es correcta ' ))


class PostmasterForm(GenericUserForm):
    password       = forms.CharField(label=_('Nueva Contraseña'), widget=forms.PasswordInput(), required=True) 
    password_2     = forms.CharField(label=_('Vuelve a introducir la contraseña'),                                             
                     widget=forms.PasswordInput(), required=True)
    cn             = forms.CharField(widget=forms.HiddenInput(), initial='postmaster') 

    class Meta:
        model=Postmaster
        fields=["password", "password_2", "cn"]
    
class CreateUserForm(GenericUserForm):
    sshSftp_help_text   = _("SFTP otrorga acceso al servidor sin terminal (solo clientes sftp tipo FileZilla), limitando la cuenta a su directorio personal. Con SSH la cuenta dispondrá de la terminal para lanzar comandos. Una vez asignada una de las dos opciones solo la podrás desactivar pero no cambiar una por otra.")
    password       = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(), required=True) 
    password_2     = forms.CharField(label=_('Vuelve a introducir la contraseña'),                                             
                     widget=forms.PasswordInput(), required=True)
    sshSftp = forms.ChoiceField(
        label=_('Acceso SSH o SFTP al servidor'),
        choices=(
            ("none",_("Sin Acceso")),
            ("sftp",_("Acceso solo por sftp")),
            ("ssh",_("Acceso  SFTP y SSH")),
        ),
        required=True,
        help_text=sshSftp_help_text
        )
    ssh_key_input        = forms.CharField(label=_('Clave SSH'), required=False, widget=forms.Textarea,
                           help_text=_('Inserta la clave pública SSH'))
    instructions   = forms.BooleanField(label=_('Instrucciones de configuración VPN'), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Enviar instrucciones')),
                                        help_text=_("Envía un correo con los archivos de configuración y las instrucciones "
                                                    "para configurar el cliente VPN. <br/>Advertencia: Las instrucciones incluyen todos los "
                                                    "datos necesarios menos la contraseña, que por razones de seguridad debes proporcionar "
                                                    "por otro canal."))
    authorized_service = forms.MultipleChoiceField(choices = (), widget=forms.CheckboxSelectMultiple, required=False)

    class Meta:
        model=User
        fields=['username','first_name','last_name','email', 'password','password_2', 'sshSftp', 'ssh_key_input','authorized_service' ] 
        #TODO : function for selecting available services.

    #  Render fields as divs
    #  and adds a css class to these divs, as we've removed %(html_class_attr)sshSftps
    #  this rendering method won't include any class defined by field in the form
    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True
        )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super().__init__(*args, **kwargs)
        super(CreateUserForm, self).__init__(*args, **kwargs) 
        # if openvpn is not activated in the server hide related_fields
        enabled_services = self.request.enabled_services
        available_permissions = {'jitsi': 'Jitsi', 'phpmyadmin': 'PhpMyAdmin', 'openvpn': 'VPN'}
        apps = available_permissions.keys() & enabled_services
        result = {k:available_permissions[k] for k in apps}
        # permission for phpmyadmin is named apache. replace it
        if 'phpmyadmin' in result:
            result['apache'] = result.pop('phpmyadmin')
        self.fields['authorized_service'].choices = [ (key,value) for key,value in result.items()]
        if not 'openvpn' in enabled_services:
            del self.fields['instructions']


    def clean(self):
        cleaned_data = super().clean()
        username     = self.cleaned_data.get("username")
        openvpn      = self.cleaned_data.get("openvpn")
        instructions = self.cleaned_data.get("instructions")
        email        = self.cleaned_data.get("email")
        existing_user= False
	# if user is not present in ldap, also check in system before validate
        if not utils.validate_username(username):
            raise forms.ValidationError(_("Nombre no válido. Solo se aceptan letras mayúsculas, minúsculas, cifras y los signos \'-',\'_\' y \'.\' siempre que no estén al principio")
            )
            try:
                existing_user = pwd.getpwnam(username)
            except Exception as e:
                utils.p("✕ view_users.py", "User does not exist.: ", e)
            if existing_user:
                raise forms.ValidationError(
                    _("Ya existe una cuenta con ese nombre. Has de escoger un nombre distinto.")
                )
        # check if there's email to send notifications if checked
        if instructions and not email:
        ### It should never happen....email is a required field !!!!!!
            raise forms.ValidationError(
                _("Para poder recibir las instrucciones de configuración de la VPN, es necesaria una dirección de correo. "
                  "Introduce una dirección de correo o desactiva el envío de instrucciones.")
            )
        if not openvpn:
            self.cleaned_data['instructions'] = False

class EditUserForm(CreateUserForm):
    password       = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(), required=False)
    password_2     = forms.CharField(label=_('Vuelve a introducir la contraseña'),    
                                     widget=forms.PasswordInput(), required=False)
    ssh_key              = forms.MultipleChoiceField(label=_('Claves SSH habilitadas'),
                            help_text=_("Desactiva la casilla para eliminar la clave correspondiente"),
                            required=False, 
                            widget=forms.CheckboxSelectMultiple(attrs={"class": "pre"}))
    class Meta:
        model=User
        fields=['username','first_name','last_name','email', 'password','password_2', 'sshSftp', 'ssh_key','ssh_key_input','authorized_service' ] 
        #TODO : function for selecting available services. 

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        super(EditUserForm, self).__init__(*args, **kwargs) 
        self.fields['username' ]= forms.CharField(
            widget   = forms.HiddenInput(),
            required = True
        )  
        all_ssh_keys = self.instance.ssh_key
        if not all_ssh_keys:
            del self.fields["ssh_key"]
        else:
            result = {all_ssh_keys[k]:all_ssh_keys[k] for k in range(0,len(all_ssh_keys))}
            self.fields['ssh_key'].choices = [ (key,value) for key,value in result.items()]
        # Is the user has already sshd permission menas it is already a 
        #ssh or sftp user and this status can not be changed
        if 'sshd' in self.instance.authorized_service:
            value = 'sftp' if self.instance.group == settings.SFTP_GUID else 'ssh'
            
            self.fields['sshSftp' ]= forms.CharField(
                widget   = forms.HiddenInput(),
                required = False
            )
            self.fields['sshSftp' ].initial = value

class EditSuperUserForm(EditUserForm):
    current_password = forms.CharField(label=_('Contraseña actual de acceso al panel de control'), widget=forms.PasswordInput(), required=True)
    class Meta:
        model=User
        fields=['username','first_name','last_name','email', 'password','password_2', 'ssh_key','ssh_key_input', 'authorized_service' , 'current_password']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        super(EditUserForm, self).__init__(*args, **kwargs)
        all_ssh_keys = self.instance.ssh_key
        if not all_ssh_keys:
            del self.fields["ssh_key"]
        else:
            result = {all_ssh_keys[k]:all_ssh_keys[k] for k in range(0,len(all_ssh_keys))}
            self.fields['ssh_key'].choices = [ (key,value) for key,value in result.items()]
        self.fields['username' ]= forms.CharField(
            widget   = forms.HiddenInput(),
            required = True
        ) 
        self.fields['sshSftp' ]= forms.CharField(
            widget   = forms.HiddenInput(),
            required = False
        )

    def clean_current_password(self):
        username = self.request.user.username
        old_pswd = self.cleaned_data.get('current_password')
        res = authenticate(username=username, password=old_pswd)
        if not res:
            raise forms.ValidationError(_('La contraseña de acceso al panel de control ' ))

