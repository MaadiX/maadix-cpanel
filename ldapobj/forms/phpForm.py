from urllib import request
from django import forms
from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe
from apps.views import widgets, customfileds
from ..models.cpanelconf import CpanelBasicConf

class PhpVersionForm(forms.ModelForm):
    php_version_help_text = _("Puedes elegir cual versión de PhP quieres utilizar. Algunas versiones antiguads de ciertas plataformas (Drupal, Laravel etc) pdorían no funcionar con versiones más nuevas de PHP.")
    status = forms.ChoiceField(label=_("Selecciona la versión de Php"),help_text=php_version_help_text,widget=forms.RadioSelect)

    class Meta:
        model=CpanelBasicConf
        fields=["status"]

    def __init__(self,*args, **kwargs):

        versionchoices=[
            ('7.4',"7.4"),
            ('8.1',"8.1")
        ]
        self.request = kwargs.pop("request")
        super(PhpVersionForm,self).__init__(*args, **kwargs)
        if 'owncloud' in self.request.enabled_services:
            versionchoices=[
                ('7.4',"7.4"),
            ]
            self.fields['version'].help_text = _("No puedes utilizar otra versión de Php porque Owncloud está instalado y actualmente no tiene soporte para versiones mayores de PHP. Si quieres utilizar Php 8.1 deberías antes desactivar/desinstalar Owncloud") 
        self.fields['status'].choices = versionchoices

    def clean(self):
        cleaned_data = super().clean()
        """
        If noting has changed do not perform save post
        """
        if len(self.changed_data) == 0:
            raise forms.ValidationError(
                _("No has cambiado ningún valor.")
            )
class AddPoolForm(forms.Form):
    # Validation of this form is done in View post method
    newpools = forms.CharField(label=_('Inserta el número de Pools que quieres crear'), required=False, widget=forms.TextInput(attrs={'size':4}))

class PhpFpmForm(forms.Form):


    def __init__(self,*args, **kwargs):
        tootltip_text = _("Para poder desactivar este pool tienes antes que quitar el dominio asociado desde la página de edición del dominio")
        self.allowed_pools = kwargs.pop("allowed_pools")
        self.fpms = kwargs.pop("user_fpms")
        self.deletable_fpms = []
        greater_fpm = int(0)
        choices_full = [(None,_('Desactivado')),('dynamic',_('Activar')),('delete',_('Eliminar'))]
        choices_partial = [(None,_('Desactivado')),('dynamic',_('Activar'))]
        super(PhpFpmForm, self).__init__(*args, **kwargs)
        if(self.fpms):
            for fpm in self.fpms:
                deletable = 'False'
                domains=''
                name = fpm["ou"]
                fpm_id = name.replace("fpm","",1)
                if fpm["type"] == 'dynamic':
                    # We set the field name as the status we will give
                    # to the fpm if the Checkbox is selecetd.
                    #So if current status is dynamic (which means enabled) we only can disable the fpm
                    name = 'disabled-%s' % name
                    # As  we can not delete fmps with is lower that actives fpms
                    # we store the greater enabled id found among enabled fpms
                    if fpm_id.isdigit() and int(fpm_id) > int(greater_fpm):
                        greater_fpm = fpm_id
                    # If there is any domain assigned to this pool we can not disable it
                    # We are creating 3 pools by default. So do not let user delete a fmp pool lower than 3
                    if fpm["domains"] or int(fpm_id) < 4:
                        if int(fpm_id) < 4:
                            tootltip_text = _("Los primeros 3 Pools se crean por defecto y no se pueden eliminar")
                        self.fields[name]= customfileds.HeaderField(
                                label=fpm["ou"],required=False,label_suffix='', 
                                widget=widgets.HeaderWidget(
                                label=mark_safe(
                                "<div class='tooltip'>&#9432;<span class='tooltiptext'>%s</span></div>" % tootltip_text
                                ),
                                tag="span"))
                        for d in fpm["domains"]:
                            domains=domains + '\n '+ d
                        self.fields[name].help_text = domains
                    else:
                        self.fields[name] =forms.BooleanField(label=fpm["ou"], label_suffix="",required= False)

                else:
                    #If current status is disabled there are two possible actions:
                    # Delete the fpm or enable it.
                    # We set a generic edit field name and the action will be given
                    # by the choices option
                    name = 'edit-%s' % name
                    deletable = 'True'
                    # We first include both actions : delete and enable
                    self.fields[name]=forms.CharField(label=fpm["ou"], required=False, widget=forms.Select(choices=choices_full))
                    # Store yjis fpm_id into deleteable list. If we then find a greater enabled fpm
                    # We will remove the delete option
                    self.deletable_fpms.append(fpm_id) 

        # Cmpare each id in deletable_fpms with greater_fpm removing delete option if it s greater 
        for fpmid in self.deletable_fpms:
            if int(fpmid) <= int(greater_fpm):
                self.fields['edit-fpm%s' % fpmid]=forms.CharField(label='fpm%s' % fpmid,required= False, widget=forms.Select(choices=choices_partial))

    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(help_text)s %(field)s %(label)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=False
        )
