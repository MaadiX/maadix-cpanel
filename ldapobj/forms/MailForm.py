# django
import pwd
from urllib import request
from django import forms
from django.contrib.auth.password_validation import validate_password
from django.core.validators import validate_email
from django.utils.translation import gettext_lazy as _
from django.utils.html import escape
from django.contrib import admin
from django.contrib.auth.forms import UserCreationForm
import ldapdb.models
from ldapdb.models import fields

#Project
from django.conf import settings
#from ..models.user import LdapUser as User
from ..models.mail import LdapMail as Mail
from ..models.domain import LdapDomain as Domain , Postmaster
from apps.views import utils 
# This form checks if password matches
from .userForm import GenericUserForm

class AddMailForm(GenericUserForm):
    name       =  forms.CharField(label=_('Nombre de la cuenta (sin dominio)'), help_text=_("Para info@example.com inserta info y selecciona example.com desde el desplegable del campo anterior"))
    mail       =  forms.CharField(widget=forms.HiddenInput(), required=False)
    domain     = forms.ChoiceField(label=_('Dominio'), required=True,
                                   help_text=_("Elige el dominio para la cuenta que quieres crear"))
    password   = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(), required=True)
    password_2 = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=True)

    class Meta:
        model=Mail
        fields=['domain','name','mail', 'password','password_2', 'givenName', 'sn', 'quota' ] 
        #TODO : function for selecting available services.

    #  Render fields as divs
    #  and adds a css class to these divs, as we've removed %(html_class_attr)sshSftps
    #  this rendering method won't include any class defined by field in the form
    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True
        )
    def __init__(self, *args, **kwargs):
        self.request= kwargs.pop('request')
        super().__init__(*args, **kwargs)
        super(AddMailForm,self).__init__(*args, **kwargs)

        self.fields['mail'].widget.attrs = {
            'pattern' : '^[\w]{1,}[\w.+-_]{0,}'
        }
        role = self.request.user.role 
        if role == "postmaster":
            postm_obj = Postmaster.objects.get(mail=self.request.user.username)
            self.fields['domain'] = forms.CharField(
                required = False
            )
            self.initial['domain'] = postm_obj.domain
            self.fields['domain'].widget.attrs = { 
                'readonly' : True,
                'class'    : 'disabled'
            }
        else:
            domains = Domain.objects.all()
            self.fields['domain'].choices = ((domain.name, domain.name) for domain in domains)

        #self.fields['at'].widget.attrs['readonly'] = True

    def clean(self):
        super().clean()
        quota = self.cleaned_data.get("quota")
        qu = quota.replace(",",".")
        if not qu.replace(".","").isdigit() or float(qu) < 0.0:
            raise forms.ValidationError(
                _("El valor Quota tiene que ser un número mayor o igual a cero.")
            )

    def clean_name(self):
        email_name = self.cleaned_data['name']
        if not utils.validate_username(email_name):
            raise forms.ValidationError(
                _("El nombre que has escogido usa caracteres no permitidos.")
            )
        domain     = self.cleaned_data['domain']
        mail      = '%s@%s' % (email_name, domain)
        exist = Mail.objects.filter(mail=mail)
        # abuse@ created by former cpanel were wrong for receiving emails 
        # but are not included in this query
        # So it can still be created if it does not exists as correct mail object
        if exist:
            raise forms.ValidationError(_("La cuenta %s ya existe") % mail)
        if email_name == 'abuse':
            #TODO: fix former abuse accounts that can not be used
            # we need to delete it first as it has not correct classes 
            # and can not be a Mail object
            dn = "mail=abuse@%s,vd=%s,%s" % (domain,domain, settings.LDAP_TREE_HOSTING)
            utils.recursive_delete(self.request.ldap, dn)

        return email_name


    def clean_mail(self):
        email_name = self.cleaned_data.get('name')
        domain     = self.cleaned_data['domain']
        mail      = '%s@%s' % (email_name, domain)
        return mail

    def clean_givenName(self):
        return self.cleaned_data['givenName'] if self.cleaned_data['givenName'] else self.cleaned_data.get('name')

    def clean_sn(self):
        return self.cleaned_data['sn'] if self.cleaned_data['sn'] else self.cleaned_data.get('name')

class EditMailForm(GenericUserForm):
    forward_help_text  = _("Puedes reenviar los correos electrónicos entrantes a "
                         "una o más cuentas de correo alternativas. Si quieres "
                         "que se reenvíen a múltiples cuentas, separa cada una "
                         "de ellas con una coma (user1@example.com,user2@example.com. "
                         "Recuerda que si quieres seguir recibiendo una copia de los "
                         "correos entrantes en tu cuenta actual "
                         "también tendrás que incluirla en el listado.")

    mail = forms.CharField(widget=forms.TextInput(attrs={'readonly': True}),disabled=True)
    password   = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(), required=False)
    password_2 = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=False)
    vacationInfo   = forms.CharField(label=_('Mensaje de respuesta automática'), required=False,
                                            widget=forms.Textarea,
                                            help_text=_('Inserta el texto del mensaje de respuesta automática.'))


    class Meta:
        model=Mail
        fields=['mail', 'password','password_2', 'givenName', 'sn', 'quota',"forwardActive", "maildrop", "vacationActive", "vacationInfo"] 

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        role = self.request.role
        super(EditMailForm,self).__init__(*args, **kwargs)
        if role == "email":
            self.fields['quota'].widget.attrs = {
                'readonly' : True,
                'class'    : 'disabled'
            }
            self.fields['quota'].help_text = _("Espacio máximo permitido en GB")

        self.fields['forwardActive'].widget.attrs = { 'data-link-display' : 'id_maildrop' }
        self.fields['vacationActive'].widget.attrs = { 'data-link-display' : 'id_vacationInfo' }

    def clean(self):
        super().clean()
        vacationActive = self.cleaned_data.get("vacationActive")
        vacationInfo = self.cleaned_data.get("vacationInfo")
        if vacationActive and not vacationInfo:
            raise forms.ValidationError(_("Si activas la respuesta automática, es necesario insertar un texto para el mensaje automático"))
        forward_active = self.cleaned_data.get("forwardActive")
        maildrop = self.cleaned_data.get("maildrop")
        if forward_active and maildrop is None:
            raise forms.ValidationError(_("Si activas el renvío automático, es necesario insertar almenos una cuenta de destino"))
        if maildrop:
            emails   = maildrop.split(",")
            for email in emails:
                email = email.strip()
                validate_email(email)
