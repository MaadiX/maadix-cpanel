#pythoncheck-borg
import json
# django
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.http import  HttpResponse, JsonResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_protect
# project

@csrf_protect
def get_json_data(request):
    try:
        with open(settings.ONION_JSON) as f:
            data = json.load(f)
            print("DADADADA", data)
            for item,val  in data['maadix_tor_hidden_services']:
                print(item, val)
    except Exception as e:
        print("views_backups.py - There was an erro getting json data")
        print(e)
    return JsonResponse(data)