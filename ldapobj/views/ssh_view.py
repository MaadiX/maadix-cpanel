# django
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
# project
from ..models.cpanelconf import CpanelAdvancedConf
from ..mixins import AdminUserPassesTestMixin
from ..forms.system_forms import SSHForm
from .. import utils

class SSHConfig(AdminUserPassesTestMixin, FormView):
    template_name = 'ldapobj/pages/security-config.html'
    form_class    = SSHForm
    model = CpanelAdvancedConf

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_object(self):
        # sshg config dn may not exists in ldap
        try:
            obj = CpanelAdvancedConf.objects.get(ou="sshd")
            return obj
        except CpanelAdvancedConf.DoesNotExist:
            obj = CpanelAdvancedConf.objects.create(ou="sshd", ipServicePort='22',ipServiceProtocol=False)
        return obj

    def get_context_data(self, **kwargs):
        context = super(SSHConfig, self).get_context_data(**kwargs)

        context['subtitle'] = _("Configuración Servidor SSH")
        return context

    def get_form(self):
        obj = self.get_object()
        ipServicePort = obj.ipServicePort if obj.ipServicePort else '22'
        ipServiceProtocol = utils.ldap_val(obj.ipServiceProtocol) if obj.ipServiceProtocol else False
        kwargs = self.get_form_kwargs()
        kwargs['initial'] = {"ipServicePort": ipServicePort, "ipServiceProtocol": ipServiceProtocol }
        print(kwargs['initial'])
        return self.form_class(**kwargs)

    def form_valid(self,form, *args, **kwargs):
        form = self.get_form()
        obj = self.get_object()
        obj.ipServicePort  = form["ipServicePort"].value()
        obj.ipServiceProtocol = form["ipServiceProtocol"].value()
        context = self.get_context_data()
        try:
            obj.save()
            utils.lock_cpanel()  
            return HttpResponseRedirect( reverse('logout') ) 
        except Exception as e:
             messages.error(self.request, _('Se ha producido un error. No se ha podido actualizar la configuración'))
             print("ERRROR ", e)
        return self.render_to_response(context)
