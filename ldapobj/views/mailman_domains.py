# python
import os
# django
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
# project
from .. import utils
from ..models.domain import Dkim, LdapDomain
from django.conf import settings
from ..mixins import AdminUserPassesTestMixin

class MailmanDomainsView(AdminUserPassesTestMixin, DetailView):
    """
    Mailman Domains list view.
    """
    def get(self, request, *args, **kwargs):
        active_mail_domains = utils.get_active_maildomains(request.ldap)
        mailman_domains     = utils.get_mailman_domains()
        active              = _('Activado')
        inactive            = _('Desactivado')
        processing          = _('Procesando')
        has_dkim = False
        # Get dkim status in cpanel..if locked the puppet apply is going to be run
        # and dkim for mailman domains will be automatically creates

        for domain in mailman_domains:
            domain['dkim_status'] = False
            try:
                #if dkim entry already exists in ldap sme=how status
                # dkim caild be already created or in progress 
                Dkim.objects.get(ou=domain['mail_host'])
		        # Check if dkim certificate exists in server 
                dkim_path = "/etc/opendkim/keys/%s/default.txt" % domain['mail_host']
                if os.path.isfile(dkim_path):
                    domain['DKIM'] = "<span class='fa fa-power-off enabled'></span> %s" % active  
		    # ou=aaa.com,ou=opendkim,ou=cpanel,dc=example,dc=tld

                # If dkim status is not ready, dkim is going to be created
                else:        
                    domain['DKIM'] = "<span class='fa fa-spin fa-cog'></span> %s" % processing 
                    # Run puppet local  to create dkim for mailman domains 
                    #domain['dkim_status'] =True 

            except Exception as e:
                print("There was a problem retrieving DKIM entry in LDAP: ")
                print( str(e) )
                #domain['DKIM'] = _('La clave DKIM no está activada para este dominio')
                domain['DKIM'] = "<span class='fa fa-power-off disabled'></span> "
                domain['dkim_status'] =True

            #domain['dkim_status'] = False
            # Check vhost web status for given domain
            if domain['mail_host'] in active_mail_domains:
                domain['status'] = "<span class='fa fa-power-off enabled'></span> %s" % active
            else:
                domain['status'] = "<span class='fa fa-power-off disabled'></span> %s" % inactive
            # Check if dkim certificate exists in server 
        return render(request, 'ldapobj/pages/mailman-domains.html', locals())

class CreateDkim(AdminUserPassesTestMixin, FormView):
    """ View to delete an entry from LDAP """

    def post(self, request):
        try:
            dname  = request.POST.get('dn')
            url = request.POST.get('url')
            # Create  item 
            domain_obj  = LdapDomain(name=dname)
            domain_obj.set_dkim()
            # set_dkim metho locks dkim cpanel
            messages.success(self.request, _('Creando certificado dkim para el dominio %s' % dname))
            return HttpResponseRedirect(url)
        except Exception as e:
            if settings.DEBUG:
                print(e)
            messages.error(self.request, _('Hubo algún problema creando el certificado Dkim '
                                           'Contacta con los administrador-s si el problema persiste'))
            return HttpResponseRedirect(url)  