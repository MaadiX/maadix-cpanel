import psutil

# django
from django.utils.translation import gettext_lazy as _
#from django import views
from django.views.generic.detail import DetailView
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST
#from django.views.generic.edit import FormView

# project
from .. import utils
from .. import dns_utils
from ..models.cpanelconf import Cpanel
from ..mixins import AdminUserPassesTestMixin


class Details(AdminUserPassesTestMixin, DetailView):
    """
    System details view.
    """
    graph_width  = 240
    graph_height = 120

    def get_object(self):
        obj = Cpanel.objects.get(ou="cpanel")
        return obj

    def get_height(self, percent):
        """ Util function to get the height of a value from the related percent. """
        return round(self.graph_height * percent/100)

    def get(self, request):
        fresh_install = False
        start_release  = ''
        cur_release    = ''
        release_number = ''
        """ Force Users to check security settings"""
        # In release 202201 there are not security features added.
        # So we remove this forcing
        # We preserve fres install info for future use
        obj = self.get_object()
        cur_release = obj.typename
        start_release = obj.info
        release_number = cur_release.split('_', 1 )
        # Make differnce among fresh new install or upgraded VM
        if(start_release == cur_release):
            # If it is a fresh install VPN can not be in use. 
            # Avoid alert if then they install VPN and have not dismissed alert 
            # Security alert is to be shown in any case 
            fresh_install = True
 
        # Disable security popup 
        """Handle GET requests."""
        graph_width  = self.graph_width
        graph_height = self.graph_height
        cpu_bars_n = 20
        cpu_barwidth = round(graph_width / cpu_bars_n)
        cpu_bars = [ round(cpu_barwidth * i + 5) for i in range(cpu_bars_n) ]
        # Get system memory stats
        memory    = psutil.virtual_memory()
        ram_size  = self.get_height(memory.percent)
        # Get system CPU stats
        cpu       = psutil.cpu_percent()  # percentage of used cpu
        cpu_count = psutil.cpu_count(logical=False) # number of physical cpus
        cpu_size  = self.get_height(cpu)
        # Get disk usage stats
        disk_usage     = psutil.disk_usage('/')  # gets disk usage of the partition in which the folder is
        disk_size      = self.get_height(disk_usage.percent)
        # DNS for FQDN
        host = utils.get_server_host()
        dns = dns_utils.get_all_dns(host)

 
        self.release_number = release_number
        return render(request, 'ldapobj/pages/details.html', locals())



@csrf_protect
@require_POST
def get_cpu_usage(request):
    """
    View to get the CPU usage of the host machine.
    """
    cpu = psutil.cpu_percent()
    return HttpResponse(cpu, content_type="application/json")
