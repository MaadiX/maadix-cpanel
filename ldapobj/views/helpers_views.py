from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from ..models.modules import Module
from ..utils import *

class PassRequestToFormViewMixin:
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

class BaseLdapModelsView(View):
    # Using dict of dict in case more than one module has to be locked
    modules = {
        "LdapDomain"   : {"domains"},
        "LdapUser"     : {"domains"},
        "LdapMail"     : {"domains"}, 
        "pgpCertID"     : {"zeyple"},

        }

    def post(self, request):
        if request.method=='POST':
            #TODO: check if module domain is locked.
            try:
                selected_action = request.POST.__getitem__('action')
                selected_ids = request.POST.getlist('remove')
                object_list = self.model.objects.filter(pk__in=selected_ids)
                print("OBJECT list", object_list)
                if object_list.count() < 1:
                    messages.error(request, _("Selecciona al menos un elemento"))
                    return HttpResponseRedirect(self.request.get_full_path()) 
                context = {
                    'action_name' : selected_action,
                    'deleted_objects' : object_list,
                    'model'       : self.model,
                }
                print("HeLLLLLO")
                print("selected_action",selected_action)
                if selected_action == 'delete_selected':
                    try:
                        action_approved = request.POST.get('action_approved')
                        print("action_approved",action_approved)
                        if action_approved == 'on':
                            print("Action approved", action_approved)
                            deleted = self.delete_objects(selected_ids)
                            context['errors'] = deleted['errors']
                            context['success'] = deleted['success']
                            if(deleted['success']):
                                self.lock_module()
                            return render(request,"ldapobj/delete/delete_confirmation.html", context)
                        else:
                            return render(request,"ldapobj/delete/delete_selected_confirmation.html", context)
                    except KeyError:
                        #action_approved param is not available
                        #show the objects check page for delete approval
                        return render(request,"ldapobj/delete/delete_selected_confirmation.html", context)
                else:
                    messages.error(request, _("Tienes que seleccionar una opción"))
                    return HttpResponseRedirect(self.request.get_full_path()) 
            except Exception as e:
                print(e)
        return HttpResponse(self.request.get_full_path())

    def delete_objects(self, selected_ids: list):
        errors = []
        ok = []
        
        for item in selected_ids:
            try:
                obj = self.model.objects.get(pk=item)
                obj.delete_recursive()
                # Then delete object
                self.model.objects.filter(pk=item).delete()
                # Then move to trash
                obj.move_to_trash()
                ok.append(item)
            except Exception as e:
                print(e)
                #transaction.rollback()
                errors.append(item)
        return {'errors' : errors, 'success': ok}

    def lock_module(self):
       module =  self.model.__name__
       toLock = self.modules[module]
       for item in toLock:
           obj = Module.objects.get(pk=item)
           obj.lock()

