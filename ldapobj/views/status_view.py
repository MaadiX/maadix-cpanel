# django
from django import views
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.http import  HttpResponse 
from django.conf import settings
# project
from ..utils import get_cpanel_status 

class CheckCpanelStatus(views.View):
    """ View to delete an entry from LDAP """
    #@method_decorator(csrf_exempt)
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        """ Need to allow post from others domains and without token, 
            for cases of fqdn change """
        response = super(CheckCpanelStatus, self).dispatch(*args, **kwargs)
        response["Access-Control-Allow-Headers"] = "X-CSRFToken"
        response["Access-Control-Allow-Origin"] = "*" 
        response["Access-Control-Allow-Methods"] = "POST, OPTIONS"
        return response

    def post(self, request):

        try:
            status = get_cpanel_status()
        except Exception as e:
            if settings.DEBUG:
                print(e)
            # If there is some error accesing LDAP the cpanel is lcoked 
            status = 'locked'
        return HttpResponse (status)
