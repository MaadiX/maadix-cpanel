# django
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
# project
from ..models.cpanelconf import CpanelAdvancedConf
from ..mixins import AdminUserPassesTestMixin
from ..forms.system_forms import TlsForm
from .. import utils

class TlsConfig(AdminUserPassesTestMixin, FormView):
    template_name = 'ldapobj/pages/security-config.html'
    form_class    = TlsForm
    model = CpanelAdvancedConf

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_object(self, service):
        try:
            obj = CpanelAdvancedConf.objects.get(ou=service)
            return obj
        except CpanelAdvancedConf.DoesNotExist:
            obj = CpanelAdvancedConf.objects.get(ou=service, ipServiceProtocol=3)
            return None

    def get_context_data(self, **kwargs):
        context = super(TlsConfig, self).get_context_data(**kwargs)
        context['subtitle'] = _("Configuración TLS")
        return context

    def get_form(self):
        obj_apache = self.get_object("apache")
        obj_email = self.get_object("email")
        ipServiceProtocol_apache = obj_apache.ipServiceProtocol
        ipServiceProtocol_email = obj_email.ipServiceProtocol
        kwargs = self.get_form_kwargs()
        kwargs['initial'] = { "ipServiceProtocol_email": ipServiceProtocol_email, "ipServiceProtocol_apache": ipServiceProtocol_apache }
        print(kwargs['initial'])
        return self.form_class(**kwargs)

    def form_valid(self,form, *args, **kwargs):
        form = self.get_form()
        obj_email = self.get_object("email")
        obj_apache = self.get_object("apache")
        obj_email.ipServiceProtocol = form["ipServiceProtocol_email"].value()
        obj_apache.ipServiceProtocol = form["ipServiceProtocol_apache"].value()
        context = self.get_context_data()
        try:
            obj_email.save()
            obj_apache.save()
            utils.lock_cpanel()  
            return HttpResponseRedirect( reverse('logout') ) 
        except Exception as e:
             messages.error(self.request, _('Se ha producido un error. No se ha podido actualizar la configuración'))
             print("ERRROR ", e)
        return self.render_to_response(context)
