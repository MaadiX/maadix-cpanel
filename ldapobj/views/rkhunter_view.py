# django
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
# project
from ..models.modules import ModuleTop
from ..mixins import AdminUserPassesTestMixin
from ..forms.system_forms import RkhunterForm


class RkhunterConfig(AdminUserPassesTestMixin, FormView):
    template_name = 'ldapobj/pages/rkhunter.html'
    form_class    = RkhunterForm
    model = ModuleTop

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_object(self):
        try:
            obj = ModuleTop.objects.get(ou="rkhunter")
        except ModuleTop.DoesNotExist:
            obj = ModuleTop.objects.create(ou="rkhunter", status='ready')
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        rkhunter_status = self.get_object().status
        context['rkhunter'] = rkhunter_status  
        return context

    def form_valid(self,form, *args, **kwargs):
        form = self.get_form()
        obj = self.get_object()
        context = self.get_context_data()
        try:
            obj.lock()  
            messages.success(self.request, _('Cambios aplicados con éxito'))
            return HttpResponseRedirect(self.request.get_full_path())
        except Exception as e:
             messages.error(self.request, _('Ha habido un error. No se ha podido actualizar la configuración'))
             print("ERRROR ", e)
        return self.render_to_response(context)
