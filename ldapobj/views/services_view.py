# python
import  psutil

# django
from django.utils.translation import gettext_lazy as _
from django.db.models import Q
from django.contrib import messages
from django.urls import reverse
from django.http import  HttpResponseRedirect
from django.urls import reverse
from django.views.generic.edit import FormView

# contrib
from ldap3 import MODIFY_REPLACE

# project
from ..utils import get_puppet_status, p, lock_cpanel, generic_update_or_create_ldap_entry,\
    get_php_version,ldap_bool,encrypt_password
from django.conf import settings
from ..forms.appsForm import InstallAppForm, UpdateAppForm
from ..models.appsGroups import App
from ..models.modules import ModuleTop
from ..models.cpanelconf import Buckets, BasicCpanelTree

class Services(FormView):
    template_name = 'ldapobj/pages/services.html'
    form_class    = UpdateAppForm
    ldap          = {}
    ldap_attributes = ['status']
    dep_status    = {}

    def get_success_url(self):
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):

        context = super(Services, self).get_context_data(**kwargs)
        # For dependencies
        context['not_disabled'] = self.not_to_disable
        context['show_modal']   = 'form' in kwargs
        context['enabled']      = self.enabled_services
        context['disabled']     = self.disabled_services
        context['all_services'] = self.all_services
        # Apss that can be removed
        context['removable_servs'] = self.removable_services
        if self.puppet_status['puppetstatus'] == 'error' or self.puppet_status['puppetstatus'] == 'pending':
            context['maintenance']= True

        return context

    def get(self, request):
        self.not_to_disable = ['mail', 'nodejs']
        # Since release 202302 Owncloud can not be reactivated beacuse it odes not suppor php 8.1
        php_version       = get_php_version()
        if php_version == '7.4':
            self.not_to_enable = settings.NOT_7_4
        else:
            self.not_to_enable =settings.NOT_8_1

        self.all_services  = []
        self.removable_services = []
        self.used_dependencies = []
        self.puppet_status = get_puppet_status(request)
        try:
            self.enabled_services = self.request.enabled_services
    	    # Exclude installed services from available services
    	    # self.services_available = [ service for service in all_services if service['id'] not in installed_services ]
            disabled_services = App.objects.all().filter(status="disabled",typename="installed")
            self.disabled_services = [service.ou for service in disabled_services]
            self.confs = request.release.get('configurations')
            if self.confs: 
                for s in self.confs:
                    if s['settings']['id'] in self.enabled_services or s['settings']['id'] in self.disabled_services:
                        self.all_services.append(s)
                    # Applications can only be removed while their deendency are enabled.
                    # Eg. to remove discourse we need to launch docker commands
                    if s['settings']['remove']:
                        if len(s['dependencies']) == 0:
                            removable = True
                        else:
                            for dep in s['dependencies']:
                                removable=False
                                if dep['id'] and dep['id'] in self.enabled_services:
                                    removable = True
                                    break
                        if removable:        
                            self.removable_services.append(s['settings']['id'])
                    # Can not disable a dependency if needed by an app tht is either installed or deactivated
                    for dep in s['dependencies']:
                        if dep['id'] and dep['id'] not in self.not_to_disable and (
                            s['settings']['id']  in self.enabled_services or
                            s['settings']['id']  in self.disabled_services):
                                self.not_to_disable.append(dep['id'])
        except Exception as e:
            # Maybe mark a difference between no available aplicaction due to 'All of them have been installed' and a 
            # problem retreiving available application from ldap or whatever error
            # Now both cases are the same and sets context['no_updates']  = True
            p("Services view", "✕ There was a problem retrieving enabled services", e)
        
        return super(Services, self).get(request)

    def get_form(self):
        return self.form_class(
            enabled_services  = self.enabled_services,
            disabled_services = self.disabled_services,
            all_services      = self.all_services, 
            removable_servs   = self.removable_services,
            not_to_disable    = self.not_to_disable,
            not_to_enable     = self.not_to_enable,
            php_version       = get_php_version(),
            **self.get_form_kwargs()
        )

    def post(self, request, *args, **kwargs):
        
        data    = self.get(request)
        form    = self.get_form()
        context = self.get_context_data(**kwargs)
    
        remove_services    = []
        disable_services   = []
        enable_services    = []
        enable_group_deps  = []
        disable_group_deps = []
        deps_needed        = []
        
        if form.is_valid():

            for service in self.all_services:
                app_name   = service['settings']['id']

                # Applications to be removed
                field_name = "remove-%s" % app_name
                value      = form[field_name].value() if field_name in form.fields else None
                # Only disabled services can be removed
                if app_name in self.disabled_services and value:
                    #TODO: if a dependency eg: docker is to be removed,
                    # there must be no app  with this dependency in a status different than  
                    #status: disabled
                    #type: available
                    # So we can not remove docker if discourse is installed or deactivated.
                    # First you must remove dicpusre and only office and then docker ca be removed
                    # maybe this check should be done in form and avoid showing the remove checkbox
                    # for this cases
                    remove_services.append( app_name )
                
                # Applications to be disabled
                field_name = "disable-%s" % app_name 
                value      = form[field_name].value() if field_name in form.fields else None
                if app_name in self.enabled_services and value:
                    disable_services.append( app_name ) 
                    # If app_name has a dependency check if it can be
                    # disabled or if is in use by another app
                    if service['dependencies']:   
                        field_name = "dependency-%s" % app_name
                        value      = form[field_name].value() if field_name in form.fields else None
                        if value and not value in context['not_disabled']:
                            disable_group_deps.append( value )
                # Applications to be enabled                
                field_name = "enable-%s" % app_name
                value      = form[field_name].value() if field_name in form.fields else None
                
                if app_name in self.disabled_services and value:
                    enable_services.append( app_name )
                    if service['dependencies']:
                        field_name = "dependency-%s" % app_name
                        value      = form[field_name].value() if field_name in form.fields else None               
                        if value and (value not in enable_group_deps or value not in enable_services):
                            enable_group_deps.append(value)
            # Check if candidates dependencies can be disabled or if are used from other apps
            # Need to check this after post, after groups are checked and actions are defined
            for service in self.all_services:
                for dependency in service['dependencies']:
                    if dependency['id'] in self.enabled_services:
                        deps_needed.append( dependency['id'] )

            # Used in modal confirmation to list apps to be enabled/disabled
            # Remove items from disable_group_deps if are already in disable_services
            # TODO: If a service with dependency A is to be disabled and at the same tine
            # another service with same dependency is to be 
            # enabled, do not disable dependency A.
            # For ex. if a dependency is marked to collect all emabled and disabled dependecies.
            # For any match, remove it from disable an leave it in enable 

            clean_disable_dep  = [ 
                item for item in disable_group_deps 
                if  item not in disable_services 
                and item not in enable_group_deps
            ]
            clean_enable_dep  = [ 
                item for item in enable_group_deps 
                if  item not in enable_services 
                and item not in self.enabled_services
            ]
            context['disable_groups']     = disable_services
            context['disable_dep_groups'] = clean_disable_dep
            context['enable_groups']      = enable_services
            context['enable_dep_groups']  = clean_enable_dep

            # Check if trying to disable a required app
            for service in disable_services:
                if service in enable_group_deps or (service in deps_needed and service not in disable_group_deps):
                    messages.error(
                        self.request, _( 
                           'La aplicación %s no se puede desactivar por que es '
                           'necesaria para otras aplicaciones seleccionadas o activadas' 
                           % service
                        )
                    )
                    return self.render_to_response(context)
                    
            # Check if tryin to remove a dependency
            for service in remove_services:
                if service in self.not_to_disable:
                    messages.error(
                        self.request, _(  
                           'La aplicación %s no se puede eliminar de forma permanente por que es '
                           'necesaria para otras aplicaciones' 
                           % service
                        )
                    )
                    return self.render_to_response(context)

                if 'discourse' in remove_services:
                    disk_usage     = psutil.disk_usage('/')
                    print("disk free ", disk_usage.free)
                    if disk_usage.free <= 5242880000: 
                        messages.error(
                            self.request, _(  
                               'Para poder desinstalar Discourse se necesitan al menos 5GB de espacio libre.'
                            )
                        )

                        return self.render_to_response(context)

            if self.confs: 
                context['remove_groups']=[]
                for s in self.confs:  
                    data = {}
                    if s['settings']['id'] in remove_services:
                        context['remove_groups'].append(s['settings']['name'])
            context['show_modal'] = True
            
            # Check if app in dep_groups is in use
            if 'writeldap' in request.POST:
                
                services_enable  = enable_services + clean_enable_dep
                services_disable = disable_services

                try:
                    # update services to be disabled
                    if services_disable:
                        for service in services_disable:
                            dn = 'ou=%s,%s' % (service, settings.LDAP_TREE_SERVICES)
                            request.ldap.modify(dn, {
                                'type'   : [(MODIFY_REPLACE, 'installed')],
                                'status' : [(MODIFY_REPLACE, 'disabled')],
                            })

                    # update services to be enabled
                    if services_enable:
                        for service in services_enable:
                            dn = 'ou=%s,%s' % (service, settings.LDAP_TREE_SERVICES)
                            request.ldap.modify(dn, {
                                'type'   : [(MODIFY_REPLACE, 'available')],
                                'status' : [(MODIFY_REPLACE, 'enabled')],
                            })
                    # update services to be removed
                    if remove_services:
                        for service in remove_services:
                            dn = 'ou=%s,%s' % (service, settings.LDAP_TREE_SERVICES)
                            request.ldap.modify(dn, {
                                'status' : [(MODIFY_REPLACE, 'remove')],
                            })

                            
                    # lock cpanel
                    lock_cpanel()

                    #messages.success(self.request, _('Aplicaciones modificadas con éxito'))
                    return HttpResponseRedirect( reverse('logout') )

                except Exception as e:
                    messages.error(self.request, _(
                        'Hubo algún problema modificando el estado de tus aplicaciones. '
                        'Contacta con los administrador-s si el problema persiste'
                    ))
                    print(e)
                    context['show_modal'] = False
                    return self.render_to_response(context)

        else:
            context['show_modal'] = False 
        #return self.form_invalid(form, **kwargs)
        return self.render_to_response(context)

class AvailableAppsview(FormView):
    template_name = 'ldapobj/pages/appsinstall.html'
    form_class    = InstallAppForm
    ldap          = {}
    ldap_attributes = ['status']
    dep_status    = {}
    inputdeps     = []
    #maintenance   = False

    def get_success_url(self):
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):

        context = super(AvailableAppsview, self).get_context_data(**kwargs)
        context['status'] = self.puppet_status
        context['release'] = self.release_info
        context['available'] = self.services_available
        context['all_installed'] = self.allinstalled
        context['no_updates'] = self.no_updates
        context['show_modal'] = 'form' in kwargs
        context['groups'] = 'form' in kwargs	
        context['dep_groups'] = 'form' in kwargs
        return context

    def get(self, request):
        #TODO: merge both requests
        self.release_info  = request.release
        self.puppet_status = get_puppet_status(request)
        all_services = []
        self.allinstalled = False
        self.no_updates = False
        self.installed_services = []
        # From now we offer mailtrain 2. Mailtrain1 is till an available app only for disable/desintall
        not_to_install = settings.NOT_TO_INSTALL

        try:
            all_services = self.release_info['configurations']
            all_apps = App.objects.all().filter(Q(status="disabled",typename="installed") | Q(status="enabled",typename="available"))
            self.installed_services= [ service.ou for service in all_apps ]
            """
            request.ldap.search(
                settings.LDAP_TREE_BASE,
                settings.LDAP_FILTERS_INSTALLED_SERVICES_ALL,
                attributes=['ou']
                )
            installed_services = [ service.ou.value for service in request.ldap.entries ]
            print("installed_services", installed_services)
            """
            # Exclude installed services from available services
            # add owncloud to not allowed apps - we do not wantr this app to be installed anymore
            not_installable_services=list(set(self.installed_services + not_to_install))
            self.services_available = [ service for service in all_services if service['settings']['id'] not in not_installable_services]

            if not self.services_available:
                self.allinstalled = True
        except Exception as e:
            # Maybe mark a difference between no available aplicaction due to 'All of them have been installed' and a 
            # problem retreiving available application from ldap or whatever error
            # Now both cases are the same and sets context['no_updates']  = True
            self.no_updates = True
            p("ServicesAvailable view", "✕ There was a problem retrieving enabled services", e)
            if self.puppet_status['puppetstatus'] == 'error' or self.puppet_status['puppetstatus'] == 'pending':
                self.maintenance= True
                
        return super(AvailableAppsview, self).get(request)

    def get_form(self):
        return InstallAppForm(
            request=self.request,
            services_available=self.services_available, 
            installed_services=self.installed_services,
            **self.get_form_kwargs()
        )    

    def post(self, request, *args, **kwargs):
        data    = self.get(request)
        form = self.get_form()
        context = self.get_context_data(**kwargs)
        buckets = BasicCpanelTree.objects.get_or_create(ou="buckets") 

        # if an app has nginx as depednecy, we need create ldap entry
        nginx_dn = []
        # an empty list to store all gorups to be installed
        services = []
        group_deps = []
        # an empty dict to store user input dependencies
        dependencies = {}
        dependencies["app"] = []
        ObjectClasses = ['organizationalUnit', 'metaInfo']
        if form.is_valid():
            # create or update buckets data in ldap 
            for app in settings.S3_APPS:
                s3_name = "s3_{}_enabled".format(app)
                if self.puppet_status[s3_name]:
                    status = self.puppet_status["s3_{}_bucket".format(app)] 
                    location = self.puppet_status["s3_{}_location".format(app)]
                    b, created = Buckets.objects.update_or_create(
                            ou=app,
                            defaults={
                                "status":status,
                                "typename":location
                                }
                            )
                
            for service in self.services_available:
                appname = service['settings']['id']
                # First get checked application
                if form.cleaned_data.get(appname) and form[appname].value():
                    # Add app name to apps to be installed in ldap
                    services.append(appname)
                    nginx = False    
                    for app in service['dependencies']:
                        if app['id'] == 'nginx':
                            nginx = True
                        fieldname = "dependency-%s" % appname
                        if form.cleaned_data.get(fieldname) and form[fieldname].value() and app['id'] not in self.installed_services:
                            group_deps.append( app['id'] )
                    for field in service['fieldlist']:
                        fieldname  = 'input-%s-%s' % ( appname, field['fid'] )
                        if form.cleaned_data.get(fieldname) and form[fieldname].value():
                            if type(form[fieldname].value()) ==  bool:
                                val = ldap_bool(bool)
                            else:
                                val = form[fieldname].value().strip()
                            args = {
                                'name'   : field['fid'],
                                'service': appname,
                                'editable': field['editable'],
                                'value'  : val,
                            }
                            dependencies["app"].append(args)
                        
                        # Create array with additional DN required to create nginx vhosts
                        # this is a ou=webserver entry for avery fqdn field 
                        if field['widget'] == 'fqdn' and nginx:
                            n_dn= "ou=webserver,ou=%s,ou=%s,%s" % (field['fid'],  appname, settings.LDAP_TREE_SERVICES)
                            nginx_dn.append(n_dn)

            
            context['show_modal'] = True
            context['groups']     = services
            context['dep_groups'] = group_deps
            if 'writeldap' in request.POST:
                services += group_deps
                for service in services:
                    DN = "ou=%s,%s" % ( service, settings.LDAP_TREE_SERVICES )
                    if settings.DEBUG_INSTALL:
                        print("Installing service '%s' with DN '%s'" % (service, DN))
                    else:
                        generic_update_or_create_ldap_entry(
                            request.ldap,
                            DN,
                            ObjectClasses,
                            { 
                                'status' : [ (MODIFY_REPLACE, 'install') ] 
                            },
                            "✕ service not found ",
                            {
                                'ou'     : service,
                                'status' : 'install',
                                'type'   : 'available',
                            },
                            "✕ could not add service ",
                        )
                if nginx_dn:
                    for dn in nginx_dn:
                        generic_update_or_create_ldap_entry(
                            request.ldap, dn, ObjectClasses,
                            {
                                'type' : "nginx"
                            },
                            "✕ entry for service deps does not exists",
                            {
                                'ou'     : "webserver",
                                'type'   : "nginx" 
                            },
                            "✕ Could not create dependency tree",
                        )

                # Add dependencies
                # This are user input dependencies, that are inserted as subtree of the app they belong
                for dependency in dependencies['app']:
                    #crypt ediatable passwords fields or snappymail password
                    if (dependency["name"]=='password' and dependency["service"] == "snappymail" and  dependency["editable"])\
                        or (dependency["name"]=='password' and not dependency["editable"]):
                        try:
                            dependency["value"] = encrypt_password(dependency["value"])
                        except Exception as e:
                            print("Errro  encrypting pass", e)

                    create_dep = False
                    DN = 'ou=%s,ou=%s,%s' % ( 
                        dependency["name"], 
                        dependency["service"], 
                        settings.LDAP_TREE_SERVICES
                    )
                    if settings.DEBUG_INSTALL:
                        print("Installing dep '%s' with DN '%s'" % ( dependency, DN ))
                        print( dependency['value'] )
                    else:
                        generic_update_or_create_ldap_entry(
                            request.ldap, DN, ObjectClasses, 
                            {
                                'status' : [ ( MODIFY_REPLACE, dependency["value"] ) ]
                            },
                            "✕ entry for service deps does not exists",
                            {
                                'ou'     : dependency["name"],
                                'status' : dependency["value"],
                            },
                            "✕ Could not create dependency tree",
                        )
                    
                if nginx_dn:
                    for dn in nginx_dn:
                        generic_update_or_create_ldap_entry(
                        request.ldap, dn, ObjectClasses,
                            {
                                'type' : "nginx"
                            },
                            "✕ entry for service deps does not exists",
                            {
                                'ou'     : "webserver",
                                'type'   : "nginx" 
                            },
                            "✕ Could not create dependency tree",
                        )
                #lock opendkim module to create dkim for app domain
                ModuleTop.objects.update_or_create(
                    ou="opendkim", defaults={"status": "locked"}
                )

                lock_cpanel()
                # messages.success(self.request, _('Aplicaciones instaladas con éxito'))
                return HttpResponseRedirect( reverse('logout'))

        else:
            context['show_modal'] = False 
        #return self.form_invalid(form, **kwargs)
        return self.render_to_response(context)
