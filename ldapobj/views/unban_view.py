# django

from django.utils.translation import gettext_lazy as _
from django.shortcuts import render
from django.views.generic.edit import FormView
from django.urls import reverse
from django.contrib import messages

# project
from ..forms.unbanForm import UnbanForm 
from ..models.modules import Module
from ..mixins import AdminUserPassesTestMixin

class UnbanView(AdminUserPassesTestMixin, FormView):

    template_name = 'ldapobj/pages/unban.html'
    form_class    = UnbanForm

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        unban_ip = form['unban_ip'].value().strip()
        unban_user = form['unban_user'].value().strip()
        try:
            if unban_user:
                tally, created = Module.objects.update_or_create(
                ou="tally", defaults={"status": "locked", "typename": unban_user}
                )
                if tally:
                    print("Updated obj ", tally)
                else:
                    print("created on", created)
            if unban_ip:
                Module.objects.update_or_create(
                ou="fail2ban", defaults={"status": "locked", "typename": unban_ip}
                )
        except Exception as e:
            messages.error(self.request, _('Se ha producido un error' ))
        messages.success(self.request, _('Cambios guardados con éxito. El proceso de desbloqueo tardará un par de minutos' ))
        return super(UnbanView, self).form_valid(form)
