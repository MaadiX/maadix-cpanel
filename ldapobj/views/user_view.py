from django.utils.translation import gettext_lazy as _
from django.views.generic.edit import FormView
from django.views.generic import  UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.contrib import messages
from django.contrib.auth import logout as auth_logout
from django.http import HttpResponseRedirect
from django.contrib.auth.views import LogoutView as logout
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
# Check if we can remove lda3
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA
from ldap3.utils.hashed import hashed
from django_tables2 import SingleTableView

# project
from django.conf import settings
from apps.views  import utils
from ..tables import AccountTable, PostmasterTable
from ..models.user import LdapUser as User, AdminUser, SuperUser
from ..models.domain import Postmaster
from ..models.mail import LdapMail
from ..forms.userForm import CreateUserForm, EditUserForm, PostmasterForm, AdminUserForm, EditSuperUserForm
from ..forms.MailForm import EditMailForm
from .helpers_views import PassRequestToFormViewMixin
from ..views.helpers_views import BaseLdapModelsView
from ..mixins import AdminUserPassesTestMixin, EmailUserPassesTestMixin


def LoginRedirect(request):
    # Custom landing url to redirect users after login according to their roleº
    # With 2FA the LoginView is provided by two_factor
    user = request.user
    """ Redirection url if form is valid. Depends on user's role """
    role = user.role
    if role == 'postmaster':
        return HttpResponseRedirect(reverse('mails'))
    elif role == 'email':
        return HttpResponseRedirect(reverse('mxcpprofile'))
    else:
        return HttpResponseRedirect(reverse('system-details'))

class LogoutView(logout):
    """ Custom logout view that destroys session data """

    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        next_page = self.get_next_page()
        # clean session data
        response  = HttpResponseRedirect(next_page)
        # username = request.session['user']
        # del request.session['user']
        # del request.session['enc_pass']
        # Destroy cookies
        # response.delete_cookie('s')
        # redirect to settings.LOGOUT_REDIRECT_URL
        auth_logout(request)
        return response

class EditProfile(EmailUserPassesTestMixin,PassRequestToFormViewMixin, UpdateView):
    template_name = 'ldapobj/pages/profile.html'


    def get_object(self):
        user = self.request.user
        self.role = user.role
        if self.role == 'admin':
            obj = AdminUser.objects.get(pk=user.username)
        elif self.role == "postmaster":
            obj = Postmaster.objects.get(mail=user.username)
        elif self.role == "email":
            obj = LdapMail.objects.get(pk=user.username)
        return obj

    def get_context_data(self, **kwargs):
        context = super(EditProfile, self).get_context_data(**kwargs)
        context['username'] = self.request.user.username
        return context

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        # logout if password was updated
        return self.request.get_full_path()
 
    def get_form_class(self):
        """ Return an instance of the form to be used in this view."""
        if self.role == 'admin':
            return AdminUserForm
        elif self.role == "postmaster":
            return PostmasterForm
        elif self.role == "email":
            return EditMailForm


    def form_valid(self, form):
        pwd      = ''
        user = self.get_object()
        old_pass = user.password
        obj = form.instance
        # Need to check current password
        # set fields to be updated
        try:
            form.save()
            if 'password' in form.fields and form['password'].value():
                pwd = form['password'].value()
                obj.password =  hashed(HASHED_SALTED_SHA, pwd)
                obj.save()           
            else:
                obj.password = old_pass
            obj.save()
            messages.success(self.request, _('Has cambiado tu perfil con éxito.'))

        except Exception as e:    
                messages.error(self.request, _('Se ha producido un error ' ))
                utils.p("✕ view_users.py", "There's a problem updating profile: ", e)
        return HttpResponseRedirect(self.get_success_url())

class ListPostmastersView(AdminUserPassesTestMixin, SingleTableView, BaseLdapModelsView):
    model = Postmaster 
    table_class = PostmasterTable
    template_name = 'ldapobj/pages/postmasters_list.html' 

    def get_queryset(self):
        users = Postmaster.objects.all().filter(cn='postmaster')
        query = self.request.GET.get("q")
        if query:
            users = Postmaster.objects.filter(
	    Q(mauldrop__icontains=query) |
            Q(mail__icontains=query) 
	  )
        return users 

class ListUsersView(AdminUserPassesTestMixin, SingleTableView, BaseLdapModelsView):
    model = User
    table_class = AccountTable
    template_name = 'ldapobj/pages/users_list.html'

    def get_queryset(self):
        users = User.objects.all().exclude(group=27)
        query = self.request.GET.get("q")
        if query:
            users = User.objects.filter(
        Q(username__icontains=query) |
            Q(email__icontains=query) |
            Q(authorized_service__contains=query)
      )
        return users

class AddUserView(AdminUserPassesTestMixin, PassRequestToFormViewMixin, FormView):
    form_class= CreateUserForm
    template_name = 'ldapobj/pages/add_user.html' 
    success_url = reverse_lazy('users')

    def form_valid(self, form):
        new_user = form.save(commit=False)
        # Get authorized services
        authorized_service= []
        ssh_keys = []
        for service in ['openvpn', 'apache', 'jitsi']:
            if service in form.fields and form[service].value():
                authorized_service.append(service)

        # Get next uid number
        try:
            self.request.ldap.search(
                search_base   = settings.LDAP_TREE_BASE,
                search_filter = "(&(objectClass=uidNext)(uidnumber=*))",
                attributes    = ['uidnumber']
            )
            next_uid = int(self.request.ldap.entries[0].uidnumber.value) + 1
            gidnumber = next_uid
        except Exception as e:
            messages.error(self.request, _('Se ha producido un error creando la cuenta %s' % new_user.username))
        if next_uid:
            # Update uidnumber fro next user creation
            try:
                dn = 'cn=uidNext,ou=sshd,ou=People,dc=example,dc=tld'
                self.request.ldap.modify(dn, { 'uidnumber' : [(MODIFY_REPLACE, next_uid)] })
            except Exception as e:
                utils.p("✕ view_users.py", "There was a problem updating next uid value: ", e)
            # Set the correct values dependng on ssh or sftp
            if form['ssh_key_input'].value():
                ssh_keys.append(form['ssh_key_input'].value())
            if form['sshSftp'].value() and not form['sshSftp'].value()=="none":
                if form['sshSftp'].value()=='ssh':
                    home_path       = "/home"
                    jailed_user     = False
                    self.usertype   = 'ssh'
                elif form['sshSftp'].value()=='sftp':
                    home_path       = "/home/sftpusers"
                    self.jailed_user= True
                    self.usertype   = 'sftp'
                    gidnumber       =int(settings.SFTP_GUID)
                authorized_service.append('sshd')
                home_directory = "%s/%s" % (home_path, new_user.username)
            else:
                gidnumber     = next_uid
                home_directory= 'none'

            # Assign additional value to objectbefore saving
            new_user.first_name = form['first_name'].value() if form['first_name'].value()  else new_user.username
            new_user.last_name = form['last_name'].value() if form['last_name'].value()  else new_user.username
            new_user.uid= next_uid
            new_user.gecos = '%s,,,' % new_user.username
            new_user.group = gidnumber
            new_user.home_directory = home_directory
            new_user.authorized_service = authorized_service
            if ssh_keys:
                new_user.ssh_key = ssh_keys
            new_user.password   =  hashed(HASHED_SALTED_SHA, form['password'].value())
            try:
                new_user.save()
                if 'openvpn' in self.request.enabled_services and form['instructions'].value():
                    utils.send_vpn_instructions(self.request, new_user.email, new_user.username)
                ### Add the user to the web group, if ssh is enabled, for sftp users
                # this will make the files uploaded by these users being owned by useename:web
                # web is a group which includes www-data. So apache will be able to access folders

                messages.success(self.request, _('Cuenta añadida con éxito'))
                utils.p("✔ view_users.py", "Usuarix %s añadid- con éxito':" % new_user.username, '')
            except Exception as e:
                messages.error(self.request, _('Se ha producido un error creando la cuenta %s' % new_user.username))
                utils.p("✕ view_users.py", "There was a problem creating the user %s :" % new_user.username, e)

        return super().form_valid(form)

class EditUserView(AdminUserPassesTestMixin, PassRequestToFormViewMixin, SuccessMessageMixin,  UpdateView):
    model = User
    form_class= EditUserForm
    template_name = 'ldapobj/pages/edit_user.html'
    success_message =  _('Cuenta modificada con éxito')

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):
        context = super(EditUserView, self).get_context_data(**kwargs)
        context['userobj'] = self.get_object()
        if 'sshd' in context['userobj'].authorized_service:
            context['type'] = 'SFTP' if context['userobj'].group == int(settings.SFTP_GUID) else 'SSH/SFTP'
        return context

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        #TODO:  Do not allow to edit superuser here 
        user =  self.get_object()
        old_pass = user.password
        services = form['authorized_service'].value()
        # avaiable keys are listed in form with a disabled checkbox
        # Enabling the checkbox means deleting the key
        user_keys = user.ssh_key
        try:

            form.save()
            user=form.instance
            # Case in which ssh/sftp is added to existing user
            #if form['sshSftp'].value() and not form['sshSftp'].value()=="none":
            if 'sshd' not in user.authorized_service and not form['sshSftp'].value()=="none":
                if form['sshSftp'].value()=='ssh':
                    home_path     = "/home"
                elif form['sshSftp'].value()=='sftp':
                    home_path     = "/home/sftpusers"
                    user.group = settings.SFTP_GUID
                services.append('sshd')
                user.home_directory = "%s/%s" % (home_path, user.username)
                # Only update password if it has been changed
            user.authorized_service=services
            if form['ssh_key_input'].value():
                user_keys.append(form['ssh_key_input'].value())
            if form['password'].value():
                pwd = form['password'].value()
                user.password = hashed(HASHED_SALTED_SHA, pwd)
            else:
                user.password = old_pass
            user.ssh_key = user_keys
            user.save()

        except Exception as e:
            messages.error(self.request, _('Se ha producido un error'))
            print(e)
        if 'openvpn' in services:
            try:
                if form['instructions'].value():
                    utils.send_vpn_instructions(self.request, user.email, user.username)
                    #messages.success(self.request, _('Instrucciones enviadas con éxito'))
            except Exception as e:
                 messages.error(self.request, _('Se ha producido un fallo en el envío de las instrucciones para la conexión VPN'))
        super().form_valid(form)
        return HttpResponseRedirect(self.get_success_url())

class EditSuperUser(SuccessMessageMixin,AdminUserPassesTestMixin, PassRequestToFormViewMixin, UpdateView):
    model = SuperUser
    form_class= EditSuperUserForm
    template_name = 'ldapobj/pages/edit_user.html'
    success_message =  _('Cuenta modificada con éxito')

    def get_object(self):
        obj = SuperUser.objects.get(group=27)
        return obj

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()


    def get_context_data(self, **kwargs):
        #context = super(EditSuperUser, self).get_context_data(**kwargs)
        context = super().get_context_data(**kwargs)
        context['userobj'] = self.get_object()
        context['type'] = 'SSH'
        return context

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        user =  self.get_object()
        old_pass = user.password

        try:
            user =  form.instance
            services = form['authorized_service'].value()
            user_keys = user.ssh_key
            # Preserver default permissions
            services.extend(["sshd", "cron", "sudo"])
            if form['password'].value():
                pwd = form['password'].value()
                user.password = hashed(HASHED_SALTED_SHA, pwd)
            else:
                user.password = old_pass
            user.authorized_service=services
            if form['ssh_key_input'].value():
                user_keys.append(form['ssh_key_input'].value())
            user.ssh_key = user_keys
            user.save()
        except Exception as e:
            messages.error(self.request, _('Se ha producido un error '))
            print("Error: ", e)

        if 'openvpn' in services:
            try:
                if form['instructions'].value():
                    utils.send_vpn_instructions(self.request, user.email, user.username)
            except Exception as e:
                 messages.error(self.request, _('Se ha producido un fallo en el envío de las instrucciones para la conexión VPN'))
        super().form_valid(form)
        return HttpResponseRedirect(self.get_success_url())

class EditPostmaster(AdminUserPassesTestMixin, UpdateView):
    model = Postmaster
    form_class= PostmasterForm 
    template_name = 'ldapobj/pages/postmaster.html'

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_object(self, *args, **kwargs):
        user_mail = self.kwargs.get('pk')
        obj = Postmaster.objects.get(mail=user_mail)
        return obj

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        user =  self.get_object() 

        try:
            if form['password'].value():
                user.password = hashed(HASHED_SALTED_SHA, form['password'].value())
                user.save()
            messages.success(self.request, _('Cuenta modificada con éxito'))

        except Exception as e:
            messages.error(self.request, _('Se ha producido un error'))
            print(e)
        return HttpResponseRedirect(self.get_success_url())
