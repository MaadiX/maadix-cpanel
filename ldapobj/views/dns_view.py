# django
from django.views.generic.detail import DetailView
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
# project
from .. import dns_utils
from .. import utils
from ..models.domain import LdapDomain as Domain
from django.conf import settings
from ..mixins import AdminUserPassesTestMixin

class DnsView(AdminUserPassesTestMixin, DetailView):
    """
    Domains list view.
    """


    def get(self, request, **kwargs):
        # Domain general info
        ip             = utils.get_server_ip()
        host           = utils.get_server_host()
        #domain         = request.GET.get('') if request.GET.get('domain') else host
        domain          = self.kwargs.get('domain') if self.kwargs.get('domain') else host
        edit_url       = reverse('edit-ldapdomain', args=[domain])
        install_path   = settings.FORCE_SCRIPT_NAME
        # Domain DNS related info
        domain_records = dns_utils.get_dns_records(domain)
        is_mailman_domain = False
        mailman_domains = utils.get_mailman_domain_names()

        try:
            obj = Domain.objects.get(pk=domain)
            # Is email server activated?
            mail_active = obj.accountActive
        except Exception as e:
            # if querying a wrong domain redirect to domains overview
            if not domain in mailman_domains and not domain == host:
                messages.error(self.request, _('No tienes este dominio creado'))
            elif  domain in mailman_domains:
                is_mailman_domain = True
                mail_active = True
                print("IS MAILMAN DOMAIN ", is_mailman_domain)

        if not is_mailman_domain and not domain==host and not obj.dkim:
            editurl = reverse('edit-ldapdomain', args=[domain]) 
        # Render summary table
        dns = dns_utils.get_all_dns(domain)
        mx_ok={"type":"MX", "name":domain , "Requerido": host + '.' , "Priority": 10}
        # get details about each record

        return render(request, 'ldapobj/pages/dns.html', locals())
