from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from django.views.generic import UpdateView
from django.http import HttpResponseRedirect
from django.contrib import messages
from ..mixins import AdminUserPassesTestMixin
from .helpers_views import PassRequestToFormViewMixin
from ..forms.appsForm import EditAppForm
from ..models.appsGroups import App
from ..models.cpanelconf import Cpanel
from ..models.modules import ModuleTop
from ..utils import ldap_bool, encrypt_password, get_release_info, get_puppet_status, get_server_host
from ..dns_utils import get_local_dkim_key
from django.conf import settings


class AppSettings(AdminUserPassesTestMixin, PassRequestToFormViewMixin, UpdateView):
    form_class = EditAppForm
    model = App
    template_name   = 'ldapobj/pages/apps.html'
 
    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        
        context = super(AppSettings, self).get_context_data(**kwargs)
        self.obj = self.get_object()
        self.application_id   = self.obj.ou
        self.domain = self.obj.get_dependency('domain')
        if self.domain:
            self.dkim = get_local_dkim_key(self.domain.status)
        else:
            self.dkim = None
        release = get_release_info(self.request)
        self.application_name = [
            service['settings']['name'] for service in release['configurations']
            if service['settings']['id'] == self.application_id
        ][0]
        puppet_status = get_puppet_status(self.request)
        if puppet_status['puppetstatus'] == 'error' or puppet_status['puppetstatus'] == 'pending':
            self.maintenance = True

        self.maintenance = False
        form = self.get_form()
        context['appname']      = self.application_name 
        context['maintenance']  = self.maintenance
        context['show_modal']   = form.is_valid
        context['update_group'] = 'form' in kwargs
        context['dkim'] = self.dkim 
        context['domain'] = self.domain.status if self.domain else None
        context['fqdn'] = get_server_host
        return context
 
    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_form(self):
        """
        Return an instance of the form to be used in this view,
        Populate dynamic dependencies form fields with the values from ldap
        """
        self.obj = self.get_object()
        release = get_release_info(self.request)
        for service in release['configurations']:
            if service['settings']['id'] == self.obj.ou:
                self.fields = [
                  field for field in service['fieldlist']
                  if field['editable']
                ]
        return EditAppForm(
            fieldlist      = self.fields,
            **self.get_form_kwargs()
        )

    def form_valid(self, form):

        obj= form.instance
        context     = self.get_context_data()
        form        = self.get_form()
        context['update_group'] = True
        context['show_modal'] = True
        
        if 'writeldap' in self.request.POST: 
            try:
                #form.save()
                for field in self.fields:
                
                    fid       = field['fid']
                    if fid in form.changed_data:
                        if field['widget'] == 'boolean':
                            value = ldap_bool(form[fid].value())
                        else:
                            value = form[fid].value().strip() 
                        if fid == 'password' and self.application_id == 'snappymail':
                            value = encrypt_password(value)

                        try:
                            old_value = obj.get_dependency(fid)
                            old_value = old_value.status
                        except:
                            old_value = None
                        base_dn= "ou=%s,%s" % (obj.ou,settings.LDAP_TREE_SERVICES )
                        dn="ou=%s,%s" % (fid, base_dn)
                        obj.set_dependency(fid,value)
                        if (fid == 'domain' or fid == 'sandbox' or fid == 'public' or fid == 'proxy' ) and old_value:
                            dep_name = "%s_old" % fid
                            obj.set_dependency(dep_name,old_value)
                    #lock opendkim module to create dkim for app domain
                    dk, created = ModuleTop.objects.get_or_create(
                    ou="opendkim",
                        defaults={ "status", "locked"}
                    )
                    dk.lock()
                    cp = Cpanel.objects.get(pk="cpanel")
                    cp.lock()
                return HttpResponseRedirect( reverse('logout') )
            except Exception as e:
                context['show_modal'] = False
                messages.error(self.request, _('Se ha producido un error ' ))
                print("ERROR updating app", e)
        return self.render_to_response(context)
