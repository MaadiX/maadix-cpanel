from django.utils.translation import gettext_lazy as _
from django.views.generic.edit import FormView
from django.views.generic import UpdateView
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib import messages
# Check if we can remove lda3
from ldap3 import  HASHED_SALTED_SHA
from ldap3.utils.hashed import hashed
from django_tables2 import SingleTableView
from ..views.helpers_views import BaseLdapModelsView
# project
from django.conf import settings
#from apps.views  import utils
from ..tables import MailsTable
#from ..models.user import LdapUser as User
from ..models.mail import LdapMail as Mail
from .helpers_views import PassRequestToFormViewMixin
from ..forms.MailForm import AddMailForm, EditMailForm
from ..mixins import PostmasterUserPassesTestMixin,EmailUserPassesTestMixin
from ..models.domain import LdapDomain, Postmaster
from .. import utils 

class ListMailsView(PostmasterUserPassesTestMixin, SingleTableView, BaseLdapModelsView):
    model = Mail
    table_class = MailsTable
    template_name = 'ldapobj/pages/mail_list.html' 

    def get_queryset(self):
   
        user = self.request.user
        self.role = user.role
        query = self.request.GET.get("q")
        if user.role == 'admin':
            mails = Mail.objects.all()
            if query:
                mails= Mail.objects.filter(
                Q(mail__icontains=query)
            )
        elif user.role == "postmaster":
            domain = user.username.split("@")[1]
            parent_dn = "vd=%s,%s" % (domain, settings.LDAP_TREE_HOSTING)
            d_filter = '@{}'.format(domain)
            mails = Mail.objects.filter(mail__icontains=d_filter)
            if query:
                mails = Mail.objects.filter(mailbox__startswith=domain).filter(
                Q(mail__icontains=query) 
            )
        return mails

class AddMailView(PostmasterUserPassesTestMixin, PassRequestToFormViewMixin, FormView):
    form_class= AddMailForm
    template_name = 'ldapobj/pages/add_mail.html' 
    success_url = reverse_lazy('mails')

    def form_valid(self, form):
        error=""
        password   = hashed(HASHED_SALTED_SHA, form['password'].value())
        domain = form['domain'].value()
        email =  '%s@%s' % (form['name'].value(), domain) 
        try:
            new_mail = form.save(commit=False)
            new_mail.password=password
            new_mail.cn =  new_mail.givenName + ' ' + new_mail.sn
            new_mail.mailbox = str('%s/%s' % (form['domain'].value(),form['name'].value()))
            new_mail.mailautoreply = new_mail.mail
            new_mail.uid = '%s.%s' % (form['name'].value(), form['domain'].value())
            new_mail.quota = utils.quota_to_ldap(form['quota'].value()) 
            new_mail.save()
            utils.notify_email(new_mail.mail)
            messages.success(self.request, _('Cuenta de correo añadida con éxito'))
        except Exception as e:
            utils.p("MailAccounts view", "There was a problem creating email account", e)
            error = _('Ha habido problemas creando la cuenta de correo, prueba de nuevo.')
        if error:
            messages.error(self.request, error)   
        return super().form_valid(form)

class EditMailView(EmailUserPassesTestMixin, PassRequestToFormViewMixin, UpdateView):
    model = Mail
    form_class= EditMailForm
    #template_name = 'ldapobj/pages/edit_mail.html'
    template_name = 'ldapobj/pages/mail_account.html'

    def test_func(self):
        # Allow edition by role
        role = self.request.user.role
        obj = self.get_object()
        res = False
        if role == 'email':
            if obj.mail == self.request.user.username:
                res = True
        elif role == 'postmaster':
            postm_obj = Postmaster.objects.get(mail=self.request.user.username)
            if postm_obj.get_domain() == obj.domain:
                res = True
        elif role == 'admin':
            res = True
        return res

    def get_context_data(self, **kwargs):

        #domains_module = utils.get_cpanel_local_status(self.request.ldap, 'domains')
        obj = self.get_object()
        context = super(EditMailView, self).get_context_data(**kwargs)
        context['mail_account'] = obj.mail
        context['fqdn'] = utils.get_server_host()
        return context


    def get_form(self):
        obj = self.get_object()
        kwargs   = self.get_form_kwargs()
        kwargs['initial']['quota'] = float(obj.quota)/1024**3
        return EditMailForm(**kwargs)

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        # logout if password was updated
        return self.request.get_full_path()

    def form_valid(self, form):       
        # This fails because we are composing the RDN from the pk adding vd=domain.tld in the model
        # pk is the email account
        # The object dn is mail=EMAIL,vd=domain,LDAP_TREE_HOSTING
        # upon saving the rdn is built again so there are finally 2 vd=
        #  mail=EMAIL,vd=domain,vd=domain,LDAP_TREE_HOSTINlG
        # Bacuase build_dn is return "%s,%s" % (self.build_rdn(), self.base_dn)
        # and ldapdb/models/base.py calls build_dn again and compares it to old_dn 
        #form.save()

        user =  self.get_object()
        old_pass = user.password
        try: 
            obj = form.instance
            form.save()
            if 'password' in form.fields or 'quota' in form.changed_data:
                if 'password' in form.fields and form['password'].value():
                    pwd = form['password'].value()
                    obj.password =  hashed(HASHED_SALTED_SHA, pwd)
                else:
                    obj.password = old_pass
                if 'quota' in form.changed_data:
                    obj.quota = utils.quota_to_ldap(form['quota'].value())
                obj.save()
            messages.success(self.request, _('Cuenta actualizada con éxito.'))

        except Exception as e:    
                messages.error(self.request, _('Se ha producido un error ' ))
                utils.p("✕ mail_view.py", "There's a problem updating account: ", e)
        return HttpResponseRedirect(self.get_success_url())    


