import time
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.views.generic import FormView
from django.shortcuts import render
from django import views
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import logout as auth_logout
from ..mixins import AdminUserPassesTestMixin
from ..models.cpanelconf import CpanelBasicConf, CpanelAdvancedTopConf
from ..forms.changeFqdnForm import ChangeFqdnForm
from ..models.modules import Module
from django.conf import settings
from ..utils import get_server_ip, get_server_hostname, get_server_host, get_puppet_status,lock_cpanel,p

class ChangeFqdnView(AdminUserPassesTestMixin, FormView):
    form_class= ChangeFqdnForm
    template_name = "ldapobj/pages/fqdn.html"


    def get_object(self):
        obj = CpanelBasicConf.objects.get(pk="fqdn_domain")
        return obj

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context               = super(ChangeFqdnView, self).get_context_data(**kwargs)
        fqdn                  = get_server_host()
        context['fqdn']       = fqdn
        servername            = get_server_hostname()
        context['servername'] = servername
        # get the server domain : remove servername from fqdn
        # didn't find a python function equivalent to hostname -d 
        replacing_string = "%s." % servername
        server_domain = fqdn.replace(replacing_string, "", 1)
        context['server_domain'] = server_domain
        context['display_form'] = 'form' in kwargs
        context['ip'] = get_server_ip()
        puppet_status = get_puppet_status(self.request)
        if puppet_status['puppetstatus'] != 'ready':
            context['maintenance'] = True
        return context

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        errors=False
        context = self.get_context_data()
        new_domain = form['name'].value().strip()
        old_domain = context['server_domain']
        try:
            CpanelAdvancedTopConf.objects.update_or_create(
                ou="fqdn_domain_old", defaults={"status": old_domain}
                )
            CpanelBasicConf.objects.update_or_create(
                ou="fqdn_domain", defaults={"status": new_domain}
                )
            
            Module.objects.update_or_create(
                ou="customfqdn", defaults={"status": "locked"}
                )
        except RuntimeError as re:
            print("There was a problem updating ldap data")
            print(re)
            errors=True
        except Exception as e:
            errors=True
            messages.error(self.request, _('Se ha producido un error' ))

        if not errors:
            Module.objects.update_or_create(
                ou="customfqdn", defaults={"status": "locked"}
            )
            # time.sleep(t) because local cpanel must run befor puppet master
            time.sleep(1)
            # lock cpanel ad log out user
            lock_cpanel() 
            # Destroy cookies
            next_page = 'changesystem'
            response  = HttpResponseRedirect(reverse(next_page), locals)
            response.set_cookie('oldDomain', context['servername'] + '.' + context['server_domain'])
            response.set_cookie('newDomain', context['servername'] + '.' + new_domain)
            # redirect to settings.LOGOUT_REDIRECT_URL
            auth_logout(self.request)
            return response
        else: 
            return super(ChangeFqdnView, self).form_valid(form)

class NewFqdnSet(views.View):
    def get(self, request):
        # TODO: if user is logged in redirect to cpanel homepage (details)
        path = settings.FORCE_SCRIPT_NAME 
        return render(request, 'registration/fqdn-change.html', locals())