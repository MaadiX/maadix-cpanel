# django
from django.views.generic import UpdateView
from django.http import HttpResponseRedirect
from django.urls import  reverse
from django.utils.translation import gettext_lazy as _
# project
from .. import utils
from ..models.cpanelconf import Cpanel
from django.conf import settings
from ..mixins import AdminUserPassesTestMixin
from ..forms.updateForm import UpdateForm

class Update(AdminUserPassesTestMixin, UpdateView):
    """
    View to update the system.
    """
    model = Cpanel
    template_name = 'ldapobj/pages/update.html'
    form_class = UpdateForm

    def get_object(self):
        try:
            obj = Cpanel.objects.get(ou="cpanel")
            return obj
        except Cpanel.DoesNotExist:
            return None

    def get_release_data(self):
        """Handle GET requests."""
        obj = self.get_object()
        context = {}
        try:
            obj = self.get_object()
            cur_release = obj.typename
            context['current_release'] = cur_release.replace("_", " ")
        except Exception as e:
            context['current_release'] = None

        if settings.NO_API:
            puppet_status = debug.STATUS
        else:
            puppet_status = utils.get_puppet_status(self.request)
        context['status'] = puppet_status.get('puppetstatus')
        if context['status'] == 'pending' or context['status'] == 'error':
            context['status'] = context['status']
        elif context['status'] == 'ready':
            if settings.NO_API:
                release_info = debug.RELEASE
            else:
                release_info = utils.get_release_info(self.request)
                # TODO: check if there is a newer release , return the release info.
                if release_info:
                    context['update'] = release_info['update']
        else:
            context['status'] = 'no-updates'
        # Check which apps are to be updated, to calculate if there is enough free space
        # Actually only Discourse and Nextcloud
        if context.get('update'):
            apps_to_update = list(context['update']['updatedapps'])
            apps_installd = self.request.enabled_services
            checking = list(set(apps_to_update).intersection(apps_installd))
            free_stuff = utils.get_remaining_resources(self.request.release.get('configurations'),checking)
            if free_stuff['disk'] <=0:
                context['nospace'] = False 
        #return render(request, 'ldapobj/pages/update.html', context)
        return context
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        data = self.get_release_data()
        context.update(data)
        return context

    def get_initial(self):
        context = self.get_release_data() 
        """
        Returns the initial data to use for forms on this view.
        """
        initial = super().get_initial()
        release = context.get("update")
        # Set an empty value if 
        if release:
            initial['typename'] = release["name"]
        return initial

    def form_valid(self, form):
        form = self.get_form()
        context = self.get_context_data()
        release = form['typename'].value()
        if (release and release != 'pending'):
            form.save()
        utils.lock_cpanel()
        return HttpResponseRedirect( reverse('logout') )
