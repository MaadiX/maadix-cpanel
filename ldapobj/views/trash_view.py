import os, time
from re import I
from django.utils.translation import gettext_lazy as _
# Check if we can remove lda3
from ldap3 import  HASHED_SALTED_SHA
from ldap3.utils.hashed import hashed
from django_tables2 import SingleTableView
from django.contrib import messages
from django.http import  HttpResponseRedirect
#from apps.views  import utils
from ..tables import TrashDomainsTable, TrashUsersTable, TrashMailsTable 
from ..models.trash import MailTrash, Trash, DomainTrash, UserTrash, BackupsTrash, OnionTrash
from ..models.modules import Module
from ..mixins import AdminUserPassesTestMixin
from .. import utils 

class Trash(AdminUserPassesTestMixin, SingleTableView):
    model = DomainTrash
    table_class = TrashDomainsTable
    template_name = 'ldapobj/pages/trash.html' 

    def get_queryset(self):
        self.domains = DomainTrash.objects.all()
        return self.domains 
        
    def get_users(self):
        self.users = UserTrash.objects.all()
        return self.users

    def get_mails(self):
        self.mails = MailTrash.objects.all()
        return self.mails

    def get_backups(self, dir_path="/home/.trash/backups/"):
        # backups have different behaviour as they are not in ldap yet
        filesToList=[]
        if(os.path.isdir(dir_path)):
            listFiles = os.listdir(dir_path)
            filesToList = self.list_files_in_trash(dir_path,listFiles,model=BackupsTrash)
        return filesToList

    def get_onions(self, dir_path="/home/.trash/onions/"):
        # onions are like backups. they are not in ldap yet
        filesToList=[]
        if(os.path.isdir(dir_path)):
            listFiles = os.listdir(dir_path)
            filesToList = self.list_files_in_trash(dir_path,listFiles, model=OnionTrash)
        return filesToList

    def list_files_in_trash(self,dir_path,listFiles,model):
        filesToList = []
        for item in listFiles:
            fname = os.path.join(dir_path, item)
            if os.path.isfile(fname):
                fSize = os.path.getsize(fname)
            elif os.path.isdir(fname):
                fSize = utils.get_folder_size(fname)
            dataF= str(round(fSize / (1024 * 1024), 3)) + "MB"
            info = os.stat(fname)
            date = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(info.st_mtime))
            # If a file has been recently moved  to trash, it may still appear in the dir_path
            # When moving it to trash we record it's inode so we make sure we do not move to trash an unwanted item
            # By default, file is not in ldap. status:none
            # If we find it 
            try:
                obj = model.objects.get(l=info.st_ino, cn=item)
                status = obj.status
            except Exception as e:
                status=None
            data = {"name" : item, "size" :dataF , "description" : date, "status": status, "inode": info.st_ino}
            filesToList.append(data)
        return filesToList

    def get_context_data(self, **kwargs):
        context = super(Trash, self).get_context_data(**kwargs)
        self.trash = Module.objects.get(ou="trash")
        if (self.trash.status == 'error'):
            context['modal_text_body'] =_("Si continúas se reanudará la última operación de borrado que generó un error y se eliminarán de forma permanente las carpetas previamente seleccionadas. Esta acción no se puede deshacer.")
        else:
            context['modal_text_body'] =_("Si continúas se borrarán de forma permanente los elementos seleccionados. Esta acción no se puede deshacer.")
        context['modal_text_confirm'] =_("¿Confirmas que quieres continuar?")
        context['trash_status'] = self.trash.status
        # Additional tables
        context['table_users'] =  TrashUsersTable(UserTrash.objects.all(), prefix="users-")
        context['table_mails'] = TrashMailsTable(MailTrash.objects.all(), prefix="mails")
        context['domains']     = self.domains
        context['mails']       = self.get_mails()
        context['users']       = self.get_users()
        context['backups']     = self.get_backups()
        context['onions']     = self.get_onions()
    # TODO: Can not create a table from a dict. 
        # ldapdb trhow errors becuse make a query and expects an objetc
        # So we will create am  html table in the templates
        #context['table_backups']= BackupsTrash(context['backups'])
        return context
    def post(self, request, **kwargs):
        self.object_list = self.get_queryset()
        context = self.get_context_data(**kwargs)
        # Get all checked iputs
        trash = Module.objects.get(ou="trash")
        trash_status = trash.status
        print("tras status", trash_status)
        success = True
        try:
            # For each domain selected crete/update a domain trash object 
            # Do the same for users and mails
            domains = request.POST.getlist('domains[]')
            users = request.POST.getlist('users[]')
            mails = request.POST.getlist('mails[]')
            backups =  request.POST.getlist('backups[]')
            onions = request.POST.getlist('onions[]')
            docker = request.POST.get("docker")
            apt =  request.POST.get("apt")
            if (not domains and not  users and not  mails and not backups and not docker and not apt and not onions):
                messages.error(request,_("No has seleccionado ningun elemento."))
                success = False
            # Check again. User may have delayed the selection so we plrefer to re check the trash status
            if (trash_status and trash_status != 'ready' and trash_status != 'error'):
                 messages.error(request, _("Hay otra operación de borrado en curso. Espera unos minutos para que termine y poder eliminar de forma permanente otras carpetas."))
                 success = False
            for item in domains:
                it = DomainTrash.objects.get(cn=item)
                it.status='purge'
                it.save()
            for item in mails:
                it = MailTrash.objects.get(cn=item)
                it.status='purge'
                it.save()
            for item in users:
                it = UserTrash.objects.get(cn=item)
                it.status='purge'
                it.save()
            if docker:
                Module.objects.update_or_create(ou="cleandocker", defaults=dict(info="ready", status="locked"))                
            if apt:
                Module.objects.update_or_create(ou="cleanapt", defaults=dict(info="ready", status="locked")) 
            for item in backups:
                inode = request.POST.get('%s-inode' % item)
                now =  utils.ldap_creation_dateTime()
                BackupsTrash.objects.update_or_create(cn=item,l=inode, defaults=dict(typename=item,otherPath="home/backups", description=now , status="purge"))
            for item in onions:
                inode = request.POST.get('%s-inode' % item)
                now =  utils.ldap_creation_dateTime()
                OnionTrash.objects.update_or_create(cn=item,l=inode, defaults=dict(typename=item,otherPath="home/onions", description=now , status="purge"))            
            if success:
                trash.status = "locked"        
                trash.save()
        except Exception as e:
            messages.error(request, _('Se ha producido un error' ))
            utils.p("Trash view", "Error updating trashes entry in ldap", e)
            return self.render_to_response(context)
        if success:
            messages.success(self.request, _('Cambios aplicados con éxito'))
        return HttpResponseRedirect(self.request.get_full_path())
