from django.utils.translation import gettext_lazy as _
from django.views.generic.edit import FormView
from django.views.generic import UpdateView 
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib import messages
# Check if we can remove lda3
from ldap3 import  HASHED_SALTED_SHA
from ldap3.utils.hashed import hashed
from django_tables2 import SingleTableView

# project
from apps.views  import utils
from ..tables import DomainsTable
#from ..models.user import LdapUser as User
from ..models.domain import LdapDomain as Domain
from ..models.modules import Module, ModuleTop
from ..forms.domainForm import DomainForm, AddDomainForm, EditDomainForm, ResetDomainForm
from ..views.helpers_views import BaseLdapModelsView
from ..mixins import AdminUserPassesTestMixin
from ..views.json_tor import get_json_data 

class PassRequestToFormViewMixin:
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

class ListDomainsView(AdminUserPassesTestMixin, SingleTableView, BaseLdapModelsView):
    model = Domain
    table_class = DomainsTable
    template_name = 'ldapobj/pages/domain_list.html' 

    def get_queryset(self):
        domains = Domain.objects.all()
        query = self.request.GET.get("q")
        if query:
            domains = Domain.objects.filter(
	    Q(name__icontains=query) |
            Q(adminID__icontains=query) 
	  )
        return domains 


class DomainView(PassRequestToFormViewMixin, AdminUserPassesTestMixin, FormView):
    model=Domain
    form_class= DomainForm

    def get_success_url(self) -> str:
        return self.request.get_full_path()

    def lock_module(self):
        module = Module.objects.get(ou="domains")
        module.lock()
    
    def get_context_data(self, **kwargs):

        #domains_module = utils.get_cpanel_local_status(self.request.ldap, 'domains')
        domains_module = Module.objects.get(ou="domains")
        domains_module = domains_module.status
        context = super(DomainView, self).get_context_data(**kwargs)
        context['domains_module'] = domains_module
        return context
    
    
    def get_form_class(self):
        domains_module = Module.objects.get(ou="domains")
        domains_module = domains_module.status
        if domains_module == 'error':
            return ResetDomainForm
        else:
            return self.form_class

class AddDomainView(DomainView):

    template_name = 'ldapobj/pages/add_domain.html' 
    success_url = reverse_lazy('domains')

    def get_form_class(self):
        domains_module = Module.objects.get(ou="domains")
        domains_module = domains_module.status
        if domains_module == 'error':
            return ResetDomainForm
        else:
            return AddDomainForm

    def form_valid(self, form, **kwargs):
        #If domains module is in error, try to launch again before editing 
        form = self.get_form()

        context = self.get_context_data()
        if context["domains_module"] == "error":
            try:
                form.save()
                messages.success=(self.request, _('Reanudando operación previa. El proceso tardará unos minutos'))
            except Exception as e:
                 messages.error=(self.request, _('Se ha producido un error'))
        else:
            """ domain form has very few fields from model. So manual adding has to be perdomed drectly to ldap """
            domain = form['name'].value().strip()
            webserver = form["webServer"].value()
            # WeserverType anf tor Field may not exist
            ws=""
            if 'webserver_type' in form.fields and form['webserver_type'].value():
                ws = form['webserver_type'].value()
            else:
                ws = 'apache'
            active   = form['accountActive'].value()
            admin  = form['adminID'].value() if form['adminID'].value() else 'nobody'
            dkim   = form['dkim'].value()
            wordpress = form['wordpressInstall'].value()
            documentRoot = form["documentRoot"].value()
            phpPool = form['phpPool'].value() if form['phpPool'] else None
            acl = form["acl"].value() 

            try:

                new_domain = form.save(commit=False)
                new_domain.status= 'webon' if webserver else 'weboff'
                new_domain.save()

                if wordpress:
                    wp = new_domain.set_wordpress(form["wpPass1"].value())
                    if not wp:
                        messages.error(self.request, _('Error activando wordpress'))

                if acl:
                    acldb = new_domain.set_acl("true")
                    if not acldb:
                        messages.error(self.request, _('Error activando ACL'))
                if ws:
                    ser_type= new_domain.set_webserver_type(ws)
                    if not ser_type:
                         messages.error(self.request, _('Error habilitando {}'.format(ws)))
                if 'onion_service' in form.fields and form['onion_service'].value():
                    # ldap bool is a string 
                    onion = new_domain.set_tor("TRUE")
                    if not onion:
                        messages.error(self.request, _('No se ha podido habilitar el dominio .onion'))

                if documentRoot:
                    path_obj = new_domain.set_path(documentRoot)
                    if not path_obj:
                        messages.error(self.request, _('Error configurando la carpeta raíz'))


                if dkim:
                    dkimdb = new_domain.set_dkim()
                    # lock dkim puppet local module
                    if dkimdb:
                        dkim, create = ModuleTop.objects.get_or_create(ou="opendkim", defaults={"status": "locked"})
                    else:
                        messages.error(self.request, _('Error activando DKIM'))

                postmaster = new_domain.set_postmaster() 
                if not postmaster:
                    messages.error(self.request, _('Error activando la cuenta Postmaster'))
                
                messages.success(self.request, _('Dominio %s añadido con éxito' % new_domain.name))
                """ Lock domain in the panel to trigger puppet local for domain creation """
                self.lock_module()


            except Exception as e:
                utils.p("view_domains.py", "✕ There's was a problem creating the domain %s" % new_domain.name, e)
                messages.error(self.request, _('Ha habido un problema creando el dominio %s.' % new_domain.name))

        #return super(AddDomainView, self).form_valid(form)
        return HttpResponseRedirect(self.success_url)
    


class EditDomainView(DomainView,UpdateView):
    model=Domain
    template_name = 'ldapobj/pages/edit_domain.html' 
    success_url = reverse_lazy('domains')

    def get_form_class(self):
        domains_module = Module.objects.get(ou="domains")
        domains_module = domains_module.status
        if domains_module == 'error':
            return ResetDomainForm
        else:
            return EditDomainForm

    def form_valid(self, form):

        context = self.get_context_data()
        if context["domains_module"] == "error":
            try:
                form.save()
                messages.success=(self.request, _('Reanudando operación previa. El proceso tardará unos minutos'))
            except Exception as e:
                 messages.error=(self.request, _('Se ha producido un error'))

        else:
            domain =  form.instance
            messages.error=""
            webmaster = form['adminID'].value()
            mail      = form['accountActive'].value()
            #old_dkim = utils.ldap_val(form['old_dkim'].value())
            dkim     = form['dkim'].value()
            acl      = form['acl'].value()
            phpPool = form['phpPool'].value() if form['phpPool'] else None
            # If user choose to also delete the webroot, we set the domain to
            # Purged instead, so any mounted folder will also be umounted
            if form['deleteWebroot'].value():
                webserver = 'purged'
            else:
                webserver = 'online' if form['webServer'].value() else 'offline'

            domain.status= webserver
            domain.save() 

            # Make sure the vd domain has the objectClass: metaInfo
            
            try:

                initials = self.get_form_kwargs()

                # ACL - update acl in ldap if it has changed
                # If webmaster or phpool has changed and acl are activated, we must set acl ststuts to pending
                # Get initial data for acl to check if it was activated
                if acl:
                    status = "true"
                else:
                    status = "false"
                # Need to update acl if have changed in form or if are enabled and 
                # owner or group has changed
                if 'acl' in form.changed_data or acl and ('adminID' in form.changed_data or 'phpPool' in form.changed_data):
                    acl_obj = domain.set_acl(status)    

                if 'webserver_type' in form.fields and 'webserver_type' in form.changed_data:
                    ws = form['webserver_type'].value()
                    ser_type= domain.set_webserver_type(ws)
                # TOR  checkbox may not exists
                if 'onion_service' in form.fields and 'onion_service' in form.changed_data:
                    tor = form['onion_service'].value()
                    domain.set_tor(utils.ldap_bool(tor))

                # Wordpress
                # Tell puppet to install wordpress
                if 'wordpressInstall' in form.fields and form['wordpressInstall'].value():
                    domain.set_wordpress(form["wpPass1"].value())

                if 'dkim' in form.changed_data:
                    if dkim:
                        #utils.add_dkim(self.request.ldap, domain.name)
                        domain.set_dkim()
                    else:
                        #utils.remove_dkim(self.request.ldap, domain.name)
                        domain.del_dkim()

                # Webroot deletion. It will be moved to trash
                if form['deleteWebroot'].value():
                    #utils.move_to_trash(self.request.ldap, domain.dn)
                    domain.move_to_trash()
                form.save()
            except Exception as e:
                utils.p("✕ view_domains.py", "There was a problem updating the domain", e)
                messages.error(self.request, _('Ha habido un error modificando el dominio.'))

            # Only lock domains if one of theese field has changed (Do not need for mail actions)
            if 'adminID' in form.changed_data or 'webServer' in form.changed_data or 'phpPool' in form.changed_data \
                    or 'deleteWebroot' in form.changed_data or 'wordpressInstall' in form.changed_data \
                    or 'acl' in form.changed_data or 'webserver_type' in form.changed_data or 'onion_service' in form.changed_data:
                self.lock_module()
            
            # Avoid double message Error and Success
            if not messages.error:
                messages.success(self.request, _('Edición realizada con éxito. Espera unos segundos hasta que el sistema aplique la nueva configuración'))
        return super().form_valid(form)
