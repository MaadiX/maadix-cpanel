# django
from django.views import View
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
from django.shortcuts import render
# project
from .. import utils
from ..mixins import AdminUserPassesTestMixin
from ..models.modules import Module

class Reboot(AdminUserPassesTestMixin, View):
    """
    View to reboot the system.
    """
    def get(self, request):
        """Handle GET requests."""
        return render(request, 'ldapobj/pages/reboot.html')

    def post(self, request, *args, **kwargs):
        try:
            Module.objects.update_or_create(
                ou="reboot", defaults={"status": "locked", "info": "ready"}
            )
            utils.lock_cpanel()
        except Exception as e:
            messages.error(self.request, _('Se ha generado un error'))

        return HttpResponseRedirect( reverse('logout') )