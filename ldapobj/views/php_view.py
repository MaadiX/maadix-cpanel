import psutil
from django.views.generic.edit import FormView
from django.views.generic import  UpdateView
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from ..mixins import AdminUserPassesTestMixin
from ..models.cpanelconf import CpanelBasicConf,CpanelAdvancedTopConf, Fpm
from ..models.domain import LdapDomain
from ..forms.phpForm import PhpVersionForm, PhpFpmForm, AddPoolForm
from .helpers_views import PassRequestToFormViewMixin
from ..utils import p, lock_cpanel, get_available_pools

class PhpVersionView(AdminUserPassesTestMixin,PassRequestToFormViewMixin, UpdateView):
    Model=CpanelBasicConf
    form_class= PhpVersionForm
    template_name = "ldapobj/pages/php_version.html"


    def get_object(self):
        obj = CpanelBasicConf.objects.get(pk="php")
        return obj

class PhpFpmView(FormView):

    template_name = 'ldapobj/pages/php-fpm.html'
    form_class    = PhpFpmForm
    AddPoolForm  = AddPoolForm

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_existing_pools(self):
        try:
            obj_pool = CpanelAdvancedTopConf.objects.get(ou="fpmpools")
            pools = int(obj_pool.status)
        except Exception as e:
            print(e)
            pools = int(0)
        return pools

    def get_max_pools(self):
        # Calculate max pools based on RAM
        memory = psutil.virtual_memory()
        return ((memory.total)/1024/1024/1024)*10

    def update_tot_pools(self, quantity):
        try:
            CpanelAdvancedTopConf.objects.update_or_create(
                ou="fpmpools", defaults={"status": quantity}
                )
        except Exception as e:
            p("view_phpfpm.py · form_valid", "✕ There was a problem adding pool", e)

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context               = super(PhpFpmView, self).get_context_data(**kwargs)
        context['show_modal'] = 'form' in kwargs
        context['show_modal_add'] = 'AddPoolForm' in kwargs
        context["allowed_pools"] = self.allowed_pools 
        context["existing_pools"] = self.get_existing_pools()
        context["user_fpms"] = self.user_fpms 
        context["enabled_pools"] = 'dynamic' in [x['type'] for x in self.user_fpms]
        context["disabled_pools"] = 'disabled' in [x['type'] for x in self.user_fpms]       
        context["formAddPools"]= self.AddPoolForm
        return context

    def get_pool_domains(self,pool):
        # Check if a pool has any assigned domain
        # and return them
        try:
            domains = LdapDomain.objects.filter(typename=pool)
        except Exception as e:
            print(e)
        return domains

    def update_fpm_status(self,action, fpm_id):
        #dn: ou=ifpm_id,ou=fpmpools,ou=conf,ou=cpanel,dc=example,dc=tld
        # Possible actions are:
        # disable
        # enable
        # delete

        fpm_obj =  Fpm.objects.get(ou=fpm_id)
        if (action == 'delete'):
            tot_slots = self.get_existing_pools()
            quantity = tot_slots -1 
            fpm_obj.delete()
            self.update_tot_pools(quantity)

        else:
            #update_error = _('No se ha podido actualizar la información')
            fpm_obj.typename = action
            fpm_obj.save()
        
    def get_form(self):
        # Check how many pools cab be created. This is the max pools based on RAM - the pools already created.
        self.allowed_pools = int(self.get_max_pools()) - int(self.get_existing_pools())
        # Get all existing fpmx pools like ou=fpm1,ou=fpmpools,ou=conf,ou=cpanel,dc=example,dc=tld
        # These a re user created pools
        self.user_fpms =  get_available_pools(self.request.ldap)
        return self.form_class(allowed_pools=self.allowed_pools,user_fpms= self.user_fpms,**self.get_form_kwargs())

    def get_form_add(self):
        return self.AddPoolForm(**self.get_form_kwargs())

    def post(self, request, *args, **kwargs):
        existing_pools = self.get_existing_pools()
        form = self.get_form()
        AddPoolForm = self.get_form_add()
        context = self.get_context_data(**kwargs)
        allowed_pools = int(context["allowed_pools"])
        newpools = request.POST.get('newpools')
        if request.method == "POST" and 'addpools' in request.POST:
            if newpools.isdigit() and  int(newpools) < 1 or int(newpools) > int(allowed_pools):
                messages.error(request, _('Debes insertar un número entre 0 y %d' % int(allowed_pools) ))
                context['show_modal_add'] = False 
                return HttpResponseRedirect( reverse('php-fpm') )
            else: 
                context['show_modal_add'] = True
            context["formAddPools"] = AddPoolForm
        elif request.method == "POST" and 'editools' in request.POST:
            context['show_modal'] = True

        #objectClasses = ['organizationalUnit', 'metaInfo','top']
        #error = _("Ha ocurrido un error. No se ha podido guardar la nueva configuración")
        # Check which one of the two forms has been posted.
        # there are two differnet confimation modals with different input name
        # as there can not be two equals in the same page
        # Maybe there is abetter way to do it.
        # Some pool has been edited

        if request.method == "POST" and 'writeldap' in request.POST: 
            for field in form:
                #If a checkbox i checked
                if field.value():
                    fpm_str = field.name.split('-')
                    action = fpm_str[0]
                    fpm_id = fpm_str[1]
                    if(action =='edit'):
                        action = request.POST.get(field.name)
                    self.update_fpm_status(action, fpm_id)                
            lock_cpanel()
            return HttpResponseRedirect( reverse('logout'))
        # Some pool has been added
        elif request.method == "POST" and 'writeldapmulti' in request.POST:
            if newpools:
                try:
                    update_value = existing_pools + int(newpools)
                    self.update_tot_pools(update_value)
                    i=0;
                    while(i < int(newpools)):
                        # The first pool to be created must be the higher fpmX + 1
                        fpm_id = 'fpm%s' % str(existing_pools+1)
                        Fpm.objects.create(ou=fpm_id, typename='dynamic')
                        i+=1
                        existing_pools+=1
                    lock_cpanel()
                    return HttpResponseRedirect( reverse('logout'))
                except Exception as e:
                    messages.error(request, _('Ha habido un error en la operación.' ))
                    p("view_phpfpm.py · form_valid", "✕ There was a problem adding pool", e)
        
        return self.render_to_response(context)
