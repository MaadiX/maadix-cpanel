import os
import django_tables2 as tables
from django.utils.translation import gettext_lazy as _
from .models.user import LdapUser
from .models.domain import LdapDomain, Postmaster
from .models.mail import LdapMail
from .models.trash import DomainTrash, UserTrash, MailTrash, BackupsTrash
from django.utils.html import format_html
from django_tables2.utils import A
from django.conf import settings
from . import utils
import itertools

class CustomCheckBoxColumn(tables.CheckBoxColumn):
    @property
    def header(self):
        return _("Eliminar")

class AccountTable(tables.Table):
    #row_number = tables.Column(empty_values=())
    #roles = tables.Column(orderable=False, empty_values=())

    remove = tables.CheckBoxColumn(
        accessor='pk',
        attrs = { "th__input": {"name":"select-all","value":"remove-accounts"},
                  "td__input":{"class":"remove-accounts"}},
        orderable=False)
    edit = tables.LinkColumn(
        "edit-ldapuser",
        #text=lambda record: record.username,
        text=format_html("<i class='fa fa-edit'></i>"),
        args=[A("username")],
        orderable=False)

    is_webmaster = tables.Column(
            empty_values=(),
            orderable=False,
            verbose_name="Webmaster"
            )

    class Meta:
        model = LdapUser
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("remove","username","email","authorized_service", "is_webmaster" )
        attrs = {"class": "table table-striped table-hover table-responsive",
                "th": { "class": "table-dark"}
                }
        row_attrs = {'id' : lambda record: record.pk}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_row_number(self):
        return "Row %d" % next(self.counter)

    def render_is_webmaster(self, value):
        """
        all_domains = ""
        for it in value:
            all_domains += ', ' +it
        return format_html("{}", all_domains.strip(','))
        """
        all_domains ="<ul>"
        for it in value:
            all_domains += '<li>' +it + '</li>'
        all_domains+=("</ul>")
        return format_html(all_domains)

    def render_authorized_service(self, record, value):
        all_items = "<ul class='text-capitalize'>"
        for it in value:
            if it == "apache":
                it = "Phpmyadmin"
            elif it=="sshd":
                if int(record.group)==int(settings.SFTP_GUID):
                    it="SFTP"
                else:
                    it="SSH/SFTP"
            all_items += "<li>" + it + "</li>"
        all_items+="</ul>"
        return format_html(all_items)
    def render_remove(self, record):
        if record.is_webmaster:
            not_allowed = _("No se puede eliminar una cuenta si es webmaster de algún dominio")
            return format_html("<a href='#' title='{}'><i class='fa fa-question'></i></<a>",not_allowed)
        else:
            return format_html('<input class="move-accounts" type="checkbox" name="remove" value={}>', record)


class DomainsTable(tables.Table):
    servidor_web = tables.Column(
        empty_values=(),
        orderable=False)
    dns= tables.LinkColumn(
        "checkdns",
        text=format_html("<i class='fa fa-eye'></i>"),
        args=[A("pk")],
        orderable=False)

    edit = tables.LinkColumn(
        "edit-ldapdomain",
        text=format_html("<i class='fa fa-edit'></i>"),
        args=[A("pk")],
        orderable=False)

    remove = CustomCheckBoxColumn(
        accessor='pk',
        orderable=False)


    class Meta:
        model = LdapDomain
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("remove","name","adminID","accountActive","servidor_web", "dns", "edit" )
        attrs = {"class": "table table-striped table-hover table-responsive",
                "th": { "class": "table-dark"}
                }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_servidor_web(self, record):
        domain = record.name
        res = os.path.isfile('/etc/apache2/ldap-enabled/%s.conf' % domain)
        if res:
            render_field =  format_html("<i class='fa fa-power-off enabled'></i>")
        else:
            render_field = format_html("<i class='fa fa-power-off disabled'></i>")
        if utils.has_cms(domain):
            render_field+=format_html("<i class='fa fa-wordpress small-icon ms-3'></i>")
        try: 
            obj = LdapDomain.objects.get(name=domain)
            if obj.tor:
                render_field+=format_html("<i class='ti ti-onion-alt small-icon ms-3'></i>")
        except Exception:
            pass
        return render_field


class MailsTable(tables.Table):
    edit = tables.LinkColumn(
        "edit-ldapmail",
        text=format_html("<i class='fa fa-edit'></i>"),
        args=[A("pk")],
        orderable=False)

    remove = CustomCheckBoxColumn(
        accessor='pk',
        orderable=False)
        
    class Meta:
        model = LdapMail
        row_number = tables.Column(empty_values=())
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("remove","mail","cn","vacationActive", "forwardActive", "quota","edit" )
        attrs = {"class": "table table-striped table-hover table-responsive",
                "th": { "class": "table-dark"}}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_quota(self, value):
        if value=='0':
            return format_html(_("Ilimitado"))
        else:
            return format_html("{} GB", int(value)/1024**3)

    def render_row_number(self):
        return "Row %d" % next(self.counter)

class PostmasterTable(tables.Table):
    edit = tables.LinkColumn(
    "postmaster",
    text=format_html("<i class='fa fa-edit'></i>"),
    args=[A("mail")],
    orderable=False)

    class Meta:
        model = Postmaster
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("mail","edit")
        attrs = {"class": "table table-striped table-hover table-responsive",
                "th": { "class": "table-dark"}
                }

#Trash tables
class TrashTable(tables.Table):
    typename = tables.Column(
            empty_values=(),
            orderable=False,
            verbose_name=_("Nombre")
            )    
    otherPath = tables.Column(
            empty_values=(),
            orderable=False,
            verbose_name=_("Carpeta")
            )   
    description = tables.Column(
            empty_values=(),
            orderable=False,
            verbose_name=_("Fecha")
            )   
    class Meta:
        template_name = "django_tables2/bootstrap5-responsive.html"
        fields = ("remove","typename","otherPath", "description")
        attrs = {"class": "table table-striped table-hover table-responsive",
                "th": { "class": "table-dark"}
                }

class TrashDomainsTable(TrashTable):

    remove = tables.CheckBoxColumn(
        accessor='pk',
        attrs = { "th__input": {"name":"select-all","value":"remove-domains", "class":"thin-column"},
                  "td__input":{"class":"remove-domains"},
                  "th": {"class": "thin-class"},
                  "td": {"class": "thin-class"},
                  },
        orderable=False)

    class Meta(TrashTable.Meta):
         model = DomainTrash


    def render_remove(self, record):
        if record.status=='intrash':
            return format_html("<input id='id_{}', class='remove-domains' type='checkbox' name='domains[]' value='{}'/>",str(record.cn),str(record.cn))
        else:
            return format_html("<i class='fa fa-spinner fa-spin fa-1x fa-fw'></i>")

class TrashUsersTable(TrashTable):

    remove = tables.CheckBoxColumn(
        accessor='pk',
        attrs = { "th__input": {"name":"select-all","value":"remove-users", "class":"thin-column"},
                  "td__input":{"class":"remove-users"},
                  "th": {"class": "thin-class"},
                  "td": {"class": "thin-class"},
                  },
        orderable=False)

    class Meta(TrashTable.Meta):
        model = UserTrash 

    def render_remove(self, record):
        if record.status=='intrash':
            return format_html("<input id='id_{}' class='remove-users' type='checkbox' name='users[]' value='{}'/>",str(record.cn),str(record.cn))
        else:
            return format_html("<i class='fa fa-spinner fa-spin fa-1x fa-fw'></i>")

class TrashMailsTable(TrashTable):

    remove = tables.CheckBoxColumn(
        accessor='pk',
        attrs = { "th__input": {"name":"select-all","value":"remove-mails", "class":"thin-column"},
                  "td__input":{"class":"remove-mails"},
                  "th": {"class": "thin-class"},
                  "td": {"class": "thin-class"},
                  },
        orderable=False)

    class Meta(TrashTable.Meta):
        model = MailTrash 

    def render_remove(self, record):
        if record.status=='intrash':
            return format_html("<input id='id_{}' class='remove-mails' type='checkbox' name='mails[]' value='{}'/>",str(record.cn),str(record.cn))
        else:
            return format_html("<i class='fa fa-spinner fa-spin fa-1x fa-fw'></i>")

