from django.apps import AppConfig


class LdapobjConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ldapobj'
