# python
import datetime, time, os, urllib3, json, shutil, socket, re, psutil,  certifi,subprocess
from spf_validator import validator
# django
from django.utils.translation import gettext_lazy as _, get_language
from django.core.management import utils as django_management_utils
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.utils.crypto import get_random_string
from ldap3.core.exceptions import LDAPNoSuchObjectResult
# contrib
import ldap3
import gnupg
import psycopg2
# project
from django.conf import settings

from ldapobj.models.modules import Module
from .models.cpanelconf import Cpanel, CpanelBasicConf
from .models.domain import LdapDomain
from .models.trash import BackupsTrash
from ldapobj.models.appsGroups import App, AppDependency
    
def get_server_host() -> str:
    hostname = socket.getfqdn()
    return hostname if hostname != '0' else 'localhost'

def get_server_ip() -> str:
    ip = subprocess.getoutput("facter --external-dir=/opt/puppetlabs/facter/facts.d public_ip")
    return ip

def get_server_hostname() -> str:
    hostname = settings.CUSTOM_HOSTNAME if hasattr(settings, 'CUSTOM_HOSTNAME') else socket.gethostname()
    return hostname

# Calculate memory used by given list of apps
# if no memeory param is provided memory.total will be used
def get_remaining_resources(services_available: list, total_apps:list, memory=None) -> dict:
    results = {} 
    if not memory:
        memory = psutil.virtual_memory()
        memory = memory.available
    ram_required= 0
    disk_required=0
    disk_usage     = psutil.disk_usage('/')
    for service in services_available:
        if service['settings']['id'] in total_apps:
            ram_required = ram_required + service['resources']['ram']
            disk_required = disk_required + service['resources']['disk']
    results['memory'] = memory/(1024*1024) - ram_required
    results['disk'] = disk_usage.free/(1024*1024) - disk_required
    return results

def p(trigger, txt, exception):
    """ Prints exceptions to console. """
    if settings.DEBUG:
        print("---", trigger, txt, str(exception), "---", sep="\n")

def unix_timestamp() -> int:
    """ Returns Unix timestamp """
    return int(round(time.time() * 1000))

def ldap_creation_date() -> str:
    """ Returns creation date in the format used by LDAP """
    return datetime.datetime.now().strftime('%Y%m%d')
    
def ldap_creation_dateTime() -> str:
    """ Returns creation dateTime in the format used by LDAP """
    return datetime.datetime.now().strftime('%Y-%m-%d-%H:%M:%S') 

def quota_to_ldap(quota: str) -> str:
    if quota and not quota=='0':
        #allow user tu use coma or dot
        qu = quota.replace(",",".")
        qu = float(qu)*(1024**3)
        qu = int(qu)
        # quta in ldap must be string
        quota = str(qu)
    else:
        quota = '0'
    return quota

def ldap_bool(bool: bool) -> str:
    """ Convert a boolean value to its string representation in LDAP """
    if bool:
        return 'TRUE'
    return 'FALSE'

def ldap_val(val: str)-> any:
    """ Convert LDAP val to a valid str representation in Django """
    #Modify the logic to use this function also for text attribute that may be true or false (lowercase)
    if val.lower() == 'true':
        return True
    elif val.lower() == 'false':
        return False
    else:
        return val
def gen_pwd() -> str:
    """ Generates random password. """
    return django_management_utils.get_random_secret_key()

def get_user_role(username: str) -> str:
    return 'admin' if not '@' in username else 'postmaster' if 'postmaster' in username else 'email'

def user_format(username: str) -> str:
    # if @ in the username try to build DN of webmasters or email accounts
    if '@' in username:
        username_components = username.split('@')
        user   = username_components[0]
        domain = username_components[1]
        if user == 'postmaster':
            return 'cn=postmaster,vd=%s,%s' % ( domain, settings.LDAP_TREE_HOSTING )
        return 'mail=%s,vd=%s,%s' % ( username, domain, settings.LDAP_TREE_HOSTING )
    # else try admin dn
    return "cn=%s,%s" % ( username, settings.LDAP_TREE_BASE )

def connect_ldap(username: str, password: str):
    """ Connect to LDAP """
    dn = username
    try:
        # Configure the connection.
        if settings.AUTH_LDAP_START_TLS:
            auto_bind = ldap3.AUTO_BIND_TLS_BEFORE_BIND
        else:
            auto_bind = ldap3.AUTO_BIND_NO_TLS
        connection = ldap3.Connection(
            ldap3.Server(
                settings.LDAP_AUTH_URL,
                allowed_referral_hosts = [("*", True)],
                get_info               = ldap3.NONE,
                connect_timeout        = settings.LDAP_AUTH_CONNECT_TIMEOUT,
            ),
            user             = dn,
            password         = password,
            auto_bind        = auto_bind,
            raise_exceptions = True,
            receive_timeout  = settings.LDAP_AUTH_RECEIVE_TIMEOUT,
        )
        return {
            'connection' : connection,
            'error'     : None,
        }
    except Exception as e:
        print("There's a problem connecting to LDAP: %s" % str(e))
        return {
            'connection' : None,
            'error'      : e.__class__.__name__
        }

def validate_file_name(filename: str) -> bool:
    # Check vlid chaeacters  for file names
    if not re.match("^[^<>;,?\"'*&|\/]+$",filename):
       return False
    else:
        return True
       
def validate_domain_format(domain: str) -> bool:
    if re.match("^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,20}$", domain):
        return True
    else:
        return False


def get_folder_size(path:str) -> bytes:

    #initialize the size
    total_size = 0

    #use the walk() method to navigate through directory tree
    for dirpath, dirnames, filenames in os.walk(path):
        for i in filenames:

            #use join to concatenate all the components of path
            f = os.path.join(dirpath, i)

            #use getsize to generate size in bytes and add it to the total size
            total_size += os.path.getsize(f)
    return total_size

def list_files_in_trash(ldap,dirValue,dirKey,listFiles)-> list:
    
    filesToList = []
    for item in listFiles:
        fname = os.path.join(dirValue, item)
        if os.path.isfile(fname):
            fSize = os.path.getsize(fname)
            itemtype="IS A File"
        elif os.path.isdir(fname):
             fSize = get_folder_size(fname)
             itemtype="IS Arectory"
        dataF= str(round(fSize / (1024 * 1024), 3)) + "MB"
        info = os.stat(fname)
        #fSize = info.st_size
        lastAcces = os.path.getatime(fname)
        date = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(info.st_mtime))
        # If a file has been recently moved  to trash, it may still appear in the dir_path
        # When moving it to trash we record it's inode so we make sure we do not move to trash an unwanted item
        # By default, file is not in ldap: status:none
        filterS = "(&(objectClass=applicationProcess)(l=%s))" % info.st_ino
        base    = "ou=%s,%s" % (dirKey,settings.LDAP_TREE_TRASH)
        try:
            ldap.search(base,
                filterS,
                attributes=['l','status'],
                search_scope=ldap3.SUBTREE,)
            #TODO: change totrash for purge
            if (ldap.entries):
                status=ldap.entries[0].status.value
            else:
                status="none"
        except Exception as e:
            p("free view", "Error READING to  LDAP", e)
        data = {"cn" : item, "otherPath" :dataF , "description" : date, "inode" : info.st_ino,"type": item, "status" :status}
        filesToList.append(data)
    return filesToList

def move_file_to_trash(itemType, name, inode):
    # This method fidders from move_to_trash as it is moving files that are only 
    # in file system (not ldap yet)
    # Maybe one day we will add more features than backup-folder (emails folder or whatever)
    # So make a deep check
    #validate filename. avoid  *, ..  
    try: 
        validItem = validate_file_name(name)
        if validItem:
            BackupsTrash.objects_update_or_crete(
                cn              = name,
                otherPath       = settings.BACKUPS_PATH,
                typename        = name,
                description     = ldap_creation_dateTime(),
                status          = 'purge',
                l               = inode,
            )
    except Exception as e:
        p("utils.py · move_to_trash", "x error moving item to trash " , e)
        

def validate_username(user: str) -> bool:
    # For unix username string must start with alphabetic character.
    # Only -, _ and . are allowed
    if not re.match("^[a-zA-Z0-9]+([\-\.\_]{1}[a-z0-9]+)*$", user):
       return False
    else:
        return True

def anonymous_connect_ldap() -> object:
    """ Connect to LDAP as anonymous user. """

    try:
        # Configure the connection.
        if settings.AUTH_LDAP_START_TLS:
            auto_bind = ldap3.AUTO_BIND_TLS_BEFORE_BIND
        else:
            auto_bind = ldap3.AUTO_BIND_NO_TLS
        c = ldap3.Connection(
            ldap3.Server(
                settings.LDAP_AUTH_URL,
                allowed_referral_hosts=[("*", True)],
                get_info=ldap3.NONE,
                connect_timeout=settings.LDAP_AUTH_CONNECT_TIMEOUT,
            ),
            auto_bind=auto_bind,
            raise_exceptions=True,
            receive_timeout=settings.LDAP_AUTH_RECEIVE_TIMEOUT,
        )
        print("TYPE OF LDA?", type(c))
        return c
    except Exception as e:
        print("There's a problem connecting to LDAP: %s" % str(e))
        return None


def mxcp_get_api_credentials(request):
    credentials = None
    try: 
        request.ldap.search(
            settings.LDAP_TREE_API,
            '(objectClass=*)',
            attributes=['uid', 'userpassword', 'pass', 'host']
        )
        credentials = request.ldap.entries[0]
    except Exception as e:
        p("utils.py", "✕ There's a problem connecting with LDAP", e)
    return credentials

def mxcp_api_connect(request, endpoint):
    # Look for services
    # enppont for get puppet status; '/vm/' + get_server_hostname()
    # endpoint for release_info : /releasesi/release_xxxx?lang=en
    response_info = None
    credentials = mxcp_get_api_credentials(request)
    if credentials:
        try:
            if hasattr(settings, 'CUSTOM_API_ENDPOINT'):
                url = settings.CUSTOM_API_ENDPOINT + endpoint
            else:
                url = credentials.host.value + endpoint
            if settings.DEBUG:
                urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            http = urllib3.PoolManager(
                cert_reqs='CERT_REQUIRED',
                ca_certs=certifi.where())
            # Api call for puppetstatus must be GET
            response = http.request('GET', url,
                headers={
                    'Content-Type' : 'application/json',
                    'Authorization' : 'Token ' + credentials.userpassword.value,
                    'Accept-Language': get_language(),
                    'X-HOSTNAME'    : get_server_hostname()
                }
            )
        
            response_info = response.data.decode('utf-8')
            response_info = json.loads(response_info)
        except Exception as e:
            p("utils.py", "✕- get_puppet_status- There's a problem with the HTTP Request to API endpoint %s" % url, e)
    return response_info

def get_puppet_status(request):
    endpoint = '/vm/' + get_server_hostname()
    puppet_status = mxcp_api_connect(request, endpoint)
    return puppet_status


def get_cpanel_status():
    """ If cpanel is locked user can't log in"""
    try:
        cp = Cpanel.objects.get(pk="cpanel")
        return cp.status
    except Exception as e:
        p('utils.py', "✕ There was a problem getting cpanel", e)
        return None

def lock_cpanel():
    """ Locks CPanel """
    try:
        cp = Cpanel.objects.get(pk="cpanel")
        if cp.status == "ready":
            cp.lock()
            return 1
    except Exception as e:
            p('utils.py', "✕ There was a problem locking cpanel", e)
    return 0

def get_current_release(): 
    release = ""
    try:
        cp = Cpanel.objects.get(pk="cpanel")
        release = cp.typename
    except Exception as e:
        print("There's a problem retreivin release")
        print(e)
    return release

def get_release_info(request, route=None):
    cur_release = get_current_release()
    endpoint =  '/releases/%s?lang=%s' % (
        cur_release,
        get_language()
        )
    try:
        release_info = mxcp_api_connect(request, endpoint)
    except Exception as e:
        print("Error connecting to endpoin", e)
    return release_info 


def get_release_field(release, field, lang):
    fields = [ f for f in release['fields'] if f['lang'] == lang ]
    return fields[0][field] if fields else ''

def check_value(query, values):
    if values:
        return values == query or query in values
    return False

def get_cpanel_local_status(service):
    """ If cpanel local module is locked user can't perform another similar action"""
    try:
        full_dn = "ou=%s,%s" % (service, settings.LDAP_TREE_CPANEL)
        module = Module.objects.get(dn=full_dn)
        return module.status
    except Exception as e:
        p("utils.py", "✕ There's a problem connecting with LDAP", e)
        return None

def notify_email(email):
    """ Notifies user about his new email. """

    try:
        body = _("Bienvenido a tu nuevo buzón. Por favor no contestes a este mensaje")
        message = EmailMessage('Cuenta activata', body, 'no-reply@%s' % get_server_host(), [email])
        message.send(fail_silently=False)
    except Exception as e:
        p("utils.py · notificate_email", "✕ There's a problem notifying new email " % email, e)

def send_vpn_instructions(request, email, username):
    """ Sends by email VPN instructions """

    try:
        # body
        host = get_server_host()
        body =  _("Hola."
                 "Se ha creado un acceso VPN para tu cuenta en %(host)s"
                 "Nombre: %(username)s"
                 "Contraseña: Por razones de seguridad, no se envían contraseñas por correo electrónico. "
                 "Por favor, descarga el archivo adjunto y sigue las instrucciones "
                 "para tu sistema operativo, disponibles en: "
                 "https://docs.maadix.net/vpn/" % ({'host':host, 'username':username}))
        host = get_server_host()
        html_body = _("<p>Hola.</p><p>Se ha creado un acceso VPN para tu cuenta en %(host)s</p>"
                 "<p><b>Nombre</b>: %(username)s</p>"
                 "<p><b>Contraseña</b>: <em>Por razones de seguridad, no se envían contraseñas por correo electrónico. "
                 "</em></p>"
                 "<p>Descarga el archivo adjunto y sigue las instrucciones "
                 "para tu sistema operativo, disponibles en:</p>"
                 "<p><a href='https://docs.v2.maadix.net/vpn'>https://docs.maadix.net/vpn/</a></p>" % ({'host':host, 'username':username}))
        # attachments
        filesdir   = '/tmp/vpn_files'
        foldername = 'VPN-%s' % get_server_ip()
        folderpath = '%s/%s'  % (filesdir, foldername)
        filepath   = '%s' % folderpath
        if not os.path.isdir(filesdir):
            os.mkdir(filesdir)
        """
        if not os.path.isdir(folderpath):
            os.mkdir(folderpath)
        """
        crt     = '/etc/openvpn/ca.crt'
        tmp_crt = '%s/ca.crt' % filesdir
        if os.path.isfile(crt):
            shutil.copyfile(crt, tmp_crt)
        vpn_config_files = '%s/files/vpn_config' % settings.BASE_DIR
        # copy_tre problem with cache. It only copy once
        # https://bugs.python.org/issue10948
        #copy_tree(vpn_config_files, folderpath)
        shutil.copytree (vpn_config_files, folderpath)
        for root, dirs, files in os.walk(folderpath):
            for subfolder in dirs:
                # Override crt each time.  It may have changed
                shutil.copy(crt, os.path.join(root, subfolder, 'ca.crt'))
        conf_files = [
            '%s/linux/vpn.conf'              % folderpath,
            '%s/windows/vpn.ovpn'            % folderpath,
            '%s/android/android-client.ovpn' % folderpath,
            '%s/macos.tblk/vpn.ovpn' % folderpath,

        ]
        for conf_file in conf_files:
            with open(conf_file, 'a') as f:
                f.write('remote %s' % get_server_ip())
                f.close()
        shutil.make_archive(filepath, 'zip', folderpath)
        attachment_file = filepath + '.zip'
        # send email
        #to = request.ldap.entries[0].cn.value if request.ldap.entries else 'www-data@%s' % get_server_host()
        to = email
        sender = get_notifications_address(request.ldap)
        sendmail = EmailMultiAlternatives('Cuenta VPN activada', body, sender, [to])
        sendmail.attach_alternative(html_body, "text/html")
        if os.path.isfile(attachment_file):
            sendmail.attach_file(attachment_file)
        try:
            sendmail.send(fail_silently=False)
            shutil.rmtree(filesdir)
            # Remove von temporary files. If IP or ca changes we nedd to regenrate them
            # Build the .zip folder each time
            #shutil.rmtree(filesdir, ignore_errors=True)
        except Exception as e:
            p("utils.py · send_vpn_instructions", "✕ There's a problem sending new email %s" % sendmail, e)
    except Exception as e:
        p("utils.py · send_vpn_instructions", "✕ There's a problem notifying new email %s" % email, e)



def get_existing_emails(ldap, domain=None):
    """ Get a list of existing emails in LDAP. """

    base    = settings.LDAP_TREE_HOSTING
    if domain:
        base = 'vd=%s,%s' % ( domain, base )
    try:
        ldap.search(
            base,
            settings.LDAP_FILTERS_EMAILS,
            attributes    = ['mail', 'cn']
        )
        return ldap.entries
    except Exception as e:
        p("utils.py · get_existing_emails", "✕ There was a problem getting list of emails", e)

    return []


def get_existing_domains():
    #TOD : check if we should retun just  pk
    return LdapDomain.objects.all() 


def domain_is_in_use(domain):
    """ Check ig a given domain is in use by some application """
    # Get all groups:
    all_apps = App.objects.all()
    for app in all_apps:
        app_domain_dn = "ou=domain,ou=%s,%s" % (app.pk, settings.LDAP_TREE_SERVICES) 
        try:
            app_domain_obj  = AppDependency.objects.filter(dn=app_domain_dn)
            if domain == app_domain_obj.status:
                return True
        except Exception as e:
            pass
    return False

def is_valid_domain(domain):
    message = ''
    ldap_domains = LdapDomain.objects.filter(name=domain)
    domains_in_use = domain_is_in_use(domain)
    if not re.match("^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,20}$", domain):
        message = _("%s no es un nombre de dominio válido" % domain)
    elif ldap_domains:
        message =_("El dominio %s ya está activado" % domain)
    elif domain in get_server_host():
        message =_("Este es el dominio actual del panel de control")
    elif domains_in_use:
        message = _("El dominio %s está en uso por otra aplicación" % domain)
    return message 

def only_alphanumeric_characters(name):
    if re.match("[A-Za-z0-9]+$", name):
        return True
    else:
        return False

def not_valid_characters(string):
    if re.match("[A-Za-z0-9\_\-\.\@]+$", string):
        return False 
    else:
        return True

def not_valid_path(string):
    if re.match("[A-Za-z0-9\_\-\/]+$", string):
        return False
    else:
        return True

def get_notifications_address(ldap):
    """ Get email address that sends notifications. """
    sendermail = 'www-data@%s' % get_server_host()
    try:
        ldap.search(
            search_base   = settings.LDAP_TREE_SENDERMAIL,
            search_filter = settings.LDAP_FILTERS_SENDERMAIL,
            attributes    = ['cn']
        )
        if ldap.entries[0].cn.value:
            sendermail = ldap.entries[0].cn.value
    except Exception as e:
        p("utils.py · get_notifications_address", "✕ There was a problem retrieving the sendermail", e)
        if e.result == 32:
            ldap.add(settings.LDAP_TREE_SENDERMAIL, [
                'organizationalUnit',
                'top',
                'metaInfo'
            ],
            {'cn': sendermail})
    return sendermail

def get_admin(ldap):
    """ Get admin data for password recovery. """

    try:
        ldap.search(
            search_base   = settings.LDAP_TREE_BASE,
            search_filter = "(&(objectclass=extensibleObject)(objectclass=organizationalRole)(!(cn=uidNext)))",
            attributes    = ['email', 'cn', 'dn']
        )
        return ldap.entries[0]
    except Exception as e:
        p("utils.py · get_existing_emails", "✕ There was a problem getting superadmin data", e)

    #return {}

def get_active_maildomains(ldap):
    try:
        ldap.search(
            search_base   = settings.LDAP_TREE_BASE,
            search_filter = settings.LDAP_FILTERS_MAILMAN,
            attributes    = ['vd',]
        )
        return [ entry.vd.value for entry in ldap.entries ]
    except Exception as e:
        p("utils.py · get_active_maildomains", "✕ There was a problem getting mailman domains", e)

    return {}

def connect_to_postgre():
    mailman_domains = {}
    try: 
        #dsn = "postgresql:///mailman"
        dsn="host='localhost' dbname='mailman' user='postgres' password=''"
        conn = psycopg2.connect(dsn)
        cursor = conn.cursor()
        cursor.execute("SELECT * from domain")
        rows = cursor.fetchall()
        for row in rows:
            n, mail_host,_desc= row
            mailman_domains = {'mail_host': mail_host, 'description': desc }
        #new_list = [(n,mail_host,desc) for a,b in rows]
        """
        for line in rows:
            domain_val = [x[1] for x in line ]
            desc_val = [x[2] for x in line ]
            mailman_domains['mail_host']=domain_val
            mailman_domains['mail_host']=domain_val
        """
        return mailman_domains
    except Exception as e:
        print("Uh oh, connect_to_postgre - can't connect. Invalid dbname, user or password?")
        print(e)
    return {} 

def get_mailman_domains():
    mailman_domains = []
    try:
        # Data for template testing purposes
        if settings.NO_MAILMAN:
            mailman_domains = [
                { 'mail_host': 'example.com', 'description' : 'test_description '},
                { 'mail_host': 'imy.example.com', 'description' : 'test_description '},
            ]
        else:
            try:
                dsn = "postgresql:///mailman"
                #dsn="host='' dbname='mailman' user=w' password=''"
                conn = psycopg2.connect(dsn)
                cursor = conn.cursor()
                cursor.execute("SELECT * from domain")
                domains = cursor.fetchall()
                #return rows
                if domains:
                    for domain in domains:
                        domain_dic = { 'mail_host' : domain[1], 'description' : domain[2] }
                        mailman_domains.append(domain_dic)

            except Exception as e:
                print("Uh oh, can't connect. Invalid dbname, user or password?")
                print(e)

    except Exception as e:
        p("utils.py · get_mailman_domains", "✕ There was a problem getting mailman domains", e)
    return mailman_domains

def get_mailman_domain_names():
    mailman_domains = []
    for domain in get_mailman_domains():
        mailman_domains.append(domain['mail_host'])
    
    return mailman_domains

    
""" Split dependecy string coming from api"""
def split_dependencies(dependencies):
    app_deps = [] 
    for dep in dependencies:
        string = re.search(r'(?s)\.\[Description:(.*?)\]', dep) 
        if string:
            description = string.group(0) 
            dep = dep.replace(description,'')
        # First remove description which may contain dots
        dep_splitted = dep.split(".")
        app_deps.append(dep_splitted)
    return app_deps


def generic_update_or_create_ldap_entry(ldap, dn,objectClass,update_data, update_error, create_data, create_error):
        # try to update LDAP entry defined by its DN
    try:
        ldap.search(
            dn,
            "(objectclass=*)",
            attributes = [ 'dn']
        )
        if ldap.entries:
            ldap.modify(dn, update_data)
        else:
            p("view", update_error, e)
    # if not present tries to create entry in LDAP
    except Exception as e:
        try:
            ldap.add(dn,objectClass, create_data)
        except Exception as e:
            p("view", create_error , e)
def check_mountpoint(path):
    file1 = open("/proc/mounts", "r")
    # read file content
    readfile = file1.read()
    # checking condition for path found or not
    if path in readfile: 
        return True
    else: 
        return False
    file1.close() 
def get_mounted_borgfs():
    mounted_points = []
    filename = "/proc/mounts"
    readfile = open(filename, "r")

    for line in readfile:
        if str(line).startswith("borgfs"):
            borgs = line.split(" ")
            fullpath =borgs[1]
            chunks = fullpath.split("/")
            filename = chunks[len(chunks)-1]
            mounted_points.append(filename)
    return mounted_points
    readfile.close()

def generate_string(key_len):
    """ Generate a large password without special chars """
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
    key = get_random_string(key_len,chars)
    return key


def get_luks_partition():
    try:
        command ="blkid --match-token TYPE='crypto_LUKS' -o device".split()
        result = subprocess.run(command, stdout=subprocess.PIPE)
        result = str(result.stdout)
    except:
        result=""
    #Return the device name instead of boolean, in case we need it
    result = str(result)
    return result

def get_pool_domains(ldap,pool):
        # Check if a pool has any assigned domain
        # and return them
        domains = []
        try:
            ldap.search(
                settings.LDAP_TREE_HOSTING,
                '(&(objectClass=metaInfo)(type=%s))' % pool,
                attributes = ['vd']
                )
            domains =[ domain.vd.value for domain in  ldap.entries ]
        except Exception as e:
            print(e)
        return domains

def get_available_pools(ldap):
    user_fpms=[]
    dn = "ou=*,%s" % settings.LDAP_TREE_POOLS
    try:
        ldap.search(
            settings.LDAP_TREE_POOLS,
            '(objectClass=organizationalUnit)',
            attributes    = ['type', 'ou'],
            search_scope  = ldap3.SUBTREE
            )
        all_fpms = ldap.entries
        # Get only fpmXX entries, where XX must be a number.
        # WE have to distinguish them from the fpmAPPS entry such as fpmnextcloud
        # Which are not eidtable by user
        for fpm in all_fpms:
            ou = fpm.ou.value
            fpm_id = ou.replace("fpm","",1)
            if fpm_id.isdigit():
                pool_domains = get_pool_domains(ldap,ou)
                user_fpms.append({'ou': ou, 'type' : fpm.type.value, 'domains' : pool_domains})
    except Exception as e:
        p("utils · get_available_pools", "✕ There was a problem getting list of pool", e)

    return user_fpms


def encrypt_password(password):
  gpg = gnupg.GPG(gnupghome="/usr/share/mxcp/.gnupg")
  public_keys = gpg.list_keys()
  fingerprint = public_keys[0]['fingerprint']
  encrypted_password = gpg.encrypt(password, fingerprint,always_trust=True)
  return str(encrypted_password)

def has_cms(domain):
    #ou=cms,vd=wp2.com,o=hosting,dc=example,dc=tld
    try:
        ldapConn = connect_ldap(
            settings.AUTH_LDAP_BIND_DN,
            settings.AUTH_LDAP_BIND_PASSWORD
        )
        ldap = ldapConn['connection']
        dn = "ou=cms,vd=%s,%s" % (domain,settings.LDAP_TREE_HOSTING)
        ldap.search(
            dn,
            '(objectClass=organizationalUnit)',
            attributes = ['*']
        )
        wp = ldap.entries[0]
        return wp 
    except Exception as e:
        pass

def get_php_version():
    php = '8.1'
    try:
        obj = CpanelBasicConf.objects.get(pk="php")
        php = obj.status

    except LDAPNoSuchObjectResult:
        php = '8.1'
    except Exception as e:
        pass 

    return php

