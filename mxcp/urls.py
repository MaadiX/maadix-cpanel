# django
from django.contrib import admin
from django.conf.urls.i18n import i18n_patterns
from django.urls import path, re_path, include
from django.views.generic.base import TemplateView
# For paa revovery
from django.contrib.auth import views
#from django.contrib.auth import views as auth_views
from django.conf.urls import i18n 
# project
from apps.views import view_dkim as dkim
from apps.views import views_activate as activate
from apps.views import views_system as system
from apps.views import views_notifications as notifications
from apps.views import view_delete as delete
from apps.views import views_registration as registration
from apps.views import view_backups as backups
from apps.views import json_render
from apps.views import view_borgkey as borgkey
from apps.views import view_luks as luks
# ldapdb views
from ldapobj.views import user_view as ldap_users
from ldapobj.views import domain_view as ldap_domains
from ldapobj.views import mail_view as ldap_mails
from ldapobj.views import php_view as ldap_php
from ldapobj.views import unban_view as ldap_unban
from ldapobj.views import ldap_apps 
from ldapobj.views import services_view as ldap_services
from ldapobj.views import dns_view 
from ldapobj.views import details_view 
from ldapobj.views import update_view 
from ldapobj.views import rkhunter_view 
from ldapobj.views import ssh_view
from ldapobj.views import tls_view
from ldapobj.views import changefqdn_view as fqdn
from ldapobj.views import mailman_domains as mailman
from ldapobj.views import trash_view

from ldapobj.views import reboot_view as reboot
from ldapobj.views import status_view as status
from ldapobj.views import json_tor 
from . import twofactorurls as tf_urls

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('', include(tf_urls.urlpatterns)),
    # TO move to ldapobj
    path('activate', activate.Activate.as_view(), name="activate"),
    path('pass-recover', registration.PasswordResetView.as_view(), name="pass-recover"),
    path('recover-confirm/<str:uidb64>/<str:token>', registration.PasswordResetConfirmView.as_view(), name="recover-confirm"),
    path('pass-recover-done', registration.PasswordResetDoneView.as_view(), name="pass-recover-done"),
    path('reset-success', registration.PasswordResetSucessView.as_view(), name='password-reset-success'),
    path('system/luks-slots', luks.ManageLuksSlots.as_view(), name="luks-slots"),
    path('luks', luks.ActivateLuksSlots.as_view(), name="luks"),

    path('services/pgp', notifications.NotificationsPgp.as_view(), name='pgp-mails'),
    path('services/pgp/<str:keyid>/', notifications.ViewPgpKey.as_view(), name='pgpkey'),
    #path('dns', dns.DnsView.as_view(), name="dns"),
    #path('mailman/domains', domains.MailmanDomains.as_view(), name="domains-mailman"),
    
    path('notifications', notifications.Notifications.as_view(), name="notifications"),
    path('system/backups', backups.BorgBackups.as_view(), name="borgbackups"),
    path('borgkey', borgkey.BorgPrivateKey.as_view(), name="borgkey"),
    #p api
    path('api/delete-entry', delete.DeleteEntry.as_view(), name="api_delete"),
    path('api/get-cpu-usage', system.get_cpu_usage, name="api_get_cpu"),
    path('api/create-dkim', dkim.CreateDkim.as_view(), name="api_create_dkim"),
    path('api/check-borg', json_render.get_json_data, name="api_check_borg"),
    path('api/check-borgkey-status', json_render.get_keyborg_status, name="api_check_borgkey_status"),
    # a link to services-install is harcoded inside static/mxcp/js/status.js

    # Already in ldapobj
    # registration
    path('user-profile/', ldap_users.EditProfile.as_view(), name="mxcpprofile"),
    path('loginredirect', ldap_users.LoginRedirect, name="loginredirect"),
    path('logout', views.LogoutView.as_view(), name="logout"),

    path('domains/instructions', TemplateView.as_view(template_name='ldapobj/pages/domains-instructions.html'), name="domains-instructions"),
    # sections
    path('', details_view.Details.as_view(), name="system-details"),
    path('system/update', update_view.Update.as_view(), name="update"),
    path('system/fqdn', fqdn.ChangeFqdnView.as_view(), name="fqdn"), 
    path('changesystem', fqdn.NewFqdnSet.as_view(), name="changesystem"),

    path('system/config/rkhunter', rkhunter_view.RkhunterConfig.as_view(), name="rkhunter"), 
    path('system/config/ssh', ssh_view.SSHConfig.as_view(), name="ssh"),
    path('system/config/tls', tls_view.TlsConfig.as_view(), name="tls"),
    path('system/unban/',ldap_unban.UnbanView.as_view(), name='unban'),
    path('system/php-fpm',ldap_php.PhpFpmView.as_view(), name='php-fpm'),
    path('system/php-version/',ldap_php.PhpVersionView.as_view(), name='php-version'),
    path('system/reboot', reboot.Reboot.as_view(), name="reboot"),
    
    path('services/', ldap_services.Services.as_view(), name="services"),
    path('services/install/', ldap_services.AvailableAppsview.as_view(), name="services-available"),
    path('services/settings/<pk>/', ldap_apps.AppSettings.as_view(), name='apps-settings'),

    path('domains/', ldap_domains.ListDomainsView.as_view(), name="domains"),
    path('domains/add/', ldap_domains.AddDomainView.as_view(), name="domains-add"),
    path('domains/edit/<pk>/', ldap_domains.EditDomainView.as_view(), name="edit-ldapdomain"),
    path('checkdns/<str:domain>/',dns_view.DnsView.as_view(), name="checkdns"),
    path('mailman/domains', mailman.MailmanDomainsView.as_view(), name="domains-mailman"),
    path('mails/', ldap_mails.ListMailsView.as_view(), name="mails"),
    path('mails/add/', ldap_mails.AddMailView.as_view(), name="add-ldapmail"),
    path('mails/edit/<pk>/', ldap_mails.EditMailView.as_view(), name="edit-ldapmail"),

    path('trash', trash_view.Trash.as_view(), name="trash"),
    path('status-cpanel', status.CheckCpanelStatus.as_view(), name="status-cpanel"),
    path('api/create-dkim', mailman.CreateDkim.as_view(), name="api_create_dkim"),
    path('users/', ldap_users.ListUsersView.as_view(), name="users"),
    path('users/edit/<pk>/', ldap_users.EditUserView.as_view(), name="edit-ldapuser"),
    path('users/add/', ldap_users.AddUserView.as_view(), name="add-ldapuser"),
    path('users/superuser/edit', ldap_users.EditSuperUser.as_view(), name="superuser"),
    path('users/postmasters', ldap_users.ListPostmastersView.as_view(), name="postmasters"),
	path('users/postmasters/edit/<pk>/', ldap_users.EditPostmaster.as_view(), name="postmaster"),
    path('api/get-onions', json_tor.get_json_data, name="api-get-onions"),
    ]
