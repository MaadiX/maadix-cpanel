#ldap
import ldap, os
from django_auth_ldap.config import LDAPSearch, LDAPSearchUnion, LDAPGroupQuery
DEBUG             = False
# django ldap auth    
LDAP_AUTH_URL         = "ldap://1.1.1.1:389"
AUTH_LDAP_START_TLS   = True
AUTH_LDAP_SERVER_URI  = LDAP_AUTH_URL
AUTH_LDAP_BIND_DN = "cn=doe,dc=example,dc=tld"
AUTH_LDAP_BIND_PASSWORD = "doe_password"
AUTH_LDAP_USER_SEARCH = LDAPSearch('')

AUTH_LDAP_ALWAYS_UPDATE_USER = False
BASE_DIR=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Setiings for django-ldapdb
DATABASES = {
    'ldap': {
        'ENGINE': 'ldapdb.backends.ldap',
        'NAME': LDAP_AUTH_URL,
        'USER': AUTH_LDAP_BIND_DN,
        'PASSWORD': AUTH_LDAP_BIND_PASSWORD,
        'TLS'   : True,
     }, 
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
     },
}       
DATABASE_ROUTERS = ['ldapdb.router.Router']
     
