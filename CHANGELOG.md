# Maadix Control Panel Changelog
## [202408]

### Changed
- Do not allow webmaster deletion
- Fix CMS deletion fro domain
- Fix link in VPN instriction email

### Added
- Support for Mastodon & PeerTube
- Dkim creation form apps domains

## [202407]

### Changed
- Django Models implementation
- Tables for Lists views (Domains, Users, Mails, Postmasters, Trash)

### Added
- Support for Tor Onion services 

## [202406]

### Changed
- Django to v 4.2.16 

### Added
-Cryptpad with checkbox fileds 

## [202405]

### Changed
- Updated Puppet to v 8.7.0

### Added
- Support for Nginx 

## [202403]

### Changed
- Increased min password lenght to 14 
- Rainloop replaced by Snappymail


## [202402]

### Added
- Support for OS upgrade 
- Button for sending report


## [202302]

### Added
- Mailtrain2 install + migration from Mailtrain

### Changed
- Php 7.4 no longer available
- Owncloud no longer available

## [202301]

### Added

- Permanent removal of disabled applications
- Choice between PhP version 7.4 or PhP version 8.1
- Activation of ACLs for domain folders
- Borg management


## [202203]

### Added
- Option to assign a different root folder for domains


## [202202]

### Added
- Wordpress automated installation
- Option to assign quota to email accounts
- Option to create and assign own Pools to php applications


## [202201]

### Added

- OpenPgp key management for Zeyple
- Unlock IPs and users blocked by Fail2ban and Tally


## [202103]

### Added

- Option to receive notifications and logs to an external email account
- Checking available resources before installing applications
- Rkhunter: tool that checks the system status to detect the presence of possible rootkits, backdoors or other malware
- sysstat: tool for displaying the status of system resources 
- debsums: tool to check the integrity of installed packages 
- acct: tool to report the activity of users on the system 


## [202102]

### Added

- Two-factor authentication for the Control Panel: 2FA
- Simplified Control Panel password recovery process
- OOmKiller

## [202101]

### Added
- Vms connection to new API
- FQDN validation
- New forms for application installation with password field (avoiding emailing)
- Trash for system generated backup files from previous versions. 
- Form in the server activation process to set mysql password and not to be sent by email.

### Changed
- Encryption of application passwords with root public key so they are not stored in plain text on the system.

### Fixed
- The applications installation panel layout.
- Menus to read new API links

## [201902]

### Added

- Report submission button prior to Buster upgrade.
- Error/success message after report submission.
- System status check before performing Buster upgrade.
- Disk space check before upgrading to Buster.

### Fixed
- Translation and typos
